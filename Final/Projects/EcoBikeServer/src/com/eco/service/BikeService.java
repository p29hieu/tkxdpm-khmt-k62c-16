package com.eco.service;

import com.eco.bean.Bike;
import com.eco.bean.Station;
import com.eco.db.IEcoDatabase;
import com.eco.db.JsonBikeDatabase;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;

@Path("/bikes")
public class BikeService {

    private final IEcoDatabase<Bike> ecoDatabase;

    public BikeService() {
        ecoDatabase = JsonBikeDatabase.singleton();
    }

    @GET
    @Path("")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<Bike> getBikes() {
        return ecoDatabase.getAll(0, 20);
    }

    @GET
    @Path("/get")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<Bike> getBikes(@QueryParam("id") String id) {
        Bike bike = new Bike();
        bike.setId(id);
        return ecoDatabase.getOne(bike);
    }

    @POST
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Bike updateBike(@PathParam("id") String id, Bike bike) {
        return ecoDatabase.updateOne(bike);
    }

    @POST
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Bike addBike(Bike bike) {
        return ecoDatabase.addOne(bike);
    }
}