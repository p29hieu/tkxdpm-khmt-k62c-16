package com.eco.service;

import com.eco.bean.Station;
import com.eco.db.IEcoDatabase;
import com.eco.db.JsonStationDatabase;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;

@Path("/stations")
public class StationService {

    private final IEcoDatabase<Station> database;
    
    private JsonStationDatabase stationDatabase = new JsonStationDatabase();
    
    public StationService() {
        database = JsonStationDatabase.singleton();
    }

    @GET
    @Path("")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<Station> getAll() {
        return database.getAll(0, 10);
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<Station> getOne(@QueryParam("id") String id) {
        Station card = new Station();
        card.setId(id);
        return database.getOne(card);
    }

    @GET
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Station> getStations(@QueryParam("id") String id, @QueryParam("name") String name,
			@QueryParam("address") String address, @QueryParam("numSingleBike") int numSingleBike,
			@QueryParam("numSingleEBike") int numSingleEBike, @QueryParam("numDoubleBike") int numDoubleBike,
			@QueryParam("numEmptyDock") int numEmptyDock) {
		Station station = new Station(id, name, address);
		station.setNumSingleBike(numSingleBike);
		station.setNumDoubleBike(numDoubleBike);
		station.setNumSingleEBike(numSingleEBike);
		station.setNumEmptyDock(numEmptyDock);
		ArrayList<Station> res = stationDatabase.searchStation(station);
		return res;
	}

	@POST
	@Path("/update/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Station updateStation(@PathParam("id") String id, Station station) {
		return stationDatabase.updateStation(station);
	}
	
	@POST
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Station addStation(Station station) {
		return database.addOne(station);
	}
}