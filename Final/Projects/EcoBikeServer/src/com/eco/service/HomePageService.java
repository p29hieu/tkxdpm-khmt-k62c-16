package com.eco.service;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.JSONPObject;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.HashMap;
import java.util.Map;

@Path("/")
public class HomePageService {

    public HomePageService() {
    }

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public String hello() {
        Map<String, String> crunchifyMap = new HashMap<String, String>();
        crunchifyMap.put("message", "Hello world 1");
        try {
            return new ObjectMapper().writeValueAsString(crunchifyMap);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return "ERROR";
    }


}