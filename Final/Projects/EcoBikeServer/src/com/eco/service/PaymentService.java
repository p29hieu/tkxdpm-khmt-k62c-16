package com.eco.service;

import java.util.ArrayList;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.eco.bean.*;
import com.eco.db.IEcoDatabase;
import com.eco.db.JsonBikeDatabase;
import com.eco.db.JsonCardDatabase;
import com.eco.db.JsonTransactionDatabase;
import com.eco.db.seed.Seed;

@Path("/payment")
public class PaymentService {

    private static Seed singleton = new Seed();
    private static ArrayList<Deposit> deposits;
    private static ArrayList<Rental> rentals;
    private static ArrayList<Transaction> transactions;
    private final IEcoDatabase<Transaction> transactionDatabase;
    private final IEcoDatabase<Card> cardDatabase;

    public PaymentService() {
        deposits = singleton.getDeposit();
        rentals = singleton.getRental();
        transactions = singleton.getTransactions();
        transactionDatabase = JsonTransactionDatabase.singleton();
        cardDatabase = JsonCardDatabase.singleton();
    }

    @GET
    @Path("/deposit")
    @Produces(MediaType.APPLICATION_JSON)
    public Deposit getDeposit() {
        return deposits.get(0);
    }

    @GET
    @Path("/rental")
    @Produces(MediaType.APPLICATION_JSON)
    public Rental getRental() {
        // System.out.println(rentals.get(0).calRental(1456));
        return rentals.get(0);
    }

    @POST
    @Path("/rental")
    @Produces(MediaType.APPLICATION_JSON)
    public Rental updateRental(Rental res) {
        Rental rt = rentals.get(0);
        rentals.remove(rt);
        rentals.add(res);
        System.out.println(rentals);
        return res;
    }

    @POST
    @Path("/deposit")
    @Produces(MediaType.APPLICATION_JSON)
    public Deposit updateDeposit(Deposit res) {
        Deposit rt = deposits.get(0);
        deposits.remove(rt);
        deposits.add(res);
        System.out.println(deposits);
        return res;
    }

    @POST
    @Path("/transaction")
    @Produces(MediaType.APPLICATION_JSON)
    public Transaction createTransaction(Transaction transaction) {
        transactions.add(transaction);
        System.out.println(transactions);
        return transaction;
    }

    @GET
    @Path("/transaction/{user_id}")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<Transaction> getTransactions(@PathParam("user_id") String user_id) {
        Card card = new Card();
        card.setUserId(user_id);
        String card_id = "";
        ArrayList<Card> cards = cardDatabase.getOne(card);
        ArrayList<Transaction> res = new ArrayList<>();
        for (int i = 0; i < cards.size(); i++) {
            if (cards.get(i).getUserId().equals(user_id)) {
                card_id += cards.get(i).getId();
                break;
            }
        }
        Transaction transaction = new Transaction();
        transaction.setCardId(card_id);
        System.out.println("transactions" + transactions);
        for (int i = 0; i < transactions.size(); i++) {
            if (transactions.get(i).getStatus().equals("HIRING")) {
                res.add(transactions.get(i));
            }
        }
        System.out.println("res : " + res);
        return res;
    }

    @GET
    @Path("/getrental")
    @Produces(MediaType.APPLICATION_JSON)
    public long getRentalCost(@QueryParam("time") int time) {
        long res;
        res = rentals.get(0).calRental(time);
        return res;
    }

    @POST
    @Path("/transaction/update")
    @Produces(MediaType.APPLICATION_JSON)
    public Transaction updateTransaction(Transaction transaction) {
        for (Transaction t : transactions) {
            if (t.equals(transaction)) {
                t.setStatus(transaction.getStatus());
                break;
            }
        }
        // System.out.println(transactionDatabase.updateOne(transaction));
        // System.out.println(transaction);
        return transaction;
    }
}
