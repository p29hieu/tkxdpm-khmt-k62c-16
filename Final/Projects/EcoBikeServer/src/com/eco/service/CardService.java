package com.eco.service;

import com.eco.bean.Card;
import com.eco.db.IEcoDatabase;
import com.eco.db.JsonCardDatabase;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;

@Path("/cards")
public class CardService {

    private final IEcoDatabase<Card> database;

    public CardService() {
        database = JsonCardDatabase.singleton();
    }

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<Card> getAll() {
        return database.getAll(0, 10);
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList getOne(@QueryParam("id") String id) {
        Card card = new Card();
        card.setId(id);
        return database.getOne(card);
    }

}