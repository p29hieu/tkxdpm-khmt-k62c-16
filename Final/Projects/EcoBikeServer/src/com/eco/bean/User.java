package com.eco.bean;

import java.util.Date;

public class User {

    private String id;

    private String userName;

    private String password;

    private String phone;

    private String email;

    public User() {
    }

    public User(String userName, String password, String phone, String email) {
        this.id = String.valueOf(new Date().getTime());
        this.userName = userName;
        this.password = password;
        this.phone = phone;
        this.email = email;
    }

    public User(String id, String userName, String password, String phone, String email) {
        this.id = id;
        this.userName = userName;
        this.password = password;
        this.phone = phone;
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
