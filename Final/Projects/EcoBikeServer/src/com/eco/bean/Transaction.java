package com.eco.bean;

import java.util.Date;

//enum String {SUCCESS, REJECT, HIRING}

public class Transaction {

    private String id;
    private String cardId;
    private String bikeId;
    private String description = "";
    private int deposit;
    private int refund;
    private long startedAt;
    private long finishedAt = 0;
    private String status = "HIRING"; // {SUCCESS, REJECT, HIRING}

    public Transaction() {
    }

    public Transaction(String id, String cardId, String bikeId, String description, int deposit, int refund,
            long startedAt, long finishedAt, String status) {
        this.id = id;
        this.cardId = cardId;
        this.bikeId = bikeId;
        this.description = description;
        this.deposit = deposit;
        this.refund = refund;
        this.startedAt = startedAt;
        this.finishedAt = finishedAt;
        this.status = status;
    }

    public Transaction(String cardId, String bikeId, String description, int deposit, int refund, long startedAt,
            long finishedAt, String status) {
        this.id = String.valueOf(new Date().getTime());
        this.cardId = cardId;
        this.bikeId = bikeId;
        this.description = description;
        this.deposit = deposit;
        this.refund = refund;
        this.startedAt = startedAt;
        this.finishedAt = finishedAt;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getBikeId() {
        return bikeId;
    }

    public void setBikeId(String bikeId) {
        this.bikeId = bikeId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getDeposit() {
        return deposit;
    }

    public void setDeposit(int deposit) {
        this.deposit = deposit;
    }

    public int getRefund() {
        return refund;
    }

    public void setRefund(int refund) {
        this.refund = refund;
    }

    public long getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(long startedAt) {
        this.startedAt = startedAt;
    }

    public long getFinishedAt() {
        return finishedAt;
    }

    public void setFinishedAt(long finishedAt) {
        this.finishedAt = finishedAt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean match(Transaction transaction) {
        return this.getId().equals(transaction.getId()) || this.getCardId().equals(transaction.getCardId());
    }

    public boolean equals(Transaction transaction) {
        return this.getId().equals(transaction.getId()) && this.getCardId().equals(transaction.getCardId())
                && this.getStartedAt() == transaction.getStartedAt();
    }

}
