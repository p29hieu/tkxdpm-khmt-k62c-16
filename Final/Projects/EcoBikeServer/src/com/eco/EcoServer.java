package com.eco;

import com.eco.service.*;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

public class EcoServer {
    public static final int PORT = 8080;

    public static void main(String[] args) throws Exception {
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");

        Server jettyServer = new Server(PORT);
        jettyServer.setHandler(context);

        ServletHolder jerseyServlet = context.addServlet(org.glassfish.jersey.servlet.ServletContainer.class, "/*");
        jerseyServlet.setInitOrder(0);


        jerseyServlet.setInitParameter("jersey.config.server.provider.classnames",
                HomePageService.class.getCanonicalName() + ", " +
                        BikeService.class.getCanonicalName() + ", " +
                        PaymentService.class.getCanonicalName() + ", " +
                        CardService.class.getCanonicalName() + ", "+
                        StationService.class.getCanonicalName() + ", "
        );

        try {
            jettyServer.start();
            System.out.println("SERVER started at http://127.0.0.1:"+PORT);
            jettyServer.join();
        } finally {
            jettyServer.destroy();
        }
    }
}
