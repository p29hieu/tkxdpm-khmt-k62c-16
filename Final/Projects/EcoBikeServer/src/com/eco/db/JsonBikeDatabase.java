package com.eco.db;

import com.eco.bean.Bike;
import com.eco.bean.Card;
import com.eco.db.seed.Seed;

import java.util.ArrayList;

public class JsonBikeDatabase implements  IEcoDatabase<Bike>  {
    private static  IEcoDatabase<Bike > singleton = new JsonBikeDatabase();

    private final ArrayList<Bike> bikes = Seed.singleton().getBikes();

    private JsonBikeDatabase() {
    }

    public static  IEcoDatabase<Bike >singleton() {
        return singleton;
    }

    @Override
    public ArrayList<Bike> getAll(int pageIndex, int pageSize) {
        ArrayList<Bike> res = new ArrayList<Bike>();
        for (int i = pageIndex * pageSize; i < (pageIndex + 1) * pageSize; i++) {
            if (i >= bikes.size()) return res;
            res.add(bikes.get(i));
        }
        return res;
    }

    @Override
    public ArrayList<Bike> getOne(Bike bike) {
        ArrayList<Bike> res = new ArrayList<Bike>();
        for (Bike b : bikes) {
            if (b.match(bike)) {
                res.add(b);
            }
        }
        return res;
    }

    @Override
    public Bike addOne(Bike bike) {
        for (Bike m : bikes) {
            if (m.equals(bike)) {
                return null;
            }
        }
        bikes.add(bike);
        return bike;
    }

    @Override
    public Bike updateOne(Bike bike) {
        for(int i = 0; i<bikes.size(); i++){
            if(bikes.get(i).match(bike)){
                bikes.remove(i);
                bikes.add(i, bike);
                return bike;
            }
        }
        return null;
    }
}
