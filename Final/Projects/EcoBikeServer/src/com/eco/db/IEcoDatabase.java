package com.eco.db;

import java.util.ArrayList;

public interface IEcoDatabase <T> {
    public ArrayList<T> getAll(int pageIndex, int pageSize);

    public ArrayList<T> getOne(T t);

    public T updateOne(T t);

    public T addOne(T t);
    
}
