package com.eco.db.seed;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import com.eco.bean.*;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Seed {

    private static ArrayList<Deposit> deposits;
    private static ArrayList<Rental> rentals;

    //////
    private ArrayList<Bike> bikes;
    private ArrayList<Card> cards;
    private ArrayList<Station> stations;
    private ArrayList<Transaction> transactions;
    private ArrayList<User> users;
    //////
    private static Seed singleton = new Seed();


    public ArrayList<Bike> getBikes() {
        return bikes;
    }

    public ArrayList<Card> getCards() {
        return cards;
    }

    public ArrayList<Station> getStations() {
        return stations;
    }

    public ArrayList<Transaction> getTransactions() {
        return transactions;
    }

    public ArrayList<User> getUsers() {
        return users;
    }

    public static Seed singleton() {
        return singleton;
    }

    public Seed() {
        start();
    }

    public void start() {
        deposits = new ArrayList<Deposit>();
        rentals = new ArrayList<Rental>();

        deposits.addAll(generateDepositFromFile(new File(getClass().getResource("./deposit.json").getPath()).toString()));
        rentals.addAll(generateRentalFromFile(new File(getClass().getResource("./rental.json").getPath()).toString()));


        //////
        bikes = new ArrayList<Bike>();
        bikes.addAll(generateBikesFromFile(new File(getClass().getResource("./bike.json").getPath()).toString()));

        cards = new ArrayList<Card>();
        cards.addAll(generateCardsFromFile(new File(getClass().getResource("./card.json").getPath()).toString()));

        stations = new ArrayList<Station>();
        stations.addAll(generateStationsFromFile(new File(getClass().getResource("./station.json").getPath()).toString()));

        transactions = new ArrayList<Transaction>();
        transactions.addAll(generateTransactionFromFile(new File(getClass().getResource("./transaction.json").getPath()).toString()));

        users = new ArrayList<User>();
        users.addAll(generateUsersFromFile(new File(getClass().getResource("./user.json").getPath()).toString()));
        //////

    }

    //////
    private ArrayList<? extends Bike> generateBikesFromFile(String filePath) {
        ArrayList<? extends Bike> res = new ArrayList<Bike>();
        ObjectMapper mapper = new ObjectMapper();

        String json = FileReader.read(filePath);
        try {
            mapper.setDateFormat(new SimpleDateFormat("dd/MM/yyyy"));
            res = mapper.readValue(json, new TypeReference<ArrayList<Bike>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Invalid JSON input data from " + filePath);
        }
        return res;
    }

    private ArrayList<? extends Card> generateCardsFromFile(String filePath) {
        ArrayList<? extends Card> res = new ArrayList<Card>();
        ObjectMapper mapper = new ObjectMapper();

        String json = FileReader.read(filePath);
        try {
            mapper.setDateFormat(new SimpleDateFormat("dd/MM/yyyy"));
            res = mapper.readValue(json, new TypeReference<ArrayList<Card>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Invalid JSON input data from " + filePath);
        }
        return res;
    }

    private ArrayList<? extends Station> generateStationsFromFile(String filePath) {
        ArrayList<? extends Station> res = new ArrayList<Station>();
        ObjectMapper mapper = new ObjectMapper();
        String json = FileReader.read(filePath);
        try {
            mapper.setDateFormat(new SimpleDateFormat("dd/MM/yyyy"));
            res = mapper.readValue(json, new TypeReference<ArrayList<Station>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Invalid JSON input data from " + filePath);
        }
        return res;
    }

    private ArrayList<? extends Transaction> generateTransactionFromFile(String filePath) {
        ArrayList<? extends Transaction> res = new ArrayList<Transaction>();
        ObjectMapper mapper = new ObjectMapper();

        String json = FileReader.read(filePath);
        try {
            mapper.setDateFormat(new SimpleDateFormat("dd/MM/yyyy"));
            res = mapper.readValue(json, new TypeReference<ArrayList<Transaction>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Invalid JSON input data from " + filePath);
        }
        return res;
    }

    private ArrayList<? extends User> generateUsersFromFile(String filePath) {
        ArrayList<? extends User> res = new ArrayList<User>();
        ObjectMapper mapper = new ObjectMapper();

        String json = FileReader.read(filePath);
        try {
            mapper.setDateFormat(new SimpleDateFormat("dd/MM/yyyy"));
            res = mapper.readValue(json, new TypeReference<ArrayList<User>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Invalid JSON input data from " + filePath);
        }
        return res;
    }
    //////

    public ArrayList<Deposit> generateDepositFromFile(String filePath) {
        ArrayList<Deposit> res = new ArrayList<Deposit>();
        ObjectMapper mapper = new ObjectMapper();

        String json = FileReader.read(filePath);
        try {
            mapper.setDateFormat(new SimpleDateFormat("dd/MM/yyyy"));
            res = mapper.readValue(json, new TypeReference<ArrayList<Deposit>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Invalid JSON input data from " + filePath);
        }

        return res;
    }

    public ArrayList<Rental> generateRentalFromFile(String filePath) {
        ArrayList<Rental> res = new ArrayList<Rental>();
        ObjectMapper mapper = new ObjectMapper();

        String json = FileReader.read(filePath);
        try {
            mapper.setDateFormat(new SimpleDateFormat("dd/MM/yyyy"));
            res = mapper.readValue(json, new TypeReference<ArrayList<Rental>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Invalid JSON input data from " + filePath);
        }

        return res;
    }

    public ArrayList<Deposit> getDeposit() {
        return deposits;
    }

    public ArrayList<Rental> getRental() {
        return rentals;
    }
    
    public static void main(String[] args) {
        Seed seed = new Seed();
        seed.start();
        System.out.println(seed.getDeposit());
        System.out.println(seed.getRental());
        System.out.println(seed.getStations());
    }
}

