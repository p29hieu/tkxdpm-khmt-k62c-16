package com.eco.db;

import com.eco.bean.Transaction;
import com.eco.db.seed.Seed;

import java.util.ArrayList;

public class JsonTransactionDatabase implements IEcoDatabase<Transaction> {
    private static  IEcoDatabase<Transaction>  singleton = new JsonTransactionDatabase();

    private final ArrayList<Transaction> Transactions = Seed.singleton().getTransactions();

    private JsonTransactionDatabase() {
    }

    public static  IEcoDatabase<Transaction>  singleton() {
        return singleton;
    }

    @Override
    public ArrayList<Transaction> getAll(int pageIndex, int pageSize) {
        ArrayList<Transaction> res = new ArrayList<Transaction>();
        for (int i = pageIndex * pageSize; i < (pageIndex + 1) * pageSize; i++) {
            if (i >= Transactions.size()) return res;
            res.add(Transactions.get(i));
        }
        return res;
    }

    @Override
    public ArrayList<Transaction> getOne(Transaction Transaction) {
        ArrayList<Transaction> res = new ArrayList<Transaction>();
        for (Transaction b : Transactions) {
            if (b.match(Transaction)) {
                res.add(b);
            }
        }
        return res;
    }

    @Override
    public Transaction addOne(Transaction Transaction) {
        for (Transaction m : Transactions) {
            if (m.equals(Transaction)) {
                return null;
            }
        }
        Transactions.add(Transaction);
        return Transaction;
    }

    @Override
    public Transaction updateOne(Transaction transaction) {
        for (Transaction m : Transactions) {
            if (m.equals(transaction)) {
                Transactions.remove(m);
                Transactions.add(transaction);
                return transaction;
            }
        }
        return null;
    }
}

