package com.eco.db;

import com.eco.bean.Bike;
import com.eco.bean.Card;
import com.eco.db.seed.Seed;

import java.util.ArrayList;

public class JsonCardDatabase implements IEcoDatabase<Card> {
    private static  IEcoDatabase<Card>  singleton = new JsonCardDatabase();

    private final ArrayList<Card> cards = Seed.singleton().getCards();

    private JsonCardDatabase() {
    }

    public static  IEcoDatabase<Card>  singleton() {
        return singleton;
    }

    @Override
    public ArrayList<Card> getAll(int pageIndex, int pageSize) {
        ArrayList<Card> res = new ArrayList<Card>();
        for (int i = pageIndex * pageSize; i < (pageIndex + 1) * pageSize; i++) {
            if (i >= cards.size()) return res;
            res.add(cards.get(i));
        }
        return res;
    }

    @Override
    public ArrayList<Card> getOne(Card card) {
        ArrayList<Card> res = new ArrayList<Card>();
        for (Card b : cards) {
            if (b.match(card)) {
                res.add(b);
            }
        }
        return res;
    }

    @Override
    public Card addOne(Card card) {
        for (Card m : cards) {
            if (m.equals(card)) {
                return null;
            }
        }
        cards.add(card);
        return card;
    }

    @Override
    public Card updateOne(Card card) {
        for (Card m : cards) {
            if (m.equals(card)) {
                cards.remove(m);
                cards.add(card);
                return card;
            }
        }
        return null;
    }
}
