package com.eco.db;

import com.eco.bean.Station;
import com.eco.db.seed.Seed;

import java.util.ArrayList;

public class JsonStationDatabase implements IEcoDatabase<Station> {
    private static  IEcoDatabase<Station>  singleton = new JsonStationDatabase();

    private final ArrayList<Station> stations = Seed.singleton().getStations();

    public JsonStationDatabase() {
    }

    public static  IEcoDatabase<Station>  singleton() {
        return singleton;
    }

    @Override
    public ArrayList<Station> getAll(int pageIndex, int pageSize) {
        ArrayList<Station> res = new ArrayList<Station>();
        for (int i = pageIndex * pageSize; i < (pageIndex + 1) * pageSize; i++) {
            if (i >= stations.size()) return res;
            res.add(stations.get(i));
        }
        return res;
    }

    @Override
    public ArrayList<Station> getOne(Station station) {
        ArrayList<Station> res = new ArrayList<Station>();
        for (Station b : stations) {
            if (b.equals(station)) {
                res.add(b);
            }
        }
        return res;
    }

    @Override
    public Station addOne(Station station) {
//        for (Station m : stations) {
//            if (m.equals(station)) {
//                return null;
//            }
//        }
    	
    	String oldId = stations.get(stations.size() - 1).getId();    	
    	int count = Integer.parseInt(oldId.substring(10, 11)) + 1;
    	
    	station.setId("station-00" + count);
    	
        stations.add(station);
  
        return station;
    }

    @Override
    public Station updateOne(Station station) {
        for (Station m : stations) {
            if (m.equals(station)) {
                stations.remove(m);
                stations.add(station);
                return station;
            }
        }
        return null;
    }
    
    public ArrayList<Station> searchStation(Station station) {
		ArrayList<Station> res = new ArrayList<Station>();
		for (Station b: stations) {
			if (b.match(station)) {
				res.add(b);
			}
		}
		return res;
	}

	public Station addStation(Station station) {
		for (Station m: stations) {
			if (m.equals(station)) {
				return null;
			}
		}
		
		stations.add(station);
		return station;
	}
	
	public Station updateStation(Station station) {
		for (Station m: stations) {
			if (m.equals(station)) {
				stations.remove(m);
				stations.add(station);
				return station;
			}
		}
		return null;
	}
}
