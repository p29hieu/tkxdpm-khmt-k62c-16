Project:
EcoBikeClientAll: Chứa source code của client
EcoBikeServer: server 

#Yeu cầu:
1. IDE: Eclipse hoặc IntellJ phiên bản mới nhất (update ngày 20/12/2020)
2. Hệ điều hành: Windows/Linux/MacOS
3. Môi trường: JavaSDK8 (JavaSDK 1.8)
4. Nếu báo lỗi thiếu thư viện thì import thư mục \lib (có trong mỗi project) vào, xóa cache rồi chạy lại

# Hướng dẫn sử dụng
1. Chạy project EcoBikeServer: EcoBikeServer\src\com\eco\EcoServer.java;
2. Chạy project EcoBikeClienntAll:
  2.1. Nếu chạy admin, thì chạy  EcoBikeClienntAll/src/com/ers/app/admin/EBAdmin.java;
  2.2. Nếu chạy user thì chạy  EcoBikeClientAll\src\com\ers\app\user\EUSUser.java

3. Test: thư mục test: EcoBikeClientAll\test\com\AllTestSuite.java

