package com.ers.app.user;

import javax.swing.JPanel;

import com.ers.components.rental.controller.RentalBikeController;
import com.ers.components.rental.controller.RentingBikeController;
import com.ers.components.station.controller.UserStationPageController;

public class ERSUserController {
	
	public JPanel getStationPage() {
		UserStationPageController controller = new UserStationPageController();
		return controller.getDataPagePane();
	}
	
	public JPanel getCarRentalPage() {
		RentalBikeController controller = new RentalBikeController();
		return controller.getDataPagePane();
	}
	
	public JPanel getHiringBikePage() {
		RentingBikeController controller = new RentingBikeController();
		return controller.getDataPagePane();
	}
}
