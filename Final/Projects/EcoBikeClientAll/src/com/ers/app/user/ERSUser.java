package com.ers.app.user;

import java.awt.BorderLayout;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

@SuppressWarnings("serial")
public class ERSUser extends JFrame {

	public static final int WINDOW_WIDTH = 800;
	public static final int WINDOW_HEIGHT = 550;

	public ERSUser(ERSUserController controller) {
		JPanel rootPanel = new JPanel();
		setContentPane(rootPanel);
		BorderLayout layout = new BorderLayout();
		rootPanel.setLayout(layout);

		JTabbedPane tabbedPane = new JTabbedPane();
		rootPanel.add(tabbedPane, BorderLayout.CENTER);

		JPanel stationPage = controller.getStationPage();
		JPanel carRentalPage = controller.getCarRentalPage();
		JPanel rentingBikePage = controller.getHiringBikePage();

		tabbedPane.addTab("Bai xe", null, stationPage, "Bai xe");
		tabbedPane.addTab("Thue xe", null, carRentalPage, "Thue xe");
		tabbedPane.addTab("Xe dang thue", null, rentingBikePage, "Xe dang thue");
		tabbedPane.addTab("Lich su thue xe", null, new JPanel(), "Lich su thue xe");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Eco Bike Rental System for User");
		setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
		setVisible(true);
	}

	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}

		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new ERSUser(new ERSUserController());
			}
		});
	}
}