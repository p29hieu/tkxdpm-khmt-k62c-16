//done
package com.ers.app.admin;

import javax.swing.*;
import java.awt.*;


public class EBAdmin extends JFrame {

    public static final int WINDOW_WIDTH = 800;
    public static final int WINDOW_HEIGHT = 550;

    public EBAdmin(EBAdminController controller) {
        JPanel rootPanel = new JPanel();
        setContentPane(rootPanel);
        BorderLayout layout = new BorderLayout();
        rootPanel.setLayout(layout);

//        rootPanel.add(controller.getBikePage(), BorderLayout.NORTH);

        JTabbedPane tabbedPane = new JTabbedPane();
        rootPanel.add(tabbedPane, BorderLayout.CENTER);

        JPanel stationPage = controller.getStationPage();
        JPanel bookPage = controller.getBikePage();

        /**** HA HUU LINH - start */

        JPanel rentalPage = controller.getRentalPage(this);

        /**** end */

        if (bookPage != null) tabbedPane.addTab("Bikes", null, bookPage, "Bikes");
        tabbedPane.addTab("Stations", null, stationPage, "Tabs 2");
        tabbedPane.addTab("Rental", null, rentalPage, "Rental");


        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Eco Bike Systems");
        setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        setVisible(true);
    }

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }

        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                EBAdminController ebAdminController = new EBAdminController();
                new EBAdmin(ebAdminController);
            }
        });
    }
}
