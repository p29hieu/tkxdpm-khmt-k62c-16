//done
package com.ers.app.admin;

import com.ers.components.admin.rentalcomponent.rentalpage.RentalPage;
import com.ers.components.bike.BikePageController;
import com.ers.components.admin.rentalcomponent.rentalpage.ChangeRentalPriceEvent;
import com.ers.components.admin.rentalcomponent.rentalpage.ChangeRentalPriceListener;
import com.ers.components.admin.station.AdminStationPageController;

import javax.swing.*;

public class EBAdminController extends JFrame {
    private AdminStationPageController adminStationPageController;

    public EBAdminController() {
        this.adminStationPageController = new AdminStationPageController();
    }

    public JPanel getBikePage() {
        if (adminStationPageController == null) {
            adminStationPageController = new AdminStationPageController();
        }
        BikePageController bikePageController = new BikePageController(adminStationPageController);
        return bikePageController.getDataPagePane();
    }

    public JPanel getStationPage() {
        if (adminStationPageController == null) {
            adminStationPageController = new AdminStationPageController();
        }
        return adminStationPageController.getDataPagePane();
    }
    public JPanel getRentalPage(JFrame parent) {
        RentalPage rentalPage = new RentalPage(parent);
        rentalPage.setChangeRentalPriceListener(new ChangeRentalPriceListener() {
            @Override
            public void changeEventOccurred(ChangeRentalPriceEvent e) {
                e.getController().showDialog();
            }
        });
        return rentalPage;
    }

}
