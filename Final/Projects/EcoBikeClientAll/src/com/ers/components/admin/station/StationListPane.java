package com.ers.components.admin.station;

import com.ers.bean.Bike;
import com.ers.bean.Station;
import com.ers.bean.Transaction;
import com.ers.components.abstractdata.controller.ADataPageController;
import com.ers.components.abstractdata.controller.IDataManageController;
import com.ers.components.abstractdata.gui.ADataListPane;
import com.ers.components.abstractdata.gui.ADataSinglePane;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@SuppressWarnings("serial")
public class StationListPane extends ADataListPane<Station> {

    public StationListPane(ADataPageController<Station> controller) {
        this.controller = controller;

        this.button = new JButton("Add");

        IDataManageController<Station> manageController = new IDataManageController<Station>() {
            @Override
            public Station create(Station t) {
                if (controller instanceof AdminStationPageController) {
                    Station station = ((AdminStationPageController) controller).create(t);
                    insertData(station);
                }
                return t;
            }

            @Override
            public Station read(Station t) {
                // TODO Auto-generated method stub
                return null;
            }

            @Override
            public void delete(Station t) {
                // TODO Auto-generated method stub

            }

            @Override
            public boolean update(Station t) {
                // TODO Auto-generated method stub
                return false;
            }

        };

        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new StationAddDialog(manageController);
            }
        });
        pane.add(button);
    }

    @Override
    public void decorateSinglePane(ADataSinglePane<Station> singlePane) {
//		JSpinner spin = new JSpinner();
//		spin.setModel(new SpinnerNumberModel(1, 0, null, 1));
//		singlePane.addDataHandlingComponent(spin);
//		spin.setPreferredSize(new Dimension(100, 20));

        JButton button = new JButton("Edit");
        singlePane.addDataHandlingComponent(button);

        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });
    }
}



