//done
package com.ers.components.admin.rentalcomponent.rentalpage;

import java.util.EventListener;

public interface ChangeRentalPriceListener extends EventListener {
    public void changeEventOccurred(ChangeRentalPriceEvent e);
}
