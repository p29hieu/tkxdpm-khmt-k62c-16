package com.ers.components.admin.rentalcomponent;

import com.ers.bean.Deposit;
import com.ers.bean.Rental;
import com.ers.serverapi.PaymentApi;
import java.util.HashMap;

public abstract class ARentalCostController {

    protected static PaymentApi api = new PaymentApi();
    protected static Rental rental = api.getRental();
    protected static Deposit deposit = api.getDeposit();

    public ARentalCostController() {
    }

    public abstract void showDialog();

    public abstract void setData(HashMap<String, Integer> data);

    public abstract HashMap<String, Integer> getCurrentData();

    public void updateDialog() {
        if (rental != null && deposit != null) {
            api.updateRental(rental);
            api.updateDeposit(deposit);
            System.out.println("SUCCESSFUL!");
        }
    }

    public abstract void closeDialog();
}
