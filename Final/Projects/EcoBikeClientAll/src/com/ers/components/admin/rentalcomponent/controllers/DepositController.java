package com.ers.components.admin.rentalcomponent.controllers;

import com.ers.bean.Deposit;
import com.ers.components.admin.rentalcomponent.ARentalCostController;
import com.ers.components.admin.rentalcomponent.dialogs.DepositDialog;

import javax.swing.*;
import java.util.HashMap;

public class DepositController extends ARentalCostController {

    private DepositDialog depositDialog;

    public DepositController(JDialog parent) {
        super();
        this.depositDialog = new DepositDialog(parent, this);
    }

    @Override
    public void showDialog() {
        depositDialog.setVisible(true);
    }

    @Override
    public void setData(HashMap<String, Integer> data) {
        deposit.setDeposit(data.get("singleB"), data.get("singleEB"), data.get("doubleB"));
    }

    @Override
    public HashMap<String, Integer> getCurrentData() {
        HashMap<String, Integer> data = new HashMap<>();
        data.put("singleB", deposit.getSingleBike());
        data.put("singleEB", deposit.getSingleEBike());
        data.put("doubleB", deposit.getDoubleBike());
        return data;
    }

    @Override
    public void closeDialog() {
        depositDialog.setVisible(false);
    }
}
