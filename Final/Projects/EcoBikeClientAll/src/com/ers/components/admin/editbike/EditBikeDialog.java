// done
package com.ers.components.admin.editbike;

import com.ers.bean.Bike;
import com.ers.bean.Station;
import com.ers.components.abstractdata.controller.IDataManageController;
import com.ers.components.abstractdata.gui.ADataEditDialog;
import com.ers.constant.TypeBike;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class EditBikeDialog extends ADataEditDialog<Bike> {
    protected static ImageIcon createImageIcon(String path) {
        java.net.URL imgURL = EditBikeDialog.class.getResource(path);
        if (imgURL != null) {
            return new ImageIcon(imgURL);
        } else {
            System.err.println("Couldn't find file: " + path);
            return null;
        }
    }

    private JTextField nameField;
    private JButton typeField;
    private JTextField weightField;
    private JTextField licensePlateField;
    private JTextField producerField;
    private JTextField costField;
    private JLabel statusField;
    private JButton stationField;
    private final List<Station> listStation;
    private JLabel estimatedField;
    private JLabel  batteryPercentageField;
    private JLabel  loadCyclesField;


    public EditBikeDialog(Bike bike, IDataManageController<Bike> controller, List<Station> listStation) {
        super(bike, controller);
        this.listStation = listStation;
        stationField.addActionListener(
                showChooseStation()
        );
    }

    @Override
    public void buildControls() {
        int row = getLastRowIndex();
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(new JLabel("Name"), c);
        nameField = new JTextField(15);
        nameField.setText(t.getName());
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(nameField, c);

        row = getLastRowIndex();
        stationField = new JButton(t.getStationName());
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(new JLabel("Station"), c);
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(stationField, c);


        row = getLastRowIndex();
        JLabel types = new JLabel("Types");
        typeField = new JButton(t.getType());
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(types, c);
        typeField.addActionListener(
                showChoseType()
        );
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(typeField, c);


        row = getLastRowIndex();
        JLabel weight = new JLabel("Weight");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(weight, c);
        weightField = new JTextField(15);
        weightField.setText(t.getWeight() + "");
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(weightField, c);

        row = getLastRowIndex();
        JLabel licensePlate = new JLabel("License Plate");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(licensePlate, c);
        licensePlateField = new JTextField(15);
        licensePlateField.setText(t.getLicensePlate() + "");
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(licensePlateField, c);

        row = getLastRowIndex();
        JLabel producer = new JLabel("Producer");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(producer, c);
        producerField = new JTextField(15);
        producerField.setText(t.getProducer() + "");
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(producerField, c);

        row = getLastRowIndex();
        JLabel cost = new JLabel("cost");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(cost, c);
        costField = new JTextField(15);
        costField.setText(t.getCost() + "");
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(costField, c);

        row = getLastRowIndex();
        JLabel status = new JLabel("Status");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(status, c);
        statusField = new JLabel();
        statusField.setText(t.getStatus());
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(statusField, c);

        estimatedField = new JLabel();
        estimatedField.setText("" + t.getEstimated());
        batteryPercentageField = new JLabel();
        batteryPercentageField.setText("" + t.getBatteryPercentage());
        loadCyclesField = new JLabel();
        loadCyclesField.setText("" + t.getLoadCycles());
        if (t.getType().equals(TypeBike.SINGLE_E_BIKE) || t.getType().equals(TypeBike.DOUBLE_E_BIKE)) {

            row = getLastRowIndex();
            JLabel estimated = new JLabel("Estimated");
            c.gridx = 0;
            c.gridy = row;
            getContentPane().add(estimated, c);
            c.gridx = 1;
            c.gridy = row;
            getContentPane().add(estimatedField, c);

            row = getLastRowIndex();
            JLabel batteryPercentage = new JLabel("Battery Percentage");
            c.gridx = 0;
            c.gridy = row;
            getContentPane().add(batteryPercentage, c);
            c.gridx = 1;
            c.gridy = row;
            getContentPane().add(batteryPercentageField, c);

            row = getLastRowIndex();
            JLabel loadCycles = new JLabel("Load Cycles Field");
            c.gridx = 0;
            c.gridy = row;
            getContentPane().add(loadCycles, c);
            c.gridx = 1;
            c.gridy = row;
            getContentPane().add(loadCyclesField, c);

        }
    }

    @Override
    public Bike getNewData() {
        t.setName(nameField.getText());
        t.setType(typeField.getText());
        t.setWeight(Double.parseDouble(weightField.getText()));
        t.setLicensePlate(licensePlateField.getText());
        t.setProducer(producerField.getText());
        t.setCost(Long.parseLong(costField.getText()));
        t.setStationName(stationField.getText());
        t.setEstimated(Double.parseDouble(estimatedField.getText()));
        t.setBatteryPercentage(Double.parseDouble(batteryPercentageField.getText()));
        t.setLoadCycles(Integer.parseInt(loadCyclesField.getText()));
        return t;
    }

    public ActionListener showChooseStation() {
        return e -> {
            Object[] data;
            Station currentStation = new Station();
            currentStation.setId(t.getStationId());
            currentStation.setName(t.getStationName());
            if (listStation != null) {
                data = listStation.toArray();
            } else {
                ArrayList<Station> l = new ArrayList<>();
                l.add(currentStation);
                data = l.toArray();
            }

            Station s = (Station) JOptionPane.showInputDialog(
                    frame,
                    "Chose an station",
                    "Station",
                    JOptionPane.PLAIN_MESSAGE,
                    createImageIcon("images/middle.gif"),
                    data,
                    currentStation);

            //If a string was returned, say so.
            if ((s != null)) {
                stationField.setText(s.getName());
                t.setStationName(s.getName());
                t.setStationId(s.getId());
            }
        };
    }

    public ActionListener showChoseType() {
        return e -> {
            Object[] data = {TypeBike.SINGLE_BIKE, TypeBike.SINGLE_E_BIKE, TypeBike.DOUBLE_BIKE, TypeBike.DOUBLE_E_BIKE};
            String s = (String) JOptionPane.showInputDialog(
                    frame,
                    "Chose type",
                    "Types",
                    JOptionPane.PLAIN_MESSAGE,
                    createImageIcon("images/middle.gif"),
                    data,
                    t.getType());
            if ((s != null)) {
                typeField.setText(s);
                t.setType(s);
                this.getNewData();
            }
        };
    }
}

