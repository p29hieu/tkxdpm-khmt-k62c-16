package com.ers.components.admin.station;

import com.ers.bean.Station;
import com.ers.components.abstractdata.controller.ADataPageController;
import com.ers.components.abstractdata.gui.ADataListPane;
import com.ers.components.station.gui.StationSearchPane;
import com.ers.components.station.gui.StationSinglePane;
import com.ers.serverapi.StationApi;

import java.util.List;
import java.util.Map;

public class AdminStationPageController extends ADataPageController<Station> {

	public AdminStationPageController() {
		super.init();
		System.out.print("AdminStationPageController created");
	}
	
	@Override
	public ADataListPane<Station> createListPane() {
		return new StationListPane(this);
	}

	@Override
	public ADataListPane<Station> getListPane() {
		return super.getListPane();
	}

	@Override
	public com.ers.components.station.gui.StationSinglePane createSinglePane() {
		return new StationSinglePane();
	}
	
	@Override
	public com.ers.components.station.gui.StationSearchPane createSearchPane() {
		return new StationSearchPane();
	}
	
	@Override
	public List<Station> search(Map<String, String> searchParams) {
		return new StationApi().getStations(searchParams);
	};
	
	public Station create(Station station) {
		return new StationApi().create(station);
	};
}
