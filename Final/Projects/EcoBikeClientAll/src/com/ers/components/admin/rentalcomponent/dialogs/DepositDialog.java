//done
package com.ers.components.admin.rentalcomponent.dialogs;

import com.ers.components.admin.rentalcomponent.ARentalCostController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

public class DepositDialog extends JDialog implements ActionListener{

    private ARentalCostController controller;
    private JDialog parent;

    private HashMap<String, Integer> currentDeposit;
    private HashMap<String, Integer> newDeposit;

    private GridBagLayout layout;
    private GridBagConstraints c;

    private JComboBox singleBike;
    private JComboBox singleEBike;
    private JComboBox doubleBike;

    private JButton okButton;

    public DepositDialog(JDialog parent, ARentalCostController controller) {
        super(parent, "Chi phí đặt cọc", true);
        this.parent = parent;
        this.controller = controller;
        this.currentDeposit = controller.getCurrentData();
        layoutComponents();
    }

    public void layoutComponents() {
        layout = new GridBagLayout();
        setLayout(layout);
        setSize(new Dimension(300, 200));
        c = new GridBagConstraints();

        // khoi tao cac ComboBox
        singleBike = new JComboBox();
        singleEBike = new JComboBox();
        doubleBike = new JComboBox();

        okButton = new JButton("OK");
        okButton.addActionListener(this);

        /**** ROW 1 ****/
        // set up from Minutes
        DefaultComboBoxModel singleBikeModel = new DefaultComboBoxModel();
        singleBikeModel.addElement(currentDeposit.get("singleB"));
        singleBike.setModel(singleBikeModel);
        singleBike.setSelectedIndex(0);
        singleBike.setEditable(true);

        c.gridy = 0;
        c.weightx = 1;
        c.weighty = 0.005;

        c.gridx = 0;
        c.fill = GridBagConstraints.NONE;
        c.insets = new Insets(0, 0, 0, 10);
        c.anchor = GridBagConstraints.LINE_END;
        add(new JLabel("Single Bike:"), c);

        c.gridx = 1;
        c.anchor = GridBagConstraints.LINE_START;
        c.insets = new Insets(0, 0, 0, 0);
        add(singleBike, c);

        /**** ROW 2 ****/
        // set up to Minutes
        DefaultComboBoxModel singleEBikeModel = new DefaultComboBoxModel();
        singleEBikeModel.addElement(currentDeposit.get("singleEB"));
        singleEBike.setModel(singleEBikeModel);
        singleEBike.setSelectedIndex(0);
        singleEBike.setEditable(true);

        c.gridy++;
        c.weightx = 1;
        c.weighty = 0.005;

        c.gridx = 0;
        c.fill = GridBagConstraints.NONE;
        c.insets = new Insets(0, 0, 0, 10);
        c.anchor = GridBagConstraints.LINE_END;
        add(new JLabel("Single E-Bike:"), c);

        c.gridx = 1;
        c.anchor = GridBagConstraints.LINE_START;
        c.insets = new Insets(0, 0, 0, 0);
        add(singleEBike, c);

        /**** ROW 3 ****/
        // set up Fixed Price
        DefaultComboBoxModel doubleBikeModel = new DefaultComboBoxModel();
        doubleBikeModel.addElement(currentDeposit.get("doubleB"));
        doubleBike.setModel(doubleBikeModel);
        doubleBike.setSelectedIndex(0);
        doubleBike.setEditable(true);

        c.gridy++;
        c.weightx = 1;
        c.weighty = 0.005;

        c.gridx = 0;
        c.fill = GridBagConstraints.NONE;
        c.insets = new Insets(0, 0, 0, 10);
        c.anchor = GridBagConstraints.LINE_END;
        add(new JLabel("Double Bike:"), c);

        c.gridx = 1;
        c.anchor = GridBagConstraints.LINE_START;
        c.insets = new Insets(0, 0, 0, 0);
        add(doubleBike, c);

        /**** ROW 6 ****/
        c.gridy = c.gridy + 3;
        c.weightx = 1;
        c.weighty = 0.005;

        c.gridx = 1;
        c.anchor = GridBagConstraints.FIRST_LINE_START;
        c.insets = new Insets(0, 0, 0, 0);
        add(okButton, c);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        // TODO
        if (singleBike.getSelectedItem() instanceof Integer && singleEBike.getSelectedItem() instanceof Integer && doubleBike.getSelectedItem() instanceof Integer) {
            int sgBike = (Integer) singleBike.getSelectedItem();
            int sgEBike = (Integer) singleEBike.getSelectedItem();
            int dleBike = (Integer) doubleBike.getSelectedItem();

            boolean guard = validateDepositDialog(sgBike, sgEBike, dleBike);

            if (guard) {
                if (controller != null) {

                    newDeposit = new HashMap<>();
                    newDeposit.put("singleB", sgBike);
                    newDeposit.put("singleEB", sgEBike);
                    newDeposit.put("doubleB", dleBike);

                    controller.setData(newDeposit);
                    int confirm = JOptionPane.showConfirmDialog(DepositDialog.this,
                            "Confirm the change!!!",
                            "Confirm",
                            JOptionPane.DEFAULT_OPTION);
                    if (confirm == 0) {
                        controller.updateDialog();
                        parent.setVisible(false);
                    }
                }
            } else {
                JOptionPane.showMessageDialog(new JFrame(), "Invalid, please check the inputs again!", "WARNING",
                        JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(new JFrame(), "Bạn cần nhập các số nguyên.", "WARNING",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    public boolean validateDepositDialog(int singleB, int singleEB, int doubleB) {

        // check not negative integer
        if (singleB < 0 || singleEB < 0 || doubleB < 0) {
            return false;
        }

        return true;
    }
}