//done
package com.ers.components.admin.rentalcomponent.rentalpage;

import com.ers.components.admin.rentalcomponent.ARentalCostController;

import java.util.EventObject;

public class ChangeRentalPriceEvent extends EventObject {
    private ARentalCostController controller;

    public ChangeRentalPriceEvent(Object source) {
        super(source);
    }

    public ChangeRentalPriceEvent(Object source, ARentalCostController controller) {
        super(source);
        this.controller = controller;
    }

    public ARentalCostController getController() {
        return controller;
    }
}