package com.ers.components.admin.rentalcomponent.rentalpage;
import com.ers.components.admin.rentalcomponent.controllers.RentalController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class RentalPage extends JPanel implements ActionListener {

    private JFrame parent;

    private JButton changeRentalPriceButton;

    private ChangeRentalPriceListener changeRentalPriceListener;

    public RentalPage(JFrame parent) {
        this.parent = parent;
        setBorder(BorderFactory.createEtchedBorder());
        changeRentalPriceButton = new JButton("Doi gia thue xe");

        changeRentalPriceButton.addActionListener(this);

        setLayout(new FlowLayout(FlowLayout.LEFT));

        add(changeRentalPriceButton);
    }


    public void setChangeRentalPriceListener(ChangeRentalPriceListener listener) {
        this.changeRentalPriceListener = listener;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton clicked = (JButton) e.getSource();

        if (clicked == changeRentalPriceButton) {

            if (changeRentalPriceListener != null) {
                ChangeRentalPriceEvent ev = new ChangeRentalPriceEvent(this, new RentalController(parent));
                changeRentalPriceListener.changeEventOccurred(ev);
            }
        }
    }
}