package com.ers.components.admin.rentalcomponent.controllers;

import com.ers.bean.Rental;
import com.ers.components.admin.rentalcomponent.ARentalCostController;
import com.ers.components.admin.rentalcomponent.dialogs.RentalDialog;

import javax.swing.*;
import java.util.HashMap;

public class RentalController extends ARentalCostController {

    private RentalDialog rentalDialog;

    public RentalController(JFrame parent) {
        super();
        this.rentalDialog = new RentalDialog(parent, this);
    }

    @Override
    public void showDialog() {
        rentalDialog.setVisible(true);
    }

    @Override
    public void setData(HashMap<String, Integer> data) {
        rental.setRental(data.get("fromMinutes"), data.get("toMinutes"), data.get("fixedPrice"),
                data.get("refund"), data.get("after15"));
    }

    @Override
    public HashMap<String, Integer> getCurrentData() {
        HashMap<String, Integer> data = new HashMap<>();
        data.put("fromMinutes", rental.getFromMinutes());
        data.put("toMinutes", rental.getToMinutes());
        data.put("fixedPrice", rental.getFixedPrice());
        data.put("refund", rental.getRefund());
        data.put("after15", rental.getAfter15());
        return data;
    }

    @Override
    public void closeDialog() {
        rentalDialog.setVisible(false);
    }
}
