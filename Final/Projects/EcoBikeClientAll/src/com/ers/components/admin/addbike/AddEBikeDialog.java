package com.ers.components.admin.addbike;

import java.awt.Insets;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextField;

import com.ers.bean.Bike;
import com.ers.bean.Station;

@SuppressWarnings("serial")
public class AddEBikeDialog extends AddBikeDialog{
    private JTextField batteryPercentage;
    private JTextField loadCycles;

	public AddEBikeDialog(AddBikeController controller, List<Station> listStation,String type) {
		super(controller, listStation,type);
	}

	/**
	 * 
	 */
	@Override
	public void buildControls() {
	
		super.buildControls();
			int row = getLastRowIndex();
		
	        row = getLastRowIndex();
	        JLabel battery = new JLabel("batteryPercentage");
	        c.gridx = 0;
	        c.gridy = row;
	        getContentPane().add(battery, c);
	        batteryPercentage = new JTextField(15);
	       
	        c.gridx = 1;
	        c.gridy = row;
	        getContentPane().add(batteryPercentage, c);

	        row = getLastRowIndex();
	        JLabel loadcycles = new JLabel("loadCycles");
	        c.gridx = 0;
	        c.gridy = row;
	        getContentPane().add(loadcycles, c);
	        loadCycles = new JTextField(15);
	      
	        c.gridx = 1;
	        c.gridy = row;
	        getContentPane().add(loadCycles, c);
		
		
	}

	@Override
	public Bike getNewData() {
		// TODO Auto-generated method stub
		super.getNewData();
		try {
		bike.setBatteryPercentage(Double.parseDouble(batteryPercentage.getText()));}
		catch (Exception e) {
			bike.setBatteryPercentage(3.0);
		}
		try {
		bike.setLoadCycles(Integer.parseInt(loadCycles.getText()));
		} catch (Exception e) {
			bike.setLoadCycles(10);
		}
		return bike;
		
	}
	

	

}
