package com.ers.components.admin.station;

import com.ers.bean.Station;
import com.ers.components.abstractdata.controller.IDataManageController;
import com.ers.components.abstractdata.gui.ADataAddDialog;

import javax.swing.*;
import java.awt.*;

@SuppressWarnings("serial")
public class StationAddDialog extends ADataAddDialog<Station> {
	
	private JTextField nameField;
    private JTextField addressField;
    private JSpinner numSingleBikeField;
    private JSpinner numSingleEBikeField;
    private JSpinner numDoubleBikeField;
    private JSpinner numEmptyDockField;
	
	public StationAddDialog(IDataManageController<Station> controller) {
		super(controller);
	}

	@Override
	public void buildControls() {
		int row = getLastRowIndex();
		JLabel nameLabel = new JLabel("Name");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(nameLabel, c);
		nameField = new JTextField(15);
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(nameField, c);
		
		row = getLastRowIndex();
		JLabel addressLabel = new JLabel("Address");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(addressLabel, c);
		addressField = new JTextField(15);
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(addressField, c);
	
		
		row = getLastRowIndex();
		JLabel numSingleBikeLabel = new JLabel("Number of Bike");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(numSingleBikeLabel, c);
		numSingleBikeField = new JSpinner();
		numSingleBikeField.setModel(new SpinnerNumberModel(10, 0, null, 1));
		numSingleBikeField.setPreferredSize(new Dimension(126, 20));
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(numSingleBikeField, c);
		
		row = getLastRowIndex();
		JLabel numSingleEBikeLabel = new JLabel("Number of EBike");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(numSingleEBikeLabel, c);
		numSingleEBikeField = new JSpinner();
		numSingleEBikeField.setModel(new SpinnerNumberModel(10, 0, null, 1));
		numSingleEBikeField.setPreferredSize(new Dimension(126, 20));
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(numSingleEBikeField, c);
		
		row = getLastRowIndex();
		JLabel numDoubleBikeLabel = new JLabel("Number of Twin Bike");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(numDoubleBikeLabel, c);
		numDoubleBikeField = new JSpinner();
		numDoubleBikeField.setModel(new SpinnerNumberModel(10, 0, null, 1));
		numDoubleBikeField.setPreferredSize(new Dimension(126, 20));
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(numDoubleBikeField, c);
		
		row = getLastRowIndex();
		JLabel numEmptyDockLabel = new JLabel("Number of empty docks");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(numEmptyDockLabel, c);
		numEmptyDockField = new JSpinner();
		numEmptyDockField.setModel(new SpinnerNumberModel(10, 0, null, 1));
		numEmptyDockField.setPreferredSize(new Dimension(126, 20));
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(numEmptyDockField, c);
	}

	@Override
	public Station getNewData() {
		Station station = new Station();
		
		station.setName(nameField.getText());
		station.setAddress(addressField.getText());
		station.setNumSingleBike((int) numSingleBikeField.getValue());
		station.setNumSingleEBike((int) numSingleEBikeField.getValue());
		station.setNumDoubleBike((int) numDoubleBikeField.getValue());
		station.setNumEmptyDock((int) numEmptyDockField.getValue());

		if (station.getName().equals("") || station.getAddress().equals("")) {
			return null;
		}
		
		return station;
	}
}
