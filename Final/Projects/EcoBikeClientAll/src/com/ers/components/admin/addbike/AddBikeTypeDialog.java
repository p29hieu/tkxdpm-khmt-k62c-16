package com.ers.components.admin.addbike;

import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.ers.bean.Station;
import com.ers.components.bike.BikeListPane;
import com.ers.components.bike.BikePageController;
import com.ers.components.admin.station.AdminStationPageController;

public class AddBikeTypeDialog<T> extends JDialog{
	private T t;
	private GridBagLayout layout;
	private GridBagConstraints c = new GridBagConstraints();
	private JFrame frame;
	private JComboBox typeField;
	
	public AddBikeTypeDialog(BikePageController controller,BikeListPane listPane,AdminStationPageController adminStationPageController) {
		super((Frame) null, "Chon loai xe de tiep tuc", true);
		
		setContentPane(new JPanel());
		layout = new GridBagLayout();
		getContentPane().setLayout(layout);

		
		this.buildControls();
		
		JButton addButton = new JButton("Tiep tuc");
		addButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
					String newT = getNewData();
				 System.out.println(newT);
				 AddBikeController manageController = new AddBikeController(controller,listPane,adminStationPageController);
				 if(newT.equals("SINGLE_BIKE")||newT.equals("DOUBLE_BIKE")) {
					 System.out.println(newT);
					 new AddBikeDialog(manageController,(ArrayList<Station>) adminStationPageController.getListPane().getListData(),newT);
				 } else {
					 new AddEBikeDialog(manageController,(ArrayList<Station>) adminStationPageController.getListPane().getListData(),newT);
				 }
				 AddBikeTypeDialog.this.dispose();
				
			}
		});
		
		
		c.gridx = 1;
		c.gridy = getLastRowIndex();
		getContentPane().add(addButton, c);
		
		
		this.pack();
		this.setResizable(false);
		this.setVisible(true);
	}
	private int getLastRowIndex() {
		layout.layoutContainer(getContentPane());
		int[][] dim = layout.getLayoutDimensions();
	    int rows = dim[1].length;
	    return rows;
	}
	private String getNewData() {
		String t;
		t= typeField.getItemAt(typeField.getSelectedIndex()).toString();
		return t;	
	}
	private void buildControls() {
		int row = getLastRowIndex();
        JLabel types = new JLabel("Types");
        String type[]= {"SINGLE_BIKE", "SINGLE_E_BIKE", "DOUBLE_BIKE", "DOUBLE_E_BIKE"};
        typeField = new JComboBox(type);
        c.insets = new Insets(10,10,10,10);
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(types, c);
      
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(typeField, c);

	}

}
