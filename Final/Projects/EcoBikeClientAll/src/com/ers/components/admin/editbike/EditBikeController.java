//done
package com.ers.components.admin.editbike;

import com.ers.bean.Bike;
import com.ers.components.abstractdata.controller.IDataManageController;
import com.ers.components.bike.BikeListPane;
import com.ers.serverapi.BikeApi;

import javax.swing.*;

public class EditBikeController implements IDataManageController<Bike> {

    private Bike bike;
    private final BikeListPane listPane;

    public EditBikeController(Bike bike, BikeListPane listPane) {
        this.bike = bike;
        this.listPane = listPane;
    }

    @Override
    public Bike create(Bike bike) {
        return null;
    }

    @Override
    public Bike read(Bike bike) {
        return null;

    }

    @Override
    public void delete(Bike bike) {

    }

    @Override
    public boolean update(Bike b) {
        if (b != null) {
            if (this.bike.getId().equals(b.getId())) {
                if (!this.bike.isValidateToUpdate(b)) {
                    // JOptionPane.showMessageDialog(new JFrame("Input is invalid"), "Your data is
                    // invalid!");
                    return false;
                }
                // call API
                try {
                    Bike bike1 = new BikeApi().updateBike(b.getId(), b);
                    if (!this.bike.match(bike1)) {
                        return false;
                    }
                    listPane.updateData(bike1);
                    this.bike = bike1;
                    return true;
                } catch (Exception e) {
                    System.out.println("ERROR " + e.toString());
                    return false;
                }
                //
            }
        }
        return false;
    }
}
