//done
package com.ers.components.admin.addbike;

import java.util.List;

import com.ers.bean.Bike;
import com.ers.bean.Station;
import com.ers.components.abstractdata.controller.ADataPageController;
import com.ers.components.abstractdata.controller.IDataManageController;
import com.ers.components.bike.BikeListPane;
import com.ers.components.admin.station.AdminStationPageController;

public class AddBikeController implements IDataManageController<Bike> {
	private ADataPageController<Bike> controller;
	private BikeListPane listPane;
	private AdminStationPageController stationPageController;
	public AddBikeController(ADataPageController<Bike> controller,BikeListPane listPane,AdminStationPageController stationController) {
		// TODO Auto-generated constructor stub
		super();

		this.controller= controller;
		this.listPane = listPane;
		this.stationPageController = stationController;
	}

	@Override
	public Bike create(Bike t) {
		List<? extends Station> listData = stationPageController.getListPane().getListData();	
		stationPageController.getListPane().updateListData(updateList(listData,t));
		int i = listPane.getListDataLength();
		if(i<10) {
			t.setId("bike-00"+i);
		}else {
			t.setId("bike-0"+i);
		}		
		controller.create(t);
		listPane.insertData(t);
		return null;
	}

	@Override
	public Bike read(Bike t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(Bike t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean update(Bike t) {
		// TODO Auto-generated method stub
		return true;
	}
	private List<? extends Station> updateList(List<? extends Station> listData,Bike t) {
		 for (int i = 0; i < listData.size(); i++) {
	            Station temp = listData.get(i);
	            if(temp.getId().equals(t.getStationId())) {
	            	temp.setNumEmptyDock(temp.getNumEmptyDock()-1);
	            	if(t.getType().equals("SINGLE_BIKE")) {
	            		temp.setNumSingleBike(temp.getNumSingleBike()+1);
	            	} else if(t.getType().equals("SINGLE_E_BIKE")) {
	            		temp.setNumSingleEBike(temp.getNumSingleEBike()+1);
	            	} else {
	            		temp.setNumDoubleBike(temp.getNumDoubleBike()+1);
	            	}
	            	break;
	            }
	        }
		 return listData;
	}

}
