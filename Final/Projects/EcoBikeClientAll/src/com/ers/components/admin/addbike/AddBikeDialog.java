//done
package com.ers.components.admin.addbike;

import com.ers.bean.Bike;
import com.ers.bean.Station;
import com.ers.components.abstractdata.gui.ADataAddBikeDialog;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class AddBikeDialog extends ADataAddBikeDialog<Bike> {
	private JTextField nameField;
    private JLabel typeField;
    private JTextField weightField;
    private JTextField licensePlateField;
    private JTextField producerField;
    private JTextField costField;
    private JLabel statusField;
    private JComboBox stationField;
    protected Bike bike;


	public AddBikeDialog(AddBikeController controller,List<Station> listStation,String type) {
		super(controller,listStation,type);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void buildControls() {
	
		int row = getLastRowIndex();
        c.gridx = 0;
        c.gridy = row;
        c.insets = new Insets(5,0,5,5);
        getContentPane().add(new JLabel("Name"), c);
        nameField = new JTextField(15);
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(nameField, c);

        row = getLastRowIndex();
        stationField = new JComboBox(listStation.toArray());
        c.gridx = 0;
        c.gridy = row;
        
        getContentPane().add(new JLabel("Station"), c);
        c.gridx = 1;
        c.gridy = row;
        c.weightx= 1;
        getContentPane().add(stationField, c);



        row = getLastRowIndex();
        JLabel types = new JLabel("Types");

        typeField = new JLabel(type);
        
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(types, c);
      
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(typeField, c);
        


        row = getLastRowIndex();
        JLabel weight = new JLabel("Weight");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(weight, c);
        weightField = new JTextField(15);
        weightField.setEditable(true);
      
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(weightField, c);

        row = getLastRowIndex();
        JLabel licensePlate = new JLabel("License Plate");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(licensePlate, c);
        licensePlateField = new JTextField(15);
      
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(licensePlateField, c);

        row = getLastRowIndex();
        JLabel producer = new JLabel("Producer");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(producer, c);
        producerField = new JTextField(15);
       
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(producerField, c);

        row = getLastRowIndex();
        JLabel cost = new JLabel("cost");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(cost, c);
        costField = new JTextField(15);
      
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(costField, c);

        row = getLastRowIndex();
        JLabel status = new JLabel("Status");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(status, c);
        statusField = new JLabel("EMPTY");
        
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(statusField, c);
		
	}

	@Override
	public Bike getNewData() {
		// TODO Auto-generated method stub
		bike = new Bike();
		if(nameField.getText()!=null) 
		{
		bike.setName(nameField.getText());
		}
		
		bike.setType(typeField.getText());
		if(licensePlateField.getText()!=null) {
		bike.setLicensePlate(licensePlateField.getText());
		} 
		
		bike.setStatus(statusField.getText());
		
		try{
		bike.setCost(Long.parseLong(costField.getText()));
		}catch (Exception e) {
			bike.setCost(100000);
		}
		if(producerField.getText()!=null) {
		bike.setProducer(producerField.getText());
		}
		try {
		bike.setWeight(Double.parseDouble(weightField.getText()));
		}catch (Exception e) {
			// TODO: handle exception
			bike.setWeight(3.0);
		}
		
		int index = stationField.getSelectedIndex();
		
		bike.setStationName(stationField.getItemAt(index).toString());
		bike.setStationId(listStation.get(index).getId());
		 
		return bike;
		
	}

}
