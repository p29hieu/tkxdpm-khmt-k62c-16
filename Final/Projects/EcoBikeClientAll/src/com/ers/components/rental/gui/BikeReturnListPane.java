package com.ers.components.rental.gui;

import java.awt.Dimension;
import java.awt.LayoutManager;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.ers.bean.Transaction;
import com.ers.components.rental.controller.RentingBikeController;

public class BikeReturnListPane extends JScrollPane{
	private LayoutManager layout;
    protected JPanel pane;

    protected RentingBikeController controller;

    public BikeReturnListPane() {
        pane = new JPanel();
        layout = new BoxLayout(pane, BoxLayout.Y_AXIS);
        pane.setLayout(layout);

        this.setViewportView(pane);
        this.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        this.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        this.getVerticalScrollBar().setUnitIncrement(20);
        this.getHorizontalScrollBar().setUnitIncrement(20);
    }
    
    public BikeReturnListPane(RentingBikeController controller) {
        this();
        this.controller = controller;
    }
    
    public void updateData() {
        pane.removeAll();
        pane.revalidate();
        pane.repaint();

        BikeReturnSinglePane bikeReturn = controller.createSinglePane();

//        bikeReturn.updateData(transaction);
//        this.decorateSinglePane(bikeReturn);
        pane.add(bikeReturn);
        pane.add(Box.createRigidArea(new Dimension(0, 40)));
        }
    public void delete() {

        pane.removeAll();
        pane.revalidate();
        pane.repaint();
    }
    

    
}