package com.ers.components.rental.gui;

import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;

import com.ers.bean.Bike;
import com.ers.bean.Transaction;
import com.ers.components.abstractdata.controller.IDataRentalController;

import static javax.swing.JOptionPane.showMessageDialog;

public class BikeRetalPaymentInput extends JDialog {
	protected GridBagLayout layout;
	protected GridBagConstraints c = new GridBagConstraints();
	private JTextField bankAccountField;

	public BikeRetalPaymentInput(Bike bike, IDataRentalController controller) {
		super((Frame) null, "Payment", true);

		controller.updateData(bike);
		setContentPane(new JPanel());
		layout = new GridBagLayout();
		getContentPane().setLayout(layout);

		this.buildControls();
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent windowEvent) {
				System.out.println("windowClosing");
				Bike bikeReponse = controller.changeStatusBike(bike.getId(), bike, "EMPTY");
				controller.updateData(bikeReponse);
			}

			@Override
			public void windowClosed(WindowEvent windowEvent) {
				System.out.println("windowClosed");
			}
		});
		JButton saveButton = new JButton("Thanh toán");
		saveButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String cardId = bankAccountField.getText();
				System.out.println("accountInfo: " + cardId);
				System.out.println("bikeId: " + bike.getType());
				int deposit = controller.getDeposit(bike.getType());
				if (deposit != 0) {
					Transaction transaction = controller.createTransaction(cardId, bike.getId(), "Create transaction with " + bike.getId(), deposit);
					if (transaction != null) {
						Bike bikeUpdate = controller.changeStatusBike(bike.getId(), bike, "HIRING");
						if (bikeUpdate != null) {
							showMessageDialog(null, "Thue xe thanh cong");
							BikeRetalPaymentInput.this.dispose();
						} else {
							showMessageDialog(null, "Cập nhật thông tin thue xe thất bại");
						}
						controller.updateData(bikeUpdate);
					} else {
						showMessageDialog(null, "Tạp giao dịch thất bại");
					}
				}
			}
		});

		c.gridx = 1;
		c.gridy = getLastRowIndex();
		getContentPane().add(saveButton, c);

		this.pack();
		this.setResizable(false);
		this.setVisible(true);
	}

	protected int getLastRowIndex() {
		layout.layoutContainer(getContentPane());
		int[][] dim = layout.getLayoutDimensions();
		int rows = dim[1].length;
		return rows;
	}

	public void buildControls() {
		int row = getLastRowIndex();
		JLabel bankAccountLabel = new JLabel("Mã thẻ tín dụng ảo");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(bankAccountLabel, c);
		bankAccountField = new JTextField(15);
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(bankAccountField, c);
	}
}
