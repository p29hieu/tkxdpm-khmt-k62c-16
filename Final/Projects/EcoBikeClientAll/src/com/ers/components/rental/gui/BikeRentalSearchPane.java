package com.ers.components.rental.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.ers.components.abstractdata.controller.IDataSearchController;

@SuppressWarnings("serial")
public class BikeRentalSearchPane extends JPanel {
	protected GridBagLayout layout;
	protected GridBagConstraints c;
	
	private IDataSearchController controller;

	private JTextField scanBarcodeField;
	
	public BikeRentalSearchPane() {
		layout = new GridBagLayout();
		this.setLayout(layout);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		
		buildControls();
		
		int row = getLastRowIndex();
		c.gridx = 2;
		c.gridy = row - 1;
		JButton searchButton = new JButton("Thue Xe");
		add(searchButton, c);
		searchButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				controller.search(getQueryParams());
			}
		});
		

		// Empty label for resizing
		c.weightx = 1;
		c.gridx = 3;
		c.gridy = row - 1;
		add(new JLabel(), c);
	}
	
	public BikeRentalSearchPane(IDataSearchController controller) {
		this();
		this.controller = controller;
	}
	
	public void buildControls() {
		JLabel scanBarcodeLabel = new JLabel("Barcode");
		scanBarcodeField = new JTextField(15);
		int row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(scanBarcodeLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(scanBarcodeField, c);
	};
	
	public Map<String, String> getQueryParams() {
		Map<String, String> res = new HashMap<String, String>();
		
		if (!scanBarcodeField.getText().trim().equals("")) {
			res.put("id", scanBarcodeField.getText().trim());
		}
		
		return res;
	}
	
	protected int getLastRowIndex() {
		layout.layoutContainer(this);
		int[][] dim = layout.getLayoutDimensions();
	    int rows = dim[1].length;
	    return rows;
	}
	
	
	public void setController(IDataSearchController controller) {
		this.controller = controller;
	}
	
	public void fireSearchEvent() {
		controller.search(getQueryParams());
	}

}
