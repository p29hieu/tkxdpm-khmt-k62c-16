package com.ers.components.rental.gui;

import javax.swing.JPanel;
import javax.swing.SpringLayout;

public class BikeRentalPane extends JPanel {
	public BikeRentalPane(BikeRentalSearchPane searchPane, ListBikeRental carRental) {
		SpringLayout layout = new SpringLayout();
		this.setLayout(layout);
		
		
		this.add(searchPane);
		this.add(carRental);
		
		
		layout.putConstraint(SpringLayout.WEST, searchPane, 5, SpringLayout.WEST, this);
		layout.putConstraint(SpringLayout.NORTH, searchPane, 5, SpringLayout.NORTH, this);
		layout.putConstraint(SpringLayout.EAST, searchPane, -5, SpringLayout.EAST, this);
		
		
		layout.putConstraint(SpringLayout.WEST, carRental, 5, SpringLayout.WEST, this);
		layout.putConstraint(SpringLayout.NORTH, carRental, 5, SpringLayout.SOUTH, searchPane);
		layout.putConstraint(SpringLayout.EAST, carRental, -5, SpringLayout.EAST, this);
		layout.putConstraint(SpringLayout.SOUTH, carRental, -5, SpringLayout.SOUTH, this);
	}
}
