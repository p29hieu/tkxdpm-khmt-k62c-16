package com.ers.components.rental.gui;

import java.awt.Dialog;
import java.awt.Frame;
import java.awt.GraphicsConfiguration;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.ers.bean.Bike;
import com.ers.bean.Station;
import com.ers.bean.Transaction;
import com.ers.components.rental.controller.RentingBikeController;
import com.ers.serverapi.BikeApi;
import com.ers.serverapi.StationApi;
import com.ers.serverapi.TransactionApi;


public class ReturnDialog extends JDialog {
	private Transaction transaction;
	private JTextField bikeIdField;
	private JTextField depositField;
	private JTextField refundField;
	private JTextField startAtField;
	private JTextField finishAtField;
	private JComboBox stationField;
	private JTextField rentalField;
	private JTextField timeField;
	private JTextField totalField;
	
	protected GridBagLayout layout;
	protected GridBagConstraints c = new GridBagConstraints();
	
	public ReturnDialog() {}
	
	public ReturnDialog(Transaction t, RentingBikeController controller) {
		super((Frame)null, "Return bike", true,  (GraphicsConfiguration)null);
		this.transaction = t;
		setContentPane(new JPanel());
		layout = new GridBagLayout();
		getContentPane().setLayout(layout);
		this.buildControls(t);
		
		JButton returnButton = new JButton("Tra xe");
		returnButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new TransactionApi().updateTransaction(getNewData());
				Bike bike = getBike(t.getBikeId());
				String stationID = stationField.getSelectedItem().toString();
				System.out.println(stationID);
				System.out.println(stationField.getSelectedObjects().toString());
				Station station = getStation(stationID);
				
				station = controller.updateStation(bike.getType(), station);
				bike.setStatus("EMPTY");
				bike.setStationName(station.getName());
				bike.setStationId(station.getId());
//				, SINGLE_E_BIKE, DOUBLE_BIKE, DOUBLE_E_BIKE
				
				new BikeApi().updateBike(transaction.getBikeId(), bike);
				controller.updateData();
				ReturnDialog.this.dispose();
			}
		});
		
		
		c.gridx = 1;
		c.gridy = getLastRowIndex();
		getContentPane().add(returnButton, c);
		
		
		this.pack();
		this.setResizable(false);
		this.setVisible(true);
	}
	
	protected int getLastRowIndex() {
		layout.layoutContainer(getContentPane());
		int[][] dim = layout.getLayoutDimensions();
	    int rows = dim[1].length;
	    return rows;
	}
	
	public void buildControls(Transaction t) {
		int row = getLastRowIndex();
		JLabel bikeIdLabel = new JLabel("BikeId");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(bikeIdLabel, c);
		bikeIdField = new JTextField(15);
		bikeIdField.setText(t.getBikeId());
		bikeIdField.setEditable(false);
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(bikeIdField, c);
		
		
		row = getLastRowIndex();
		JLabel depositLabel = new JLabel("Deposit");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(depositLabel, c);
		depositField = new JTextField(15);
		depositField.setText(Integer.toString(t.getDeposit()));
		depositField.setEditable(false);
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(depositField, c);
		
		
		row = getLastRowIndex();
		JLabel refundLabel = new JLabel("Refund");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(refundLabel, c);
		refundField = new JTextField(15);
		refundField.setText(t.getRefund() + "");
		refundField.setEditable(false);
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(refundField, c);
		
		row = getLastRowIndex();
		JLabel startAtLabel = new JLabel("Start at");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(startAtLabel, c);
		startAtField = new JTextField(15);
		startAtField.setText(new RentingBikeController().convertTime(t.getStartedAt()));
		startAtField.setEditable(false);
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(startAtField, c);
		
		Date nowDate = new Date();
		row = getLastRowIndex();
		JLabel finishAtLabel = new JLabel("Finish at");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(finishAtLabel, c);
		finishAtField = new JTextField(15);
		finishAtField.setText(new SimpleDateFormat("yyyy MM dd HH:mm:ss").format(nowDate));
		finishAtField.setEditable(false);
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(finishAtField, c);
		
		row = getLastRowIndex();
		JLabel timeLabel = new JLabel("Time: ");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(timeLabel, c);
		timeField = new JTextField(15);
		timeField.setText(Long.toString(Math.round((nowDate.getTime() - t.getStartedAt())/60000)));
		timeField.setEditable(false);
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(timeField, c);
		
		
		row = getLastRowIndex();
		JLabel stationLabel = new JLabel("Station");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(stationLabel, c);
		String[] stationsList = getAvailableStation();
		if(stationsList.length == 0) {
			stationsList = new String[] {"Khong con bai nao trong"};
		}
		stationField = new JComboBox(stationsList);
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(stationField, c);
		
		row = getLastRowIndex();
		JLabel rentalLabel = new JLabel("Rental:");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(rentalLabel, c);
		rentalField = new JTextField(15);
		Map<String, Integer> queryParams = new HashMap<String, Integer>();
		queryParams.put("time", Math.round((nowDate.getTime() - t.getStartedAt())/60000));
		long rental = new TransactionApi().getRental(queryParams);
		rentalField.setText(Long.toString(rental));
		rentalField.setEditable(false);
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(rentalField, c);
		
		row = getLastRowIndex();
		JLabel totalLabel = new JLabel("Total pay:");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(totalLabel, c);
		totalField = new JTextField(15);
		totalField.setText(Long.toString(rental - t.getDeposit()));
		totalField.setEditable(false);
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(totalField, c);
	};
	
	public String[] getAvailableStation() {
		ArrayList<Station> stations = new StationApi().getStations(null);
//		for(int i = 0; i < )
		String[] res = {};
		ArrayList<String> tmp= new ArrayList<>();
		for(int i = 0; i < stations.size(); i++) {
			if(stations.get(0).getNumEmptyDock() > 0) {
			tmp.add(stations.get(i).getId());}
		}
		return tmp.toArray(res);
	}
	
	public Transaction getNewData() {
		this.transaction.setStatus("SUCCESS");
		String finishString = this.finishAtField.getText();
		System.out.println(finishString);
		return this.transaction;
//		Date finishatLong = new SimpleDateFormat("yyyy MM dd HH:mm:ss").parse();
//		this.transaction.setFinishedAt(finishatLong.getTime());
	}
	
	public Bike getBike(String bikeId) {
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("id", bikeId);
		return new BikeApi().getBike(queryParams).get(0);
	}
	
	public Station getStation(String stationId) {
		return new StationApi().getOneStation(stationId);
	}
	
}
