package com.ers.components.rental.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.SpringLayout;

import com.ers.components.rental.controller.RentingBikeController;

public class BikeReturnPagePane extends JPanel {
	public BikeReturnPagePane(BikeReturnListPane bikeRental, RentingBikeController controller) {
		SpringLayout layout = new SpringLayout();
		this.setLayout(layout);
		
		JButton reloadButton = new JButton("Reload");
		reloadButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				controller.updateData();
			}
		});
		
		this.add(reloadButton);
		this.add(bikeRental);
		
		
		layout.putConstraint(SpringLayout.WEST, reloadButton, 5, SpringLayout.WEST, this);
		layout.putConstraint(SpringLayout.NORTH, reloadButton, 5, SpringLayout.NORTH, this);
		layout.putConstraint(SpringLayout.EAST, reloadButton, -5, SpringLayout.EAST, this);
		
		layout.putConstraint(SpringLayout.WEST, bikeRental, 5, SpringLayout.WEST, this);
		layout.putConstraint(SpringLayout.NORTH, bikeRental, 5, SpringLayout.SOUTH, reloadButton);
		layout.putConstraint(SpringLayout.EAST, bikeRental, -5, SpringLayout.EAST, this);
		layout.putConstraint(SpringLayout.SOUTH, bikeRental, -5, SpringLayout.SOUTH, this);
	
		
		}
}