package com.ers.components.rental.controller;

import java.util.List;
import java.util.Map;

import javax.swing.JPanel;

import com.ers.bean.Bike;
import com.ers.bean.Deposit;
import com.ers.bean.Transaction;
import com.ers.components.abstractdata.controller.IDataSearchController;
import com.ers.components.rental.gui.BikeRental;
import com.ers.components.rental.gui.BikeRentalPane;
import com.ers.components.rental.gui.BikeRentalSearchPane;
import com.ers.components.rental.gui.ListBikeRental;
import com.ers.serverapi.BikeApi;
import com.ers.serverapi.DepositApi;
import com.ers.serverapi.TransactionApi;

public class RentalBikeController {

	private BikeRentalPane pagePane;

	public RentalBikeController() {
		BikeRentalSearchPane searchPane = createSearchPane();

		ListBikeRental listBikeRental = createCarRentalPane();

		searchPane.setController(new IDataSearchController() {
			@Override
			public void search(Map<String, String> searchParams) {
				List<Bike> list = RentalBikeController.this.search(searchParams);
				System.out.println(list);
				listBikeRental.updateData(list);
			}
		});

		searchPane.fireSearchEvent();

		pagePane = new BikeRentalPane(searchPane, listBikeRental);
	}

	public JPanel getDataPagePane() {
		return pagePane;
	}

	public BikeRentalSearchPane createSearchPane() {
		return new BikeRentalSearchPane();
	};

	public List<Bike> search(Map<String, String> searchParams) {
		return new BikeApi().getBike(searchParams);
	};

	public BikeRental createSinglePane() {
		return new BikeRental();
	};

	public ListBikeRental createCarRentalPane() {
		return new ListBikeRental(this);
	};

	/**
	 * create transaction for rental bike
	 */
	public Transaction createTransaction(Transaction transaction) {
		return new TransactionApi().createTransaction((Transaction) transaction);
	}

	/**
	 * Change status of bike when rental
	 */
	public Bike changeStatusBike(String bikeId, Bike bike) {
		return new BikeApi().updateBike(bikeId, bike);
	}

	/**
	 * Get infomation of deposit
	 */
	public Deposit getDeposit() {
		return new DepositApi().getDeposit();
	}

	/**
	 +	 * Is Exist Transaction Hiring
	 +	 */
	public boolean isExistTransaction() {
		Transaction transaction = new TransactionApi().getTransactionInHiring("user-001");
		return transaction != null;
	}
}
