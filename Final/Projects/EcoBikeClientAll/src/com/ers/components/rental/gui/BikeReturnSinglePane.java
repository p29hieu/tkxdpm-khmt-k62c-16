package com.ers.components.rental.gui;

import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BandCombineOp;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.ers.bean.Station;
import com.ers.bean.Transaction;
import com.ers.components.rental.controller.RentingBikeController;

@SuppressWarnings("serial")
public class BikeReturnSinglePane extends JPanel {
	protected Transaction t;

	protected GridBagLayout layout;
	protected GridBagConstraints c;

	private JPanel panel;

	private JLabel labelBikeId;
	private JLabel labelDeposit;
	private JLabel labelRefund;
	private JLabel labelStartedAt;

	private RentingBikeController controller;

	public BikeReturnSinglePane(RentingBikeController controller) {
		this.controller = controller;
		buildControls(controller);
	}

	public BikeReturnSinglePane(Transaction t, RentingBikeController controller) {
		this(controller);
		if(t!=null) {
			this.t = t;
			displayData();
		}
		else
		{
			layout = new GridBagLayout();
			this.setLayout(layout);
			c = new GridBagConstraints();
			c.fill = GridBagConstraints.HORIZONTAL;
			c.weightx = 1;

			int row = getLastRowIndex();
			c.gridx = 0;
			c.gridy = row;
			JLabel nullNotifLabel = new JLabel();
			add(nullNotifLabel, c);
			nullNotifLabel.setText("Ban chua thue xe nao");
		}
	}

	public void buildControls(RentingBikeController controller) {
		layout = new GridBagLayout();
		this.setLayout(layout);
		c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1;

		int row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelBikeId = new JLabel();
		add(labelBikeId, c);

		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelDeposit = new JLabel();
		add(labelDeposit, c);

		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelRefund = new JLabel();
		add(labelRefund, c);

		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelStartedAt = new JLabel();
		add(labelStartedAt, c);

		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		JButton button = new JButton("Tra xe");
		add(button, c);
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new ReturnDialog(t, controller);
			}
		});
	}

	public void displayData() {
		labelBikeId.setText("BikeId: " + t.getBikeId());
		labelDeposit.setText("Deposit: " + t.getDeposit());
		labelRefund.setText("Refund: " + t.getRefund());
		labelStartedAt.setText("Started at: " + controller.convertTime(t.getStartedAt()) + "");
	};

	public void addDataHandlingComponent(Component component) {
		if (panel == null) {
			int row = getLastRowIndex();
			c.gridx = 0;
			c.gridy = row;
			panel = new JPanel();
			this.add(panel, c);
			panel.setLayout(new FlowLayout(FlowLayout.LEFT));
		}

		panel.add(component);
	}

	public void updateData(Transaction t) {
		this.t = t;
		displayData();
	}

	public Transaction getData() {
		return this.t;
	}

	protected int getLastRowIndex() {
		layout.layoutContainer(this);
		int[][] dim = layout.getLayoutDimensions();
		int rows = dim[1].length;
		return rows;
	}

	
}
