package com.ers.components.rental.gui;

import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.List;
import java.util.Map;

import javax.swing.JLabel;
import javax.swing.JPanel;

import com.ers.bean.Bike;
import com.ers.components.abstractdata.controller.IDataSearchController;
import com.ers.serverapi.StationApi;

public class BikeRental extends JPanel {
	protected Bike bike;
	
	protected GridBagLayout layout;
	protected GridBagConstraints c;
	
	private JPanel panel;

	private JLabel labelName;
	private JLabel labelType;
	private JLabel labelWeight;
	private JLabel labelCost;
	private JLabel labelStatus;
	private JLabel labelProducer;
	
	public BikeRental() {
		buildControls();
	}
	
	public BikeRental(Bike bike) {
		this.bike = bike;
		
		displayData();
	}
	
	public void buildControls() {
		layout = new GridBagLayout();
		this.setLayout(layout);
		c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1;
		
		int row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelName = new JLabel();
		add(labelName, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelType = new JLabel();
		add(labelType, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelCost = new JLabel();
		add(labelCost, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelWeight = new JLabel();
		add(labelWeight, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelStatus = new JLabel();
		add(labelStatus, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelProducer = new JLabel();
		add(labelProducer, c);
	}
	
	public void displayData() {
		labelName.setText("Name: " + bike.getName());
		labelType.setText("Type: " + bike.getType());
		labelCost.setText("Cost: " + bike.getCost() + "");
		labelProducer.setText("Producer: " + bike.getProducer() + "");
		labelStatus.setText("Status: " + bike.getStatus() + "");
	};
	
	public void addDataHandlingComponent(Component component) {
		if (panel == null) {
			int row = getLastRowIndex();
			c.gridx = 0;
			c.gridy = row;
			panel = new JPanel();
			this.add(panel, c);
			panel.setLayout(new FlowLayout(FlowLayout.LEFT));
		}
		
		panel.add(component);
	}
	
	public void updateData(Bike bike) {
		this.bike = bike;
		displayData();
	}
	
	public Bike getData() {
		return this.bike;
	}
	
	protected int getLastRowIndex() {
		layout.layoutContainer(this);
		int[][] dim = layout.getLayoutDimensions();
	    int rows = dim[1].length;
	    return rows;
	}
}
