package com.ers.components.rental.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.Date;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.ers.bean.Bike;
import com.ers.bean.Deposit;
import com.ers.bean.Transaction;
import com.ers.components.abstractdata.controller.IDataRentalController;
import com.ers.components.rental.controller.RentalBikeController;

import javax.swing.JButton;

import static javax.swing.JOptionPane.showMessageDialog;

@SuppressWarnings("serial")
public class ListBikeRental extends JScrollPane {
    private LayoutManager layout;
    protected JPanel pane;

    protected RentalBikeController controller;

    public ListBikeRental() {
        pane = new JPanel();
        layout = new BoxLayout(pane, BoxLayout.Y_AXIS);
        pane.setLayout(layout);

        this.setViewportView(pane);
        this.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        this.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        this.getVerticalScrollBar().setUnitIncrement(20);
        this.getHorizontalScrollBar().setUnitIncrement(20);
    }

    public ListBikeRental(RentalBikeController controller) {
        this();
        this.controller = controller;
    }

    public void updateData(List<Bike> list) {
        pane.removeAll();
        pane.revalidate();
        pane.repaint();

        for (Bike t : list) {
            BikeRental bikeRental = controller.createSinglePane();

            bikeRental.updateData(t);
            this.decorateSinglePane(bikeRental);
            pane.add(bikeRental);
            pane.add(Box.createRigidArea(new Dimension(0, 40)));
        }
    }

    public void decorateSinglePane(BikeRental bikeRental) {
        JButton button = new JButton("Thue");
        bikeRental.addDataHandlingComponent(button);
        Bike bike = bikeRental.getData();
        String bikeId = bike.getId();
        String status = bike.getStatus();
        System.out.println("status: " + status);
        if (status.equals("EMPTY")) {
            IDataRentalController manageController = new IDataRentalController() {
                @Override
                public Transaction createTransaction(String cardId, String bikeId, String description, int deposit) {
                    long startedAt = new Date().getTime();
                    Transaction transaction = new Transaction(cardId, bikeId, description, deposit, 0, startedAt, 0, "HIRING");
                    return controller.createTransaction(transaction);
                }

                @Override
                public int getDeposit(String bikeType) {
                    Deposit deposit = controller.getDeposit();
                    switch (bikeType) {
                        case "SINGLE_BIKE": {
                            return deposit.getSingleBike();
                        }
                        case "SINGLE_E_BIKE": {
                            return deposit.getSingleEBike();
                        }
                        case "DOUBLE_BIKE": {
                            return deposit.getDoubleBike();
                        }
                        default:
                            return 0;
                    }
                }

				@Override
				public Bike changeStatusBike(String bikeId, Bike bike, String status) {
					bike.setStatus(status);
					return controller.changeStatusBike(bikeId, bike);
				}

                @Override
                public void updateData(Bike bike) {
                    button.setEnabled(false);
                    bikeRental.updateData(bike);
                }
            };
            button.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    Boolean isExistTransaction = controller.isExistTransaction();
                    if (isExistTransaction) {
                        showMessageDialog(null, "Bạn đang thue xe! Vui lòng trả xe trước khi tiếp tục");
                    } else {
                        Bike bikeResponse = manageController.changeStatusBike(bikeId, bike, "PENDING");

                        if (bikeResponse != null) {
                            System.out.println("bikeResponse: " + bikeResponse.getStatus());
                            new BikeRetalPaymentInput(bike, manageController);
                        } else {
                            showMessageDialog(null, "Có lỗi xảy ra! Không thể thue xe vui lòng thử lại sau");
                        }
                    }
                }
            });
        } else {
            button.setEnabled(false);
        }

    }
}
