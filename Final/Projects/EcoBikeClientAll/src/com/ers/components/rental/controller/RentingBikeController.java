package com.ers.components.rental.controller;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.ers.bean.Bike;
import com.ers.bean.Station;
import com.ers.components.rental.gui.BikeReturnListPane;
import com.ers.components.rental.gui.BikeReturnPagePane;
import com.ers.components.rental.gui.BikeReturnSinglePane;
import com.ers.serverapi.StationApi;
import com.ers.serverapi.TransactionApi;


public class RentingBikeController {
	private BikeReturnSinglePane singlePane;
	private BikeReturnListPane listPane;
	private BikeReturnPagePane pagePane;
	
	public RentingBikeController() {
		
		
		listPane = this.createListPane();
		listPane.updateData();
		pagePane = new BikeReturnPagePane(listPane, this);
		
	}
	
	public JPanel getDataPagePane() {
		return pagePane;
	}
	
	
	public BikeReturnSinglePane createSinglePane() {
		TransactionApi api = new TransactionApi();
		
		return new BikeReturnSinglePane(api.getTransactionInHiring("user-001"), this);
	};
	
	public Station updateStation(String typeBike, Station station) {
		station.setNumEmptyDock(station.getNumEmptyDock() - 1);
		switch (typeBike) {
		case "SINGLE_BIKE":
			station.setNumSingleBike(station.getNumSingleBike() + 1);
			break;
		case "DOUBLE_BIKE":
			station.setNumDoubleBike(station.getNumDoubleBike() + 1);
			break;
		case "SINGLE_E_BIKE":
			station.setNumSingleEBike(station.getNumSingleEBike() + 1);
			break;
		default:
			break;
		}
		
		new StationApi().updateStation(station.getId(), station);
		return station;
	}
	
	public String convertTime(long time){
		Date date = new Date(time);
		Format format = new SimpleDateFormat("yyyy MM dd HH:mm:ss");
		return format.format(date);
	}
	public void updateData() {
		listPane.updateData();
	}
	
	public BikeReturnListPane createListPane() {
		return new BikeReturnListPane(this);
	}
}
