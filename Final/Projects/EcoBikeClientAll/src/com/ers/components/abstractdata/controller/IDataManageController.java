//done
package com.ers.components.abstractdata.controller;

public interface IDataManageController<T> {
	public T create(T t);
	public T read(T t);
	public void delete(T t);
	public boolean update(T t);
}
