// done
package com.ers.components.abstractdata.gui;

import com.ers.bean.Bike;
import com.ers.bean.Station;
import com.ers.components.abstractdata.controller.IDataManageController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

@SuppressWarnings("serial")
public abstract class ADataAddBikeDialog<T> extends JDialog {
	protected T t;
	protected GridBagLayout layout;
	protected GridBagConstraints c = new GridBagConstraints();
	protected List<Station> listStation;
	private JFrame frame;
	protected String type;

	public ADataAddBikeDialog(IDataManageController<T> controller, List<Station> listStation,String type) {
		super((Frame) null, "Add", true);
		this.listStation= listStation;
		this.type = type;
		
		setContentPane(new JPanel());
		layout = new GridBagLayout();
		getContentPane().setLayout(layout);

		
		this.buildControls();
		
		JButton addButton = new JButton("Add");
		addButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {	
				T newT = getNewData();
				 Object[] options = {"Yes",
	                        "Cancel"};
	                int n = JOptionPane.showOptionDialog(frame,
	                        "Bạn có muốn thêm " + ((Bike) newT).getName()  +" vào "+ ((Bike) newT).getStationName(),
	                        "Xác nhận",
	                        JOptionPane.YES_NO_OPTION,
	                        JOptionPane.QUESTION_MESSAGE,
	                        null,
	                        options,
	                        options[1]);
	                if (n == JOptionPane.YES_OPTION) {
	                	if(((Bike)newT).checkStation(listStation)) {
	    				controller.create(newT);
	    				ADataAddBikeDialog.this.dispose();
	    				JOptionPane.showMessageDialog(new JFrame("Success"), "Them mới thanh cong");}
	                	else {
		    				JOptionPane.showMessageDialog(new JFrame("Failue"), ((Bike) newT).getStationName()+" đã hết chỗ");
						}
	                } else if (n == JOptionPane.NO_OPTION) {

	                } else {

	                	
	                }
				
			}
		});
		
		
		c.gridx = 1;
		c.gridy = getLastRowIndex();
		getContentPane().add(addButton, c);
		
		
		this.pack();
		this.setResizable(false);
		this.setVisible(true);
	}
	
	protected int getLastRowIndex() {
		layout.layoutContainer(getContentPane());
		int[][] dim = layout.getLayoutDimensions();
	    int rows = dim[1].length;
	    return rows;
	}
	
	public abstract void buildControls();
	
	public abstract T getNewData();
}
