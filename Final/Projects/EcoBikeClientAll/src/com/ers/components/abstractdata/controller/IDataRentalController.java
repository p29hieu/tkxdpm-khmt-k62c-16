package com.ers.components.abstractdata.controller;

import com.ers.bean.Bike;
import com.ers.bean.Transaction;

public interface IDataRentalController {
    public Transaction createTransaction(String cardId, String bikeId, String description, int deposit);

    public int getDeposit(String bikeType);

    public Bike changeStatusBike(String bikeId, Bike bike, String status);

    public void updateData(Bike bike);
}
