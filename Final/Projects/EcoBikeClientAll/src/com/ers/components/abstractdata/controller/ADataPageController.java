//init
package com.ers.components.abstractdata.controller;

import java.util.List;
import java.util.Map;

import javax.swing.*;

import com.ers.bean.Bike;
import com.ers.bean.Station;
import com.ers.components.abstractdata.gui.ADataListPane;
import com.ers.components.abstractdata.gui.ADataPagePane;
import com.ers.components.abstractdata.gui.ADataSearchPane;
import com.ers.components.abstractdata.gui.ADataSinglePane;
import com.ers.serverapi.BikeApi;
import com.ers.serverapi.StationApi;

public abstract class ADataPageController<T> {
	private ADataPagePane<T> pagePane;

	private JDialog dialog;
	private ADataListPane<T> listPane;

	public ADataPageController() {
	}


	public void init(){
		ADataSearchPane searchPane = createSearchPane();

		listPane = createListPane();

		if(searchPane!=null) {
			searchPane.setController(new IDataSearchController() {
				@Override
				public void search(Map<String, String> searchParams) {
					List<? extends T> list = ADataPageController.this.search(searchParams);
					listPane.updateListData(list);
				}
			});

			searchPane.fireSearchEvent();
		}

		pagePane = new ADataPagePane<T>(searchPane, listPane);
	}
	
	public JPanel getDataPagePane() {
		return pagePane;
	}

	public ADataListPane<T> getListPane() {
		return listPane;
	}

	public void setListPane(ADataListPane<T> listPane) {
		this.listPane = listPane;
	}
	
	public abstract ADataSearchPane createSearchPane();

	public abstract List<? extends T> search(Map<String, String> searchParams);

	
	public abstract ADataSinglePane<T> createSinglePane();
	
	public abstract ADataListPane<T> createListPane();


	public void showDialog() {
		dialog.setVisible(true);
	}

	public void hideDialog() {
		dialog.setVisible(false);
	}

	public JDialog getDialog() {
		return dialog;
	}

	public void setDialog(JDialog dialog) {
		this.dialog = dialog;
		dialog.setVisible(false);
	}

	public void updateDataListPane(T t){
		listPane.updateData(t);
	}
	public List<Station> getListStation(Map<String, String> searchParams) {
		return new StationApi().getStations(searchParams);
	};

	public Bike create(Bike bike) {
		return new BikeApi().create(bike);
	}
}
