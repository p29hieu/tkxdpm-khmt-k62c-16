// done
package com.ers.components.abstractdata.gui;

import com.ers.components.abstractdata.controller.IDataManageController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@SuppressWarnings("serial")
public abstract class ADataAddDialog<T> extends JDialog {
	protected T t;
	protected GridBagLayout layout;
	protected GridBagConstraints c = new GridBagConstraints();

	public ADataAddDialog(IDataManageController<T> controller) {
		super((Frame) null, "Add", true);
		
		
		setContentPane(new JPanel());
		layout = new GridBagLayout();
		getContentPane().setLayout(layout);

		
		this.buildControls();
		
		JButton addButton = new JButton("Add");
		addButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				T newT = getNewData();
				
				if (newT != null) {
					controller.create(newT);
					ADataAddDialog.this.dispose();
				}
				else {
					JOptionPane.showMessageDialog(null, "Invalid input data", "Form validation", JOptionPane.INFORMATION_MESSAGE);
				}
			}
		});
		
		
		c.gridx = 1;
		c.gridy = getLastRowIndex();
		getContentPane().add(addButton, c);
		
		
		this.pack();
		this.setResizable(false);
		this.setVisible(true);
	}
	
	protected int getLastRowIndex() {
		layout.layoutContainer(getContentPane());
		int[][] dim = layout.getLayoutDimensions();
	    int rows = dim[1].length;
	    return rows;
	}
	
	public abstract void buildControls();
	
	public abstract T getNewData();
}
