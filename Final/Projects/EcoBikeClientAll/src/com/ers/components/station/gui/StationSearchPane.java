package com.ers.components.station.gui;

import java.util.HashMap;
import java.util.Map;

import javax.swing.JLabel;
import javax.swing.JTextField;

import com.ers.components.abstractdata.gui.ADataSearchPane;

@SuppressWarnings("serial")
public class StationSearchPane extends ADataSearchPane {
	private JTextField nameField;
	private JTextField addressField;
	
	public StationSearchPane() {
		super();
	}
	
	@Override
	public void buildControls() {
		JLabel nameLabel = new JLabel("Ten");
		nameField = new JTextField(15);
		int row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(nameLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(nameField, c);
		
		
		JLabel addressLabel = new JLabel("Dia chi");
		addressField = new JTextField(15);
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(addressLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(addressField, c);
	};
	
	@Override
	public Map<String, String> getQueryParams() {
		Map<String, String> res = new HashMap<String, String>();
		
		if (!nameField.getText().trim().equals("")) {
			res.put("name", nameField.getText().trim());
		}
		if (!addressField.getText().trim().equals("")) {
			res.put("address", addressField.getText().trim());
		}
		
		return res;
	}
}
