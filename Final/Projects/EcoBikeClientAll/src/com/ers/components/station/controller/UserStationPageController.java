package com.ers.components.station.controller;

import java.util.List;
import java.util.Map;

import com.ers.bean.Station;
import com.ers.components.abstractdata.controller.ADataPageController;
import com.ers.components.abstractdata.gui.ADataListPane;
import com.ers.components.abstractdata.gui.ADataSinglePane;
import com.ers.components.station.gui.UserStationListPane;
import com.ers.components.station.gui.StationSearchPane;
import com.ers.components.station.gui.StationSinglePane;
import com.ers.serverapi.StationApi;

public class UserStationPageController extends ADataPageController<Station> {	
	public UserStationPageController() {
		super();
		super.init();
	}
	
	@Override
	public StationSearchPane createSearchPane() {
		return new StationSearchPane();
	};

	@Override
	public List<Station> search(Map<String, String> searchParams) {
		return new StationApi().getStations(searchParams);
	};
	
	@Override
	public ADataSinglePane<Station> createSinglePane() {
		return new StationSinglePane();
	};
	
	@Override
	public ADataListPane<Station> createListPane() {
		return new UserStationListPane(this);
	};
}
