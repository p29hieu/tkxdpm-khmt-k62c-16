package com.ers.components.station.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JLabel;

import com.ers.bean.Station;
import com.ers.components.abstractdata.gui.ADataSinglePane;

@SuppressWarnings("serial")
public class StationSinglePane extends ADataSinglePane<Station> {
	private JLabel labelName;
	private JLabel labelAddress;
	private JLabel labelBikes;
	private JLabel labelEBikes;
	private JLabel labelTwinBikes;
	private JLabel labelEmptyDocks;
	
	public StationSinglePane() {
		super();
	}
	
	public StationSinglePane(Station t) {
		this();
		this.t = t;
		
		displayData();
	}
	
	public void buildControls() {
		super.buildControls();
		layout = new GridBagLayout();
		this.setLayout(layout);
		c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1;
		
		int row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelName = new JLabel();
		add(labelName, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelAddress = new JLabel();
		add(labelAddress, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelBikes = new JLabel();
		add(labelBikes, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelEBikes = new JLabel();
		add(labelEBikes, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelTwinBikes = new JLabel();
		add(labelTwinBikes, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelEmptyDocks = new JLabel();
		add(labelEmptyDocks, c);
	}
	
	@Override
	public void displayData() {
		labelName.setText("Name: " + t.getName());
		labelAddress.setText("Address: " + t.getAddress());
		labelBikes.setText("Number of bikes: " + t.getNumSingleBike() + "");
		labelEBikes.setText("Number of ebikes: " + t.getNumSingleEBike() + "");
		labelTwinBikes.setText("Number of twin bikes: " + t.getNumDoubleBike() + "");
		labelEmptyDocks.setText("Number of empty docks: " + t.getNumEmptyDock() + "");
	};
	
}
