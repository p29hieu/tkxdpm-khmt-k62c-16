//done
package com.ers.components.bike;

import com.ers.bean.Bike;
import com.ers.bean.Station;
import com.ers.components.abstractdata.controller.ADataPageController;
import com.ers.components.abstractdata.gui.ADataListPane;
import com.ers.components.abstractdata.gui.ADataSearchPane;
import com.ers.components.abstractdata.gui.ADataSinglePane;
import com.ers.components.admin.editbike.EditBikeController;
import com.ers.components.admin.station.AdminStationPageController;
import com.ers.serverapi.BikeApi;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class BikePageController extends ADataPageController<Bike> {
    private EditBikeController editBikeController;
    private final AdminStationPageController adminStationPageController;

    public BikePageController(AdminStationPageController adminStationPageController) {
        this.adminStationPageController = adminStationPageController;
        super.init();
    }

    public void setController(EditBikeController controller) {
        this.editBikeController = controller;
    }

    public ArrayList<Station> getListStation() {
        return null;
    }

    @Override
    public ADataListPane<Bike> createListPane() {
        return new BikeListPane(this, "ADMIN", adminStationPageController);
    }

    public void update(Bike bike) {
        editBikeController.update(bike);
    }

    @Override
    public ADataSearchPane createSearchPane() {
        return new BikeSearchPane();
    }

    @Override
    public List<? extends Bike> search(Map<String, String> searchParams) {
        return new BikeApi().getBikes(searchParams);
    }

    @Override
    public ADataSinglePane<Bike> createSinglePane() {
        return new BikeSinglePane();
    }

}
