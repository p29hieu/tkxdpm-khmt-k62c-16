//done
package com.ers.components.bike;

import com.ers.bean.Bike;
import com.ers.bean.Station;
import com.ers.components.abstractdata.gui.ADataListPane;
import com.ers.components.abstractdata.gui.ADataSinglePane;
import com.ers.components.admin.addbike.AddBikeTypeDialog;
import com.ers.components.admin.editbike.EditBikeController;
import com.ers.components.admin.editbike.EditBikeDialog;
import com.ers.components.admin.station.AdminStationPageController;
import com.ers.constant.StatusBike;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;

@SuppressWarnings("serial")
public class BikeListPane extends ADataListPane<Bike> {
    private final String role;
    private final AdminStationPageController adminStationPageController;

    public BikeListPane(BikePageController controller, String role, AdminStationPageController adminStationPageController) {
        this.adminStationPageController = adminStationPageController;
        this.controller = controller;
        this.role = role;

        this.button = new JButton("Them xe");


        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {


				new AddBikeTypeDialog(controller,BikeListPane.this,adminStationPageController);
			}
		});
		pane.add(button);

    }

    @Override
    public void decorateSinglePane(ADataSinglePane<Bike> singlePane) {
        if (this.role.equals("ADMIN")) {
            if (singlePane.getData() != null && singlePane.getData().getStatus().equals(StatusBike.EMPTY)) {
                return;
            }
            JButton editButton = new JButton();
            editButton.setText("Edit");
            editButton.addActionListener(showEditDialog(singlePane));
            singlePane.addDataHandlingComponent(editButton);
        }
    }

    private ActionListener showEditDialog(ADataSinglePane<Bike> singlePane) {
        return e -> {
            if (controller instanceof BikePageController) {
                ArrayList<Station> listStation = (ArrayList<Station>) adminStationPageController.getListPane().getListData();
                if (listStation == null || listStation.size() == 0) {
                    adminStationPageController.search(new HashMap<String, String>());
                    // get list station
//                    Station stemp = new Station();
//                    listStation = new ArrayList<>();
//                    stemp.setId("station 1");
//                    stemp.setName("STATION NAME 1");
//                    listStation.add(stemp.clone());
//                    stemp.setId("station 2");
//                    stemp.setName("STATION NAME 2");
//                    listStation.add(stemp);

                    listStation = (ArrayList<Station>) adminStationPageController.getListPane().getListData();
                }
                EditBikeController editBikeController = new EditBikeController(singlePane.getData(), this);
                JDialog editDialog = new EditBikeDialog(
                        singlePane.getData(),
                        editBikeController,
                        listStation
                );
                controller.setDialog(
                        editDialog
                );
                controller.showDialog();
            }
        };
    }

//    @Override
//    public void updateData(Bike bike) {
//        for (int i = 0; i < listData.size(); i++) {
//            Bike temp = listData.get(i);
//            if (temp.equals(bike)) {
//                ADataSinglePane<Bike> singlePane = controller.createSinglePane();
//                decorateSinglePane(singlePane);
//                singlePane.updateData(bike);
//                pane.remove(i);
//                pane.add(singlePane, i);
//            }
//        }
//        pane.revalidate();
//        pane.repaint();
//
//    }
//public void updateData(Bike t) {
//    pane.removeAll();
//    pane.revalidate();
//    pane.repaint();
//    pane.add(button);
//
//    for (Bike t1 : listData) {
//        ADataSinglePane<Bike> singlePane = controller.createSinglePane();
//        decorateSinglePane(singlePane);
//        if (t1.equals(t))
//            singlePane.updateData(t);
//        else singlePane.updateData(t1);
//        pane.add(singlePane);
//        pane.add(Box.createRigidArea(new Dimension(0, 40)));
//    }
//}
}
