//done
package com.ers.serverapi;

import com.ers.bean.Deposit;
import com.ers.bean.Rental;

import javax.ws.rs.client.*;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class PaymentApi {
    public static final String PATH = "http://localhost:8080/";

    private Client client;

    public PaymentApi() {
        client = ClientBuilder.newClient();
    }

    public Rental getRental() {
        WebTarget webTarget = client.target(PATH).path("payment/rental");

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();

        Rental res = response.readEntity(new GenericType<Rental>() {
        });
        System.out.println("res = " + res);
        return res;
    }

    public Deposit getDeposit() {
        WebTarget webTarget = client.target(PATH).path("payment/deposit");

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();

        Deposit res = response.readEntity(new GenericType<Deposit>() {
        });
        System.out.println("res = " + res);
        return res;
    }

    public Rental updateRental(Rental rental) {
        WebTarget webTarget = client.target(PATH).path("payment/rental");
        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.post(Entity.entity(rental, MediaType.APPLICATION_JSON));

        Rental res = response.readEntity(Rental.class);
        return res;
    }

    public Deposit updateDeposit(Deposit deposit) {
        WebTarget webTarget = client.target(PATH).path("payment/deposit");
        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.post(Entity.entity(deposit, MediaType.APPLICATION_JSON));

        Deposit res = response.readEntity(Deposit.class);
        return res;
    }
}
