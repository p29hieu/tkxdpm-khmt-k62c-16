//done
package com.ers.serverapi;

import java.util.ArrayList;
import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ers.bean.Station;

public class StationApi {
	public static final String PATH = "http://localhost:8080/";
	
	private Client client;
	
	public StationApi() {
		client = ClientBuilder.newClient();
	}
	
	public ArrayList<Station> getStations(Map<String, String> queryParams) {
		WebTarget webTarget = client.target(PATH).path("stations/get/");
		
		
		if (queryParams != null) {
			for (String key : queryParams.keySet()) {
				String value = queryParams.get(key);
				webTarget = webTarget.queryParam(key, value);
			}
		}
		

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		ArrayList<Station> res = response.readEntity(new GenericType<ArrayList<Station>>() {});
		System.out.println(res);
		return res;
	}

	public ArrayList<Station> getAllStations() {
		WebTarget webTarget = client.target(PATH).path("stations");

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		ArrayList<Station> res = response.readEntity(new GenericType<ArrayList<Station>>() {
		});
		System.out.println(res);
		return res;
	}
	
	public void updateStation(String station_id, Station station) {
		WebTarget webTarget = client.target(PATH).path("stations/update").path(station_id);
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(Entity.entity(station, MediaType.APPLICATION_JSON));
		System.out.println(response);
	}

	public Station create(Station station) {
		WebTarget webTarget = client.target(PATH).path("stations");

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(Entity.entity(station, MediaType.APPLICATION_JSON));

		Station res = response.readEntity(Station.class);

		return res;
	}

	public Station getOneStation(String stationID) {
		WebTarget webTarget = client.target(PATH).path("stations").path(stationID);
		webTarget = webTarget.queryParam("id", stationID);
		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		ArrayList<Station> res = response.readEntity(new GenericType<ArrayList<Station>>() {});
		System.out.println(res);
		return res.get(0);
	}
}
