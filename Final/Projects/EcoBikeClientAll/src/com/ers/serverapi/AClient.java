package com.ers.serverapi;


import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import java.util.Map;


public abstract class AClient<T> {
    public static final String PATH = "http://localhost:8081/";

    private Client client;

    public AClient() {
        client = ClientBuilder.newClient();
    }

    public abstract T[] getList();

    public abstract T getOne(T t);

    public abstract T search(Map<String, String> params);

    public abstract T update(String id, T t);
}
