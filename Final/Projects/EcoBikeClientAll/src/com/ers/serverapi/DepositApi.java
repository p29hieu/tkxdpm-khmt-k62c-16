//done
package com.ers.serverapi;

import java.util.ArrayList;
import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ers.bean.Bike;
import com.ers.bean.Deposit;
import com.ers.bean.Transaction;

public class DepositApi {
    public static final String PATH = "http://localhost:8080/";

    private Client client;

    public DepositApi() {
        client = ClientBuilder.newClient();
    }

    public Deposit getDeposit() {
        WebTarget webTarget = client.target(PATH).path("payment/deposit");
        Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();
        System.out.println(response);
        Deposit res = response.readEntity(new GenericType<Deposit>() {});
        return res;
    }
}
