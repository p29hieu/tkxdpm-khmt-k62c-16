//done
package com.ers.serverapi;

import java.util.ArrayList;
import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ers.bean.Bike;

public class BikeApi {
    public static final String PATH = "http://localhost:8080/";

    private static Client client;

    public BikeApi() {
        if (client == null) {
            client = ClientBuilder.newClient();
        }
    }

    public ArrayList<Bike> getBike(Map<String, String> queryParams) {
        WebTarget webTarget = client.target(PATH).path("bikes/get");

        if (queryParams != null) {
            for (String key : queryParams.keySet()) {
                String value = queryParams.get(key);
                webTarget = webTarget.queryParam(key, value);
            }
        }

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();
        System.out.println(response);
        if (response.getStatus() == 200) {
            return response.readEntity(new GenericType<ArrayList<Bike>>() {});
        }
        return new ArrayList<Bike>();
    }

    public Bike create(Bike bike) {
        WebTarget webTarget = client.target(PATH).path("bikes");

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.post(Entity.entity(bike, MediaType.APPLICATION_JSON));

        Bike res = response.readEntity(Bike.class);
        return res;
    }

    public Bike updateBike(String bike_id, Bike bike) {
        WebTarget webTarget = client.target(PATH).path("bikes/" + bike_id);
        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.post(Entity.entity(bike, MediaType.APPLICATION_JSON));
        if (response.getStatus() == 200) {
            return response.readEntity(new GenericType<Bike>() {
            });
        }
        return null;
    }

    public ArrayList<Bike> getAll() {
        WebTarget webTarget = client.target(PATH).path("bikes");

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();

        ArrayList<Bike> res = response.readEntity(new GenericType<ArrayList<Bike>>() {
        });
        System.out.println(res);
        return res;
    }

    public ArrayList<Bike> getBikes(Map<String, String> queryParams) {
        WebTarget webTarget = client.target(PATH).path("bikes");
        if (queryParams != null) {
            for (String key : queryParams.keySet()) {
                String value = queryParams.get(key);
                webTarget = webTarget.queryParam(key, value);
            }
        }
        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();
        System.out.print(response.toString());
        ArrayList<Bike> res = response.readEntity(new GenericType<ArrayList<Bike>>() {
        });
        System.out.println(res.toString());
        return res;
    }

}
