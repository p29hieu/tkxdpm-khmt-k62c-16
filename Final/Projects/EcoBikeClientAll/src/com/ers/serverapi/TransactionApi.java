package com.ers.serverapi;

import java.util.ArrayList;
import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ers.bean.Transaction;

import javax.ws.rs.client.Entity;;

public class TransactionApi {
	public static final String PATH = "http://localhost:8080/";
	
	private Client client;
	
	public TransactionApi() {
		client = ClientBuilder.newClient();
	}
	
	public Transaction createTransaction(Transaction transaction) {
		WebTarget webTarget = client.target(PATH).path("payment/transaction/");
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(Entity.entity(transaction, MediaType.APPLICATION_JSON));
		
		Transaction res = response.readEntity(Transaction.class);
		return res;
	}
	
	public Transaction getTransactionInHiring(String user_id) {
		WebTarget webTarget = client.target(PATH).path("payment/transaction/"+user_id);
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		System.out.println(response);
		ArrayList<Transaction> res = response.readEntity(new GenericType<ArrayList<Transaction>>() {});
		System.out.println(res.size());
		if(res.size()==0) { return null;}
		return res.get(0);
	}
	
	public Long getRental(Map<String, Integer> queryParams) {
		WebTarget webTarget = client.target(PATH).path("payment/getrental");
		
		if (queryParams != null) {
			for (String key : queryParams.keySet()) {
				Integer value = queryParams.get(key);
				webTarget = webTarget.queryParam(key, value);
			}
		}
		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		Long res = response.readEntity(new GenericType<Long>() {});
		return res;
		}
	
	public void updateTransaction(Transaction transaction) {
		WebTarget webTarget = client.target(PATH).path("payment/transaction/update");
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(Entity.entity(transaction, MediaType.APPLICATION_JSON));
		System.out.println(response);
	}
	
}
