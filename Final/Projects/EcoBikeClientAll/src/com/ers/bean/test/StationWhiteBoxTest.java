package com.ers.bean.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.ers.bean.Station;

public class StationWhiteBoxTest {
	private Station station = new Station("1", "Bach Khoa", "1 Dai Co Viet");
	
	@Test
	public void testMatchStation1() {
		Station station = new Station("1", "What", "Where");
		assertTrue("Station Match", this.station.match(station));
	}
	
	@Test
	public void testMatchStation2() {
		Station station = new Station("2", "Xay Dung", "");
		assertTrue("Station Match", !this.station.match(station));
	}
}
