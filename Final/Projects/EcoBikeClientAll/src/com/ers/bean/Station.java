package com.ers.bean;
// done
import java.util.Map;

public class Station {

    private String id;

    private String name;
    private String address;
    private double longitude;
    private double latitude;
    private int numSingleBike = 0;
    private int numSingleEBike = 0;
    private int numDoubleBike = 0;
    private int numEmptyDock;


    // generate from json

    public Station(String id, String name, String address, double longitude, double latitude, int numSingleBike, int numSingleEBike, int numDoubleBike, int numEmptyDock) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.longitude = longitude;
        this.latitude = latitude;
        this.numSingleBike = numSingleBike;
        this.numSingleEBike = numSingleEBike;
        this.numDoubleBike = numDoubleBike;
        this.numEmptyDock = numEmptyDock;
    }

    public Station(String id, String name, String address) {
        this.id = id;
        this.name = name;
        this.address = address;
    }

    public Station() {

    }

    static Station fromJson(Map<String, String> json) {
        return new Station(
                json.get("id"),
                json.get("name"),
                json.get("name"),
                Double.parseDouble(json.get("longitude")),
                Double.parseDouble(json.get("latitude")),
                Integer.parseInt(json.get("numSingleBike")),
                Integer.parseInt(json.get("numSingleEBike")),
                Integer.parseInt(json.get("numDoubleBike")),
                Integer.parseInt(json.get("numEmptyDock"))
        );
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public int getNumSingleBike() {
        return numSingleBike;
    }

    public void setNumSingleBike(int numSingleBike) {
        this.numSingleBike = numSingleBike;
    }

    public int getNumSingleEBike() {
        return numSingleEBike;
    }

    public void setNumSingleEBike(int numSingleEBike) {
        this.numSingleEBike = numSingleEBike;
    }

    public int getNumDoubleBike() {
        return numDoubleBike;
    }

    public void setNumDoubleBike(int numDoubleBike) {
        this.numDoubleBike = numDoubleBike;
    }

    public int getNumEmptyDock() {
        return numEmptyDock;
    }

    public void setNumEmptyDock(int numEmptyDock) {
        this.numEmptyDock = numEmptyDock;
    }

	public boolean match(Station station) {
		if (station == null)
			return true;

		if( this.id.equals(station.getId())) return true;

		if (station.address != null && !station.address.equals("") && !this.address.contains(station.address)) {
			return false;
		}

		if (station.name != null && !station.name.equals("") && !this.name.contains(station.name)) {
			return false;
		}

		return true;
	}
	public String toString(){
        return this.getName();
    }

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Station) {
			return this.id.equals(((Station) obj).id);
		}
		return false;
	}
    public Station clone() {
        Station clone = new Station();
        clone.setName(this.getName());
        clone.setId(this.id);
        clone.setAddress(this.address);
        clone.setLatitude(this.latitude);
        clone.setLongitude(this.longitude);
        clone.setNumDoubleBike(this.numDoubleBike);
        clone.setNumEmptyDock(this.numEmptyDock);
        clone.setNumSingleBike(this.numSingleBike);
        clone.setNumSingleEBike(this.numSingleEBike);
        return clone;
    }
}
