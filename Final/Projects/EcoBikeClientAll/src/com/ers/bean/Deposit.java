package com.ers.bean;
// done
public class Deposit {
    private int singleBike = 400000;
    private int singleEBike = 700000;
    private int doubleBike = 550000;

    public Deposit() {
    }

    public void setDeposit(int singleBike, int singleEBike, int doubleBike) {
        this.singleBike = singleBike;
        this.singleEBike = singleEBike;
        this.doubleBike = doubleBike;
    }

    public int getSingleBike() {
        return singleBike;
    }

    public void setSingleBike(int singleBike) {
        this.singleBike = singleBike;
    }

    public int getSingleEBike() {
        return singleEBike;
    }

    public void setSingleEBike(int singleEBike) {
        this.singleEBike = singleEBike;
    }

    public int getDoubleBike() {
        return doubleBike;
    }

    public void setDoubleBike(int doubleBike) {
        this.doubleBike = doubleBike;
    }

    @Override
    public String toString() {
        return "SingleBike: " + this.singleBike + ", SingleEBike: " + this.singleEBike + ", DoubleBike: " + this.doubleBike;
    }
}
