package com.ers.bean;
// done
//enum String {SINGLE_BIKE, SINGLE_E_BIKE, DOUBLE_BIKE, DOUBLE_E_BIKE}

//enum String {EMPTY, PENDING, HIRING}

import com.ers.constant.StatusBike;
import com.ers.constant.TypeBike;

import java.util.List;
import java.util.Objects;

public class Bike {
    private String id;
    private String stationId;
    private String stationName;
    private String name;
    private String type; //{SINGLE_BIKE, SINGLE_E_BIKE, DOUBLE_BIKE, DOUBLE_E_BIKE}
    private double weight;
    private String licensePlate;
    private long manufacturingDate;
    private String producer;
    private long cost;
    private String status = "EMPTY"; //{EMPTY, PENDING, HIRING}
    private double estimated = 0;
    private double batteryPercentage = 0;
    private int loadCycles = 0;


    public Bike() {
    }

    public Bike(String id, String stationId, String stationName, String name, String type, double weight, String licensePlate, long manufacturingDate, String producer, long cost, String status, double estimated, double batteryPercentage, int loadCycles) {
        this.id = id;
        this.stationId = stationId;
        this.stationName = stationName;
        this.name = name;
        this.type = type;
        this.weight = weight;
        this.licensePlate = licensePlate;
        this.manufacturingDate = manufacturingDate;
        this.producer = producer;
        this.cost = cost;
        this.status = status;
        this.estimated = estimated;
        this.batteryPercentage = batteryPercentage;
        this.loadCycles = loadCycles;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Bike)) return false;
        Bike bike = (Bike) o;
        return id.equals(bike.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public double getBatteryPercentage() {
        return batteryPercentage;
    }

    public void setBatteryPercentage(double batteryPercentage) {
        this.batteryPercentage = batteryPercentage;
    }

    public long getLoadCycles() {
        return loadCycles;
    }

    public void setLoadCycles(int loadCycles) {
        this.loadCycles = loadCycles;
    }

    public double getEstimated() {
        return estimated;
    }

    public void setEstimated(double estimated) {
        this.estimated = estimated;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStationId() {
        return stationId;
    }

    public void setStationId(String stationId) {
        this.stationId = stationId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public long getManufacturingDate() {
        return manufacturingDate;
    }

    public void setManufacturingDate(long manufacturingDate) {
        this.manufacturingDate = manufacturingDate;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public long getCost() {
        return cost;
    }

    public void setCost(long cost) {
        this.cost = cost;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean match(Bike bike) {
        return this.id.indexOf(bike.id) != -1 ? true : false;
    }

    public boolean isValidateToUpdate(Bike bike) {
        System.out.println("0");
        if (bike.getId() == null || bike.getId().trim().equals("")) return false;

        System.out.println("1");
        if (!this.id.equals(bike.getId())) return false;

        System.out.println("2");
        String[] excludeStatus = {StatusBike.HIRING, StatusBike.PENDING};
        for (String status : excludeStatus) {
            if (status.equals(this.status)) {
                return false;
            }
        }

        System.out.println("3");
        if (bike.getCost() < 0) return false;

        System.out.println("4");
        if (bike.getName()==null || bike.getName().trim().equals("")) return false;

        System.out.println("5");
        if (bike.getWeight() <= 0) return false;

        System.out.println("6");
        if (this.getType().equals(TypeBike.SINGLE_E_BIKE) || this.getType().equals(TypeBike.DOUBLE_E_BIKE)) {

            System.out.println("6.1");
            if (bike.getBatteryPercentage() < 0 || bike.getBatteryPercentage() > 100) return false;

            System.out.println("6.2");
            if (bike.getLoadCycles() < 0) return false;

            System.out.println("6.3");
            if (bike.getEstimated() < 0) return false;
        }

        System.out.println("7");
        if (bike.getStationId() == null || bike.getStationId().trim().equals("")) return false;

        System.out.println("8");
        if (bike.getStationName() == null || bike.getStationName().trim().equals("")) return false;

        return true;
    }
    public boolean checkStation(List<Station> listStation) {
    	for(int i=0;i<listStation.size();i++) {
			if(this.stationId.equals(listStation.get(i).getId())) {
				if(listStation.get(i).getNumEmptyDock()>0) return true;
				else {
					return false;
				}
			}
		}
		return false;
    	
    }

    @Override
    public String toString() {
        return "Bike{" +
                "id='" + id + '\'' +
                ", stationId='" + stationId + '\'' +
                ", stationName='" + stationName + '\'' +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", weight=" + weight +
                ", licensePlate='" + licensePlate + '\'' +
                ", manufacturingDate=" + manufacturingDate +
                ", producer='" + producer + '\'' +
                ", cost=" + cost +
                ", status='" + status + '\'' +
                ", estimated=" + estimated +
                ", batteryPercentage=" + batteryPercentage +
                ", loadCycles=" + loadCycles +
                "}\n";
    }
}

