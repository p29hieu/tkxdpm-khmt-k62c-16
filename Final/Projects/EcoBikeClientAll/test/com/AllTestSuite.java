package com;

import com.ecoadmin.controller.ControllerTestSuite;
import com.ecoapp.bean.BeanTestSuite;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({BeanTestSuite.class, ControllerTestSuite.class})
public class AllTestSuite {
}
