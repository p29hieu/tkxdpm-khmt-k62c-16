package com.ecoapp.bean.station.equals;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ BlackBox.class, WhiteBox.class })
public class TestSuite {
}
