package com.ecoapp.bean.station.match;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ StationBlackBoxTest.class, StationWhiteBoxTest.class })
public class TestSuite {
}
