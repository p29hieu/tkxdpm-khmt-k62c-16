package com.ecoapp.bean.station.equals;

import com.ers.bean.Bike;
import com.ers.bean.Station;
import com.ers.constant.StatusBike;
import com.ers.constant.TypeBike;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class WhiteBox {
	private Station station = new Station("station-001","Bai xe 001", "Hai Ba Trung, Ha Noi", 200, 200, 20, 20, 20, 10);
	private Object obj;
    private boolean expected;

    public WhiteBox(Object obj, boolean expected) throws Exception {
        super();
        this.obj = obj;
        this.expected = expected;
    }


    @Parameterized.Parameters
    public static Collection<Object[]> primeNumbers() {
    	Bike bike = new Bike();
    	
    	Station station1 = new Station();
    	station1.setId("Le-Minh-Duc");
    	
    	Station station2 = new Station();
    	station2.setId("station-001");
    	
        return Arrays.asList(new Object[][]{
                {bike, false},
                {station1, false},
                {station2, true},
        });
    }

    @Test
    public void equalStation() {
        assertEquals("Equal Station incorrect", expected, station.equals(obj));
    }

}
