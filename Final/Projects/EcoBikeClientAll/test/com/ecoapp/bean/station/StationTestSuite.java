package com.ecoapp.bean.station;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({com.ecoapp.bean.station.match.TestSuite.class, com.ecoapp.bean.station.equals.TestSuite.class})
public class StationTestSuite {
}
