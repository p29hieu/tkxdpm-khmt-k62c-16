package com.ecoapp.bean.station.match;

import org.junit.Test;
import com.ers.bean.Station;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;


@RunWith(Parameterized.class)
public class StationBlackBoxTest {
	private Station station = new Station("1", "Bach Khoa", "1 Dai Co Viet");
	private Station stationTest ;
	private boolean expected;

	public StationBlackBoxTest(Station obj, boolean expected) throws Exception {
		super();
		this.stationTest = obj;
		this.expected = expected;
	}

	@Parameterized.Parameters
	public static Collection<Object[]> primeNumbers() {
		return Arrays.asList(new Object[][]{
				{new Station("1", "What", "Where"), true},
				{new Station("2", "Bach", ""), true},
				{ new Station("2", "Xay Dung", ""), false},
				{ new Station("2", "Khoa", "2"), false},
				{new Station("2", "Dung", "2"), false}
		});
	}
//	@Test
//	public void testMatchStation1() {
//		Station station = new Station("1", "What", "Where");
//		assertTrue("Station Match", this.station.match(station));
//	}
	
//	@Test
//	public void testMatchStation2() {
//		Station station = new Station("2", "Bach", "");
//		assertTrue("Station Match", this.station.match(station));
//	}
	
//	@Test
//	public void testMatchStation3() {
//		Station station = new Station("2", "Xay Dung", "");
//		assertTrue("Station Match", !this.station.match(station));
//	}
//
//	@Test
//	public void testMatchStation4() {
//		Station station = new Station("2", "Khoa", "2");
//		assertTrue("Station Match", !this.station.match(station));
//	}
	
//	@Test
//	public void testMatchStation5() {
//		Station station = new Station("2", "Dung", "2");
//		assertTrue("Station Match", !this.station.match(station));
//	}


	@Test
	public void testIsValidToUpdate() {
		assert this.station.match(stationTest) == this.expected;
	}
	
}
