package com.ecoapp.bean.station.match;

import org.junit.Test;
import com.ers.bean.Station;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertTrue;

@RunWith(Parameterized.class)
public class StationWhiteBoxTest {
	private Station station = new Station("1", "Bach Khoa", "1 Dai Co Viet");
	private Station stationTest ;
	private boolean expected;

	public StationWhiteBoxTest(Station obj, boolean expected) throws Exception {
		super();
		this.stationTest = obj;
		this.expected = expected;
	}

	@Parameterized.Parameters
	public static Collection<Object[]> primeNumbers() {
		return Arrays.asList(new Object[][]{
				{ new Station("1", "What", "Where"), true},
				{new Station("2", "Xay Dung", ""), false},
		});
	}
//	@Test
//	public void testMatchStation1() {
//		Station station = new Station("1", "What", "Where");
//		assertTrue("Station Match", this.station.match(station));
//	}
	
//	@Test
//	public void testMatchStation2() {
//		Station station = new Station("2", "Xay Dung", "");
//		assertTrue("Station Match", !this.station.match(station));
//	}

	@Test
	public void testIsValidToUpdate() {
		assert this.station.match(stationTest) == this.expected;
	}

}
