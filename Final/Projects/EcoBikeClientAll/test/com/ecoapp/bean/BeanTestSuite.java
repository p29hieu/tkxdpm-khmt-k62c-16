package com.ecoapp.bean;

import com.ecoapp.bean.bike.BikeTestSuite;
import com.ecoapp.bean.rentaladmin.RentalAdminTestSuite;
import com.ecoapp.bean.station.StationTestSuite;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({StationTestSuite.class, RentalAdminTestSuite.class, BikeTestSuite.class})
public class BeanTestSuite {
}
