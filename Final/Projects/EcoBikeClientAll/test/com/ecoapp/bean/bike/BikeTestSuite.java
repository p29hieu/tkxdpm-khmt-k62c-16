package com.ecoapp.bean.bike;

import com.ecoapp.bean.bike.checkEmptyDocks.CheckEmptyDockTestSuite;
import com.ecoapp.bean.bike.isValidateToUpdate.IsValidToUpdateTestSuite;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({IsValidToUpdateTestSuite.class, CheckEmptyDockTestSuite.class})
public class BikeTestSuite {
}
