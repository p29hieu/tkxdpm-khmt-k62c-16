package com.ecoapp.bean.bike.checkEmptyDocks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import com.ers.bean.Bike;
import com.ers.bean.Station;

@RunWith(Parameterized.class)
public class BlackBoxEmptyDocksTest {
    private boolean expectedResult;
    private String stationId;

    public BlackBoxEmptyDocksTest(String stationId,boolean expectedResult) {
        this.stationId = stationId;
        this.expectedResult = expectedResult;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> primeNumbers() {
        return Arrays.asList(new Object[][]{
                {"1", true},
                {"2", true},
                {"3", false},
                {"4", true},
                {"5", true},
                {"6",false},
        });
    }

    @Test
    public void testCheckStation() {
        

        ArrayList<Station> listStation = new ArrayList();
        listStation.add(new Station("1","Station1","Số 1, Đại cồ việt",0.1,0.1,2,3,4,10));
        listStation.add(new Station("2","Station2","Số 1, Đại cồ việt",0.1,0.1,1,2,5,4));
        listStation.add(new Station("3","Station3","Số 1, Đại cồ việt",0.1,0.1,1,1,10,0));
        listStation.add(new Station("4","Station4","Số 1, Đại cồ việt",0.1,0.1,1,1,1,10));
        listStation.add(new Station("5","Station5","Số 1, Đại cồ việt",0.1,0.1,1,1,1,10));
        
        Bike bike = new Bike();
        bike.setStationId(this.stationId);
        
        assert bike.checkStation(listStation) == this.expectedResult;


    }
}

