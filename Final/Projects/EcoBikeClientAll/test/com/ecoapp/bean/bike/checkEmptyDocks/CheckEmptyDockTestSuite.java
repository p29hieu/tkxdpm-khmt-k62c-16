package com.ecoapp.bean.bike.checkEmptyDocks;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({BlackBoxEmptyDocksTest.class, WhiteBoxEmptyDocksTest.class})
public class CheckEmptyDockTestSuite {
}
