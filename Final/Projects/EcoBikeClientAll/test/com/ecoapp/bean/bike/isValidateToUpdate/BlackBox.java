package com.ecoapp.bean.bike.isValidateToUpdate;

import com.ers.bean.Bike;
import com.ers.constant.StatusBike;
import com.ers.constant.TypeBike;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class BlackBox {
    private String currentStatus;
    private String currentType;
    private long cost;
    private String name;
    private double weight;
    private String stationName;
    private double batteryPercentage;
    private double estimated;
    private int loadCycles;
    private boolean expectedResult;

    public BlackBox(String currentStatus, String currentType, long cost, String name, double weight, String stationName, double batteryPercentage, double estimated, int loadCycles, boolean expectedResult) {
        this.currentStatus = currentStatus;
        this.currentType = currentType;
        this.cost = cost;
        this.name = name;
        this.weight = weight;
        this.stationName = stationName;
        this.batteryPercentage = batteryPercentage;
        this.estimated = estimated;
        this.loadCycles = loadCycles;
        this.expectedResult = expectedResult;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> primeNumbers() {
        return Arrays.asList(new Object[][]{
                {StatusBike.HIRING, TypeBike.SINGLE_E_BIKE, 1000, "Biek1", 5, "Station name", 80, 156754, 2, false},
                {StatusBike.PENDING, TypeBike.SINGLE_E_BIKE, 1000, "Biek1", 5, "Station name", 80, 156754, 2, false},
                {StatusBike.EMPTY, TypeBike.SINGLE_E_BIKE, -1, "Biek1", 5, "Station name", 80, 156754, 2, false},
                {StatusBike.EMPTY, TypeBike.SINGLE_E_BIKE, 1000, null, 5, "Station name", 80, 156754, 2, false},
                {StatusBike.EMPTY, TypeBike.SINGLE_E_BIKE, 1000, "", 5, "Station name", 80, 156754, 2, false},
                {StatusBike.EMPTY, TypeBike.SINGLE_E_BIKE, 1000, "Biek1", -1, "Station name", 80, 156754, 2, false},
                {StatusBike.EMPTY, TypeBike.SINGLE_E_BIKE, 1000, "Biek1", 5, "", 80, 156754, 2, false},
                {StatusBike.EMPTY, TypeBike.SINGLE_E_BIKE, 1000, "Biek1", 5, "Station name", 101, 156754, 2, false},
                {StatusBike.EMPTY, TypeBike.SINGLE_E_BIKE, 1000, "Biek1", 5, "Station name", -1, 156754, 2, false},
                {StatusBike.EMPTY, TypeBike.SINGLE_E_BIKE, 1000, "Biek1", 5, "Station name", 80, 156754, -1, false},
                {StatusBike.EMPTY, TypeBike.SINGLE_E_BIKE, 1000, "Biek1", 5, "Station name", 80, -1, 2, false},
                {StatusBike.EMPTY, TypeBike.SINGLE_E_BIKE, 1000, "Biek1", 5, null, 80, 156754, 2, false},
                {StatusBike.EMPTY, TypeBike.SINGLE_E_BIKE, 1000, "Biek1", 5, "", 80, 156754, 2, false},
                {StatusBike.EMPTY, TypeBike.SINGLE_BIKE, 1000, "Biek1", 5, "Station name", 0, 0, 0, true},
                {StatusBike.EMPTY, TypeBike.DOUBLE_BIKE, 1000, "Biek1", 5, "Station name", 0, 0, 0, true},
                {StatusBike.EMPTY, TypeBike.SINGLE_E_BIKE, 1000, "Biek1", 5, "Station name", 100, 12445354, 2, true},
                {StatusBike.EMPTY, TypeBike.DOUBLE_E_BIKE, 1000, "Biek1", 5, "Station name", 100, 12445354, 2, true},
        });
    }

    @Test
    public void testIsValidToUpdate() {
        Bike currentBike = new Bike();
        Bike bike = new Bike();
        currentBike.setId("1");
        currentBike.setStatus(this.currentStatus);
        currentBike.setType(this.currentType);

        bike.setId("1");
        bike.setCost(this.cost);
        bike.setName(this.name);
        bike.setWeight(this.weight);
        bike.setStationId("1");
        bike.setStationName(this.stationName);
        bike.setBatteryPercentage(this.batteryPercentage);
        bike.setEstimated(this.estimated);
        bike.setLoadCycles(this.loadCycles);

        assert currentBike.isValidateToUpdate(bike) == this.expectedResult;
    }
}
