package com.ecoapp.bean.rentaladmin.calrental;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({BlackBox.class, WhiteBox.class})
public class TestSuite {
}