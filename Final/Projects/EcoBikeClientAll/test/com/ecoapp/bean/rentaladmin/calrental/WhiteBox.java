package com.ecoapp.bean.rentaladmin.calrental;

import com.ers.bean.Rental;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class WhiteBox {

    private final int fromMinutes = 720;
    private final int toMinutes = 1440;
    private final int fixedPrice = 200000;
    private final int refund = 10000;
    private final int after15 = 2000;

    private int minutes;
    private int expectedResult;

    public WhiteBox(int minutes, int expectedResult) {
        this.minutes = minutes;
        this.expectedResult = expectedResult;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> primeNumbers() {
        return Arrays.asList(new Object[][]{
                {-1, 0},
                {659, 180000},
                {721, 200000},
                {1450, 202000}
        });
    }

    @Test
    public void testCalRental() {
        Rental rental = new Rental();
        rental.setRental(fromMinutes, toMinutes, fixedPrice, refund, after15);
        System.out.println(rental.calRental(minutes));
        assert rental.calRental(minutes) == this.expectedResult;
    }
}

