package com.ecoapp.bean.rentaladmin;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({com.ecoapp.bean.rentaladmin.calrental.TestSuite.class})
public class RentalAdminTestSuite {
}