package com.ecoapp.bean.rentaladmin.calrental;

import com.ers.bean.Rental;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class BlackBox {

    private final int fromMinutes = 0;
    private final int toMinutes = 30;
    private final int fixedPrice = 10000;
    private final int refund = 0;
    private final int after15 = 3000;

    private int minutes;
    private int expectedResult;

    public BlackBox(int minutes, int expectedResult) {
        this.minutes = minutes;
        this.expectedResult = expectedResult;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> primeNumbers() {
        return Arrays.asList(new Object[][]{
                {-1, 0},
                {27, 10000},
                {30, 10000},
                {70, 19000}
        });
    }

    @Test
    public void testCalRental() {
        Rental rental = new Rental();
        assert rental.calRental(minutes) == this.expectedResult;
    }
}

