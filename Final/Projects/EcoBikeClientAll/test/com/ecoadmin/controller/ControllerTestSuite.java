package com.ecoadmin.controller;

import com.ecoadmin.controller.bike.UpdateBikeTestSuite;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({UpdateBikeTestSuite.class})
public class ControllerTestSuite {
}
