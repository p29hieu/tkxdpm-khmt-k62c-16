package com.ecoadmin.controller.bike;


import com.ecoadmin.controller.bike.update.TestSuite;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({TestSuite.class})
public class UpdateBikeTestSuite {
}
