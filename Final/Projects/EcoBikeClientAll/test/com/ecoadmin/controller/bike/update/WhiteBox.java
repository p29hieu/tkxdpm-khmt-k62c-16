package com.ecoadmin.controller.bike.update;

import com.ers.bean.Bike;
import com.ers.components.admin.editbike.EditBikeController;
import com.ers.components.bike.BikeListPane;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class WhiteBox {
    private Bike bikeDefault = new Bike("bike-008", "station-001", "Station 001", "SOME-371", "DOUBLE_BIKE", 7.7, "", 43503, "HIBIKE", 7500000, "EMPTY", 12.5, 45.4, 1);
    private BikeListPane bikeListPane = null;

    private Bike bikeInput;
    private boolean expectedResult;

    public WhiteBox(Bike bike, boolean expectedResult) {
        this.bikeInput = bike;
        this.expectedResult = expectedResult;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> primeNumbers() {
        return Arrays.asList(new Object[][]{
                {null, false},
                {new Bike("bike-007", "station-001", "Station 001", "SOME-371", "DOUBLE_BIKE", 7.7, "", 43503, "HIBIKE", 7500000, "EMPTY", 12.5, 45.4, 1), false},
                {new Bike("bike-008", "station-001", "Station 001", "", "DOUBLE_BIKE", 7.7, "", 43503, "HIBIKE", 7500000, "EMPTY", 12.5, 45.4, 1), false},
                {new Bike("bike-008", "station-001", "Station 001", "SOME-371", "DOUBLE_BIKE", 7.7, "asddad", 43503, "HIBIKE", 7500000, "EMPTY", 12.5, 45.4, 1), false}
        });
    }

    @Test
    public void testUpdateController() {
        EditBikeController editBikeController = new EditBikeController(this.bikeDefault, this.bikeListPane);
        boolean success = editBikeController.update(this.bikeInput);

        assert success == this.expectedResult;
    }
}
