package adminscreens;

import java.util.EventListener;

public interface ChangeRentalPriceListener extends EventListener {
    public void changeEventOccurred(ChangeRentalPriceEvent e);
}
