package adminscreens;

import org.w3c.dom.Text;
import rentalcost.RentalCostController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ToolBar extends JPanel implements ActionListener {

    private JButton changeRentalPriceButton;
    private JButton bikeManagementButton;
    private JButton parkingLotManagementButton;

    private StringListener textListener;
    private ChangeRentalPriceListener changeRentalPriceListener;

    private RentalCostController controller;

    public ToolBar() {
        setBorder(BorderFactory.createEtchedBorder());
        changeRentalPriceButton = new JButton("Đổi giá thuê xe");
        bikeManagementButton = new JButton("Quản lý xe");
        parkingLotManagementButton = new JButton("Quản lý bãi xe");

        changeRentalPriceButton.addActionListener(this);
        bikeManagementButton.addActionListener(this);
        parkingLotManagementButton.addActionListener(this);

        setLayout(new FlowLayout(FlowLayout.LEFT));

        add(changeRentalPriceButton);
        add(bikeManagementButton);
        add(parkingLotManagementButton);
    }

    public void setStringListener(StringListener listener) {
        this.textListener = listener;
    }

    public void setChangeRentalPriceListener(ChangeRentalPriceListener listener) {
        this.changeRentalPriceListener = listener;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton clicked = (JButton) e.getSource();

        if (clicked == changeRentalPriceButton) {
            if (textListener != null) {
                textListener.textEmitted("Thay đổi cách tính giá thuê xe - Hà Hữu Linh.\n");
            }
            if (changeRentalPriceListener != null) {
                controller = new RentalCostController();
                ChangeRentalPriceEvent ev = new ChangeRentalPriceEvent(this, controller);
                changeRentalPriceListener.changeEventOccurred(ev);
            }
        } else if (clicked == bikeManagementButton) {
            // TODO
            if (textListener != null) {
                textListener.textEmitted("Quản lý xe\n");
            }
        }
        else if (clicked == parkingLotManagementButton) {
            // TODO
            if (textListener != null) {
                textListener.textEmitted("Quản lý bãi xe\n");
            }
        }
    }
}
