package adminscreens;

import rentalcost.RentalCostController;

import java.util.EventObject;

public class ChangeRentalPriceEvent extends EventObject {
    private RentalCostController controller;

    public ChangeRentalPriceEvent(Object source) {
        super(source);
    }

    public ChangeRentalPriceEvent(Object source, RentalCostController controller) {
        super(source);
        this.controller = controller;
    }

    public RentalCostController getController() {
        return controller;
    }

    public void setController(RentalCostController controller) {
        this.controller = controller;
    }
}
