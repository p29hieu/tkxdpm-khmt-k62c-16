package rentalcost;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class RentalDialog extends JDialog {

    private RentalCostController controller;
    private GridBagLayout layout;
    private GridBagConstraints c;

    private JTextField fromMinutes;
    private JTextField toMinutes;
    private JTextField fixedPrice;
    private JTextField refund;
    private JTextField after15;

    // khoi tao form tinh chi phi thue xe
    public RentalDialog() {
        // TODO

        layout = new GridBagLayout();
        this.setLayout(layout);
        c = new GridBagConstraints();

        c.insets = new Insets(10,0,5,0);
        c.gridx = 0;
        c.gridy = 0;
        add(new JLabel("Từ phút thứ"), c);
        c.gridx = 1;
        c.gridy = 0;
        fromMinutes = new JTextField(15);
        add(fromMinutes, c);

        c.insets = new Insets(0,0,5,0);
        c.gridx = 0;
        c.gridy = 1;
        add(new JLabel("Đến phút thứ"), c);
        c.gridx = 1;
        c.gridy = 1;
        toMinutes = new JTextField(15);
        add(toMinutes, c);


        c.gridx = 0;
        c.gridy = 2;
        add(new JLabel("Số tiền phải trả mặc định"), c);
        c.gridx = 1;
        c.gridy = 2;
        fixedPrice = new JTextField(15);
        add(fixedPrice, c);

        c.gridx = 0;
        c.gridy = 3;
        add(new JLabel("Số tiền hoàn lại"), c);
        c.gridx = 1;
        c.gridy = 3;
        refund = new JTextField(15);
        add(refund, c);

        c.gridx = 0;
        c.gridy = 4;
        add(new JLabel("Mỗi 15 phút quá thời gian"), c);
        c.gridx = 1;
        c.gridy = 4;
        after15 = new JTextField(15);
        add(after15, c);

//        customerNameField.addKeyListener(new KeyAdapter() {
//            @Override
//            public void keyTyped(KeyEvent e) {
//                order.setCustomerName(customerNameField.getText());
//            }
//        });
//        customerPhoneField.addKeyListener(new KeyAdapter() {
//            @Override
//            public void keyTyped(KeyEvent e) {
//                order.setCustomerPhoneNumber(customerPhoneField.getText());
//            }
//        });
//        customerAddressField.addKeyListener(new KeyAdapter() {
//            @Override
//            public void keyTyped(KeyEvent e) {
//                order.setCustomerAddress(customerAddressField.getText());
//            }
//        });

        int row = getLastRowIndex();
        c.gridx = 0;
        c.gridy = row;
        JButton button = new JButton("OK");
        add(button, c);
        setSize(400, 300);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // controller.doSomething();
                System.out.println("Hello World");
            }
        });
    }

    public RentalCostController getController() {
        return controller;
    }

    public void setController(RentalCostController controller) {
        this.controller = controller;
    }

    protected int getLastRowIndex() {
        layout.layoutContainer(this.getContentPane());
        int[][] dim = layout.getLayoutDimensions();
        int rows = dim[1].length;
        return rows;
    }
}
