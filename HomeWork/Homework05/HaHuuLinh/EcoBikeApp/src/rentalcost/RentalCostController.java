package rentalcost;

public class RentalCostController {
    private RentalDialog rentalDialog;
    private DepositDialog depositDialog;

    public RentalCostController() {
        rentalDialog = new RentalDialog();
        depositDialog = new DepositDialog();

        System.out.println(rentalDialog);

        rentalDialog.setController(this);
        depositDialog.setController(this);
    }

    public void showRentalDialog() {
        rentalDialog.setVisible(true);
    }
}
