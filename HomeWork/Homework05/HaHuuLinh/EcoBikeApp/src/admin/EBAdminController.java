package admin;

import javax.swing.*;
import java.awt.*;
import adminscreens.*;
import rentalcost.RentalCostController;

public class EBAdminController extends JFrame {
    private TextPanel textPanel;
    private ToolBar toolBar;
    private FormPanel formPanel;

    private RentalCostController controller;

    public EBAdminController() {
        super("Hệ thống hỗ trợ thuê, trả xe tự động");

        setLayout(new BorderLayout());

        toolBar = new ToolBar();
        textPanel = new TextPanel();
        formPanel = new FormPanel();

        toolBar.setStringListener(new StringListener() {
            @Override
            public void textEmitted(String text) {
                textPanel.appendText(text);
            }
        });

        toolBar.setChangeRentalPriceListener(new ChangeRentalPriceListener() {
            @Override
            public void changeEventOccurred(ChangeRentalPriceEvent e) {
                // TODO
                controller = e.getController();
                controller.showRentalDialog();
            }
        });

        add(formPanel, BorderLayout.WEST);
        add(toolBar, BorderLayout.NORTH);
        add(textPanel, BorderLayout.CENTER);

        setSize(600, 500);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }
}
