package admin;

import javax.swing.SwingUtilities;


public class EBAdmin {

    public static void main(String[] args) {

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new EBAdminController();
            }
        });
    }
}
