package constant;

import java.util.HashMap;
import java.util.Map;

public class TypeBike {
    public static final String SINGLE_BIKE = "SINGLE_BIKE";
    public static final String SINGLE_E_BIKE = "SINGLE_E_BIKE";
    public static final String DOUBLE_BIKE = "DOUBLE_BIKE";
    public static final String DOUBLE_E_BIKE = "DOUBLE_E_BIKE";
}
