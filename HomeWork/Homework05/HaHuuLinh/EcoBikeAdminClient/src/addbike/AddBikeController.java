package addbike;

import com.ecoapp.bean.Bike;

import abstractdata.ADataPageController;
import abstractdata.IDataManageController;
import bike.BikeListPane;

public class AddBikeController implements IDataManageController<Bike> {
	private ADataPageController<Bike> controller;
	private BikeListPane listPane;
	public AddBikeController(ADataPageController<Bike> controller,BikeListPane listPane) {
		// TODO Auto-generated constructor stub
		super();
		
		this.controller= controller;
		this.listPane = listPane;
	}

	@Override
	public Bike create(Bike t) {
		
		controller.create(t);
		listPane.insertData(t);
		return null;
	}

	@Override
	public Bike read(Bike t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(Bike t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean update(Bike t) {
		// TODO Auto-generated method stub
		return true;
	}

}
