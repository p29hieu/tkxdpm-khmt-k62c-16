package addbike;

import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.*;

import com.ecoapp.bean.Bike;
import com.ecoapp.bean.Station;

import abstractdata.IDataManageController;
import adddialog.ADataAddDialog;

public class AddBikeDialog extends ADataAddBikeDialog<Bike> {
	private JTextField nameField;
    private JComboBox typeField;
    private JTextField weightField;
    private JTextField licensePlateField;
    private JTextField producerField;
    private JTextField costField;
    private JLabel statusField;
    private JComboBox stationField;


	public AddBikeDialog(AddBikeController controller,List<Station> listStation) {
		super(controller,listStation);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void buildControls() {
		System.out.println(listStation);
	
		int row = getLastRowIndex();
        c.gridx = 0;
        c.gridy = row;
        c.insets = new Insets(5,0,5,5);
        getContentPane().add(new JLabel("Name"), c);
        nameField = new JTextField(15);
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(nameField, c);

        row = getLastRowIndex();
        stationField = new JComboBox(listStation.toArray());
        c.gridx = 0;
        c.gridy = row;
       
        getContentPane().add(new JLabel("Station"), c);
        c.gridx = 1;
        c.gridy = row;
        c.weightx= 1;
        getContentPane().add(stationField, c);



        row = getLastRowIndex();
        JLabel types = new JLabel("Types");
        String type[]= {"SINGLE_BIKE", "SINGLE_E_BIKE", "DOUBLE_BIKE", "DOUBLE_E_BIKE"};
        typeField = new JComboBox(type);
        
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(types, c);
      
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(typeField, c);
        


        row = getLastRowIndex();
        JLabel weight = new JLabel("Weight");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(weight, c);
        weightField = new JTextField(15);
        weightField.setEditable(true);
      
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(weightField, c);

        row = getLastRowIndex();
        JLabel licensePlate = new JLabel("License Plate");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(licensePlate, c);
        licensePlateField = new JTextField(15);
      
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(licensePlateField, c);

        row = getLastRowIndex();
        JLabel producer = new JLabel("Producer");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(producer, c);
        producerField = new JTextField(15);
       
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(producerField, c);

        row = getLastRowIndex();
        JLabel cost = new JLabel("cost");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(cost, c);
        costField = new JTextField(15);
      
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(costField, c);

        row = getLastRowIndex();
        JLabel status = new JLabel("Status");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(status, c);
        statusField = new JLabel("EMPTY");
        
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(statusField, c);
		
	}

	@Override
	public Bike getNewData() {
		// TODO Auto-generated method stub
		
		Bike bike = new Bike();
		
		bike.setName(nameField.getText());
		bike.setType(typeField.getItemAt(typeField.getSelectedIndex()).toString());
		bike.setStationName(stationField.getItemAt(stationField.getSelectedIndex()).toString());
		bike.setLicensePlate(licensePlateField.getText());
		bike.setStatus(statusField.getText());
		bike.setCost(Long.parseLong(costField.getText()));
		bike.setProducer(producerField.getText());
		bike.setWeight(Double.parseDouble(weightField.getText()));
		
		return bike;
		
	}


}
