package addbike;

import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import com.ecoapp.bean.Bike;
import com.ecoapp.bean.Station;

import abstractdata.IDataManageController;
import editdialog.ADataEditDialog;

@SuppressWarnings("serial")
public abstract class ADataAddBikeDialog<T> extends JDialog {
	protected T t;
	protected GridBagLayout layout;
	protected GridBagConstraints c = new GridBagConstraints();
	protected List<Station> listStation;
	private JFrame frame;

	public ADataAddBikeDialog(IDataManageController<T> controller,List<Station> listStation) {
		super((Frame) null, "Add", true);
		this.listStation= listStation;
		
		setContentPane(new JPanel());
		layout = new GridBagLayout();
		getContentPane().setLayout(layout);

		
		this.buildControls();
		
		JButton addButton = new JButton("Add");
		addButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
					T newT = getNewData();
				 Object[] options = {"Yes",
	                        "Cancel"};
	                int n = JOptionPane.showOptionDialog(frame,
	                        "Bạn có muốn thêm "+((Bike) newT).getName()+" vào "+ ((Bike) newT).getStationName(),
	                        "Xác nhận",
	                        JOptionPane.YES_NO_OPTION,
	                        JOptionPane.QUESTION_MESSAGE,
	                        null,
	                        options,
	                        options[1]);
	                if (n == JOptionPane.YES_OPTION) {
	                	
	    				controller.create(newT);
	    				ADataAddBikeDialog.this.dispose();
	                } else if (n == JOptionPane.NO_OPTION) {

	                } else {

	                	
	                }
				
			}
		});
		
		
		c.gridx = 1;
		c.gridy = getLastRowIndex();
		getContentPane().add(addButton, c);
		
		
		this.pack();
		this.setResizable(false);
		this.setVisible(true);
	}
	
	protected int getLastRowIndex() {
		layout.layoutContainer(getContentPane());
		int[][] dim = layout.getLayoutDimensions();
	    int rows = dim[1].length;
	    return rows;
	}
	
	public abstract void buildControls();
	
	public abstract T getNewData();
}
