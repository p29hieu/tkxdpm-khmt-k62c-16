package rentalcomponent;

import api.PaymentApi;
import com.ecoapp.bean.Deposit;
import com.ecoapp.bean.Rental;
import rentalcomponent.dialogs.DepositDialog;
import rentalcomponent.dialogs.RentalDialog;

import javax.swing.*;

public class RentalCostController {

    private PaymentApi api;

    private RentalDialog rentalDialog;
    private DepositDialog depositDialog;

    private Rental rental;
    private Deposit deposit;

    public RentalCostController(JFrame parent) {

        api = new PaymentApi();

        this.rental = api.getRental();
        this.deposit = api.getDeposit();

        rentalDialog = new RentalDialog(parent, this);
    }

    public void showRentalDialog() {
        rentalDialog.setVisible(true);
    }

    public void showDepositDialog() {
        depositDialog.setVisible(true);
    }

    public void setRentalData(int fromM, int toM, int fPrice, int nM, int a15) {
        rental.setRental(fromM, toM, fPrice, nM, a15);
    }

    public void setDepositData(int sgBike, int sgEBike, int dleBike) {
        deposit.setDeposit(sgBike, sgEBike, dleBike);
    }

    public Rental getCurrentRental() {
        return this.rental;
    }

    public Deposit getCurrentDeposit() {
        return this.deposit;
    }

    public void createDepositDialog() {
        this.depositDialog = new DepositDialog(this.rentalDialog, this);
    }

    public void updateAllDialogData() {
        if (rental != null && deposit != null) {
            api.updateRental(rental);
            api.updateDeposit(deposit);
            System.out.println("SUCCESSFUL!");
        }
    }

    public void closeAllDialogs() {
        depositDialog.setVisible(false);
        rentalDialog.setVisible(false);
    }
}
