package rentalcomponent.rentalpage;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import rentalcomponent.RentalCostController;

public class ToolBar extends JPanel implements ActionListener {

    private JFrame parent;

    private JButton changeRentalPriceButton;

    private ChangeRentalPriceListener changeRentalPriceListener;

    public ToolBar(JFrame parent) {
        this.parent = parent;

        setBorder(BorderFactory.createEtchedBorder());

        changeRentalPriceButton = new JButton("Đổi giá thuê xe");
        changeRentalPriceButton.addActionListener(this);

        setLayout(new FlowLayout(FlowLayout.LEFT));
        add(changeRentalPriceButton);
    }


    public void setChangeRentalPriceListener(ChangeRentalPriceListener listener) {
        this.changeRentalPriceListener = listener;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton clicked = (JButton) e.getSource();

        if (clicked == changeRentalPriceButton) {

            if (changeRentalPriceListener != null) {
                ChangeRentalPriceEvent ev = new ChangeRentalPriceEvent(this, new RentalCostController(parent));
                changeRentalPriceListener.changeEventOccurred(ev);
            }
        }
    }
}