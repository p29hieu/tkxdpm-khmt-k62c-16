package rentalcomponent.dialogs;

import com.ecoapp.bean.Rental;
import rentalcomponent.RentalCostController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class RentalDialog extends JDialog implements ActionListener {

    private Rental currentRental;

    private RentalCostController controller;
    private GridBagLayout layout;
    private GridBagConstraints c;

    private JComboBox fromMinutes;
    private JComboBox fixedPrice;
    private JComboBox toMinutes;
    private JComboBox refund;
    private JComboBox after15;

    private JButton okButton;

    // khoi tao form tinh chi phi thue xe
    public RentalDialog(JFrame parent, RentalCostController controller) {

        super(parent, "Thay đổi cách tính giá thuê xe", true);

        this.controller = controller;
        // setting layout for Rental Dialog
        this.currentRental = controller.getCurrentRental();
        layoutComponents();
    }

    public void layoutComponents() {
        layout = new GridBagLayout();
        setLayout(layout);
        setSize(new Dimension(300, 400));
        c = new GridBagConstraints();

        // khoi tao cac ComboBox
        fromMinutes = new JComboBox();
        toMinutes = new JComboBox();
        fixedPrice = new JComboBox();
        refund = new JComboBox();
        after15 = new JComboBox();

        okButton = new JButton("Change!");

        okButton.addActionListener(this);

        /**** ROW 1 ****/

        // set up from Minutes
        DefaultComboBoxModel fromMinutesModel = new DefaultComboBoxModel();
        fromMinutesModel.addElement(currentRental.getFromMinutes());
        if (currentRental.getFromMinutes() == 0) {
            fromMinutesModel.addElement(720);
        } else {
            fromMinutesModel.addElement(0);
        }
        fromMinutes.setModel(fromMinutesModel);
        fromMinutes.setSelectedIndex(0);

        fromMinutes.setEditable(true);

        c.gridy = 0;
        c.weightx = 1;
        c.weighty = 0.005;

        c.gridx = 0;
        c.fill = GridBagConstraints.NONE;
        c.insets = new Insets(0, 0, 0, 10);
        c.anchor = GridBagConstraints.LINE_END;
        add(new JLabel("From Minutes:"), c);

        c.gridx = 1;
        c.anchor = GridBagConstraints.LINE_START;
        c.insets = new Insets(0, 0, 0, 0);
        add(fromMinutes, c);

        /**** ROW 2 ****/

        // set up to Minutes
        DefaultComboBoxModel toMinutesModel = new DefaultComboBoxModel();
        toMinutesModel.addElement(currentRental.getToMinutes());
        if (currentRental.getToMinutes() == 30) {
            toMinutesModel.addElement(1440);
        } else {
            toMinutesModel.addElement(30);
        }
        toMinutes.setModel(toMinutesModel);
        toMinutes.setSelectedIndex(0);
        toMinutes.setEditable(true);

        c.gridy++;
        c.weightx = 1;
        c.weighty = 0.005;

        c.gridx = 0;
        c.fill = GridBagConstraints.NONE;
        c.insets = new Insets(0, 0, 0, 10);
        c.anchor = GridBagConstraints.LINE_END;
        add(new JLabel("To Minutes:"), c);

        c.gridx = 1;
        c.anchor = GridBagConstraints.LINE_START;
        c.insets = new Insets(0, 0, 0, 0);
        add(toMinutes, c);

        /**** ROW 3 ****/

        // set up Fixed Price
        DefaultComboBoxModel fixedPriceModel = new DefaultComboBoxModel();
        fixedPriceModel.addElement(currentRental.getFixedPrice());
        if (currentRental.getFixedPrice() == 10000) {
            fixedPriceModel.addElement(200000);
        } else {
            fixedPriceModel.addElement(10000);
        }

        fixedPrice.setModel(fixedPriceModel);
        fixedPrice.setSelectedIndex(0);
        fixedPrice.setEditable(true);

        c.gridy++;
        c.weightx = 1;
        c.weighty = 0.005;

        c.gridx = 0;
        c.fill = GridBagConstraints.NONE;
        c.insets = new Insets(0, 0, 0, 10);
        c.anchor = GridBagConstraints.LINE_END;
        add(new JLabel("Fixed Price:"), c);

        c.gridx = 1;
        c.anchor = GridBagConstraints.LINE_START;
        c.insets = new Insets(0, 0, 0, 0);
        add(fixedPrice, c);

        /**** ROW 4 ****/

        // set up Fixed Price
        DefaultComboBoxModel refundModel = new DefaultComboBoxModel();
        refundModel.addElement(currentRental.getRefund());
        if (currentRental.getRefund() == 0) {
            refundModel.addElement(10000);
        } else {
            refundModel.addElement(0);
        }
        refund.setModel(refundModel);
        refund.setSelectedIndex(0);

        refund.setEditable(true);

        c.gridy++;
        c.weightx = 1;
        c.weighty = 0.005;

        c.gridx = 0;
        c.fill = GridBagConstraints.NONE;
        c.insets = new Insets(0, 0, 0, 10);
        c.anchor = GridBagConstraints.LINE_END;
        add(new JLabel("ReFund:"), c);

        c.gridx = 1;
        c.anchor = GridBagConstraints.LINE_START;
        c.insets = new Insets(0, 0, 0, 0);
        add(refund, c);

        /**** ROW 5 ****/

        // set up Fixed Price
        DefaultComboBoxModel after15Model = new DefaultComboBoxModel();
        after15Model.addElement(currentRental.getAfter15());
        if (currentRental.getAfter15() == 3000) {
            after15Model.addElement(2000);
        } else {
            after15Model.addElement(3000);
        }
        after15.setModel(after15Model);
        after15.setSelectedIndex(0);

        after15.setEditable(true);

        c.gridy++;
        c.weightx = 1;
        c.weighty = 0.005;

        c.gridx = 0;
        c.fill = GridBagConstraints.NONE;
        c.insets = new Insets(0, 0, 0, 10);
        c.anchor = GridBagConstraints.LINE_END;
        add(new JLabel("After 15 Minutes:"), c);

        c.gridx = 1;
        c.anchor = GridBagConstraints.LINE_START;
        c.insets = new Insets(0, 0, 0, 0);
        add(after15, c);

        /**** ROW 6 ****/
        c.gridy = c.gridy + 3;
        c.weightx = 1;
        c.weighty = 0.005;

        c.gridx = 1;
        c.anchor = GridBagConstraints.FIRST_LINE_START;
        c.insets = new Insets(0, 0, 0, 0);
        add(okButton, c);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        // TODO
        if ((fromMinutes.getSelectedItem() instanceof Integer) && (toMinutes.getSelectedItem() instanceof Integer)
                && (fixedPrice.getSelectedItem() instanceof Integer) && (refund.getSelectedItem() instanceof Integer
                && (after15.getSelectedItem() instanceof Integer))) {

            int fromMIndex = (Integer) fromMinutes.getSelectedItem();
            int toMIndex = (Integer) toMinutes.getSelectedItem();
            int fxPrIndex = (Integer) fixedPrice.getSelectedItem();
            int rfIndex = (Integer) refund.getSelectedItem();
            int afIndex = (Integer) after15.getSelectedItem();

            boolean guard = validateRentalDialog(fromMIndex, toMIndex, fxPrIndex, rfIndex, afIndex);

            if (guard) {
                if (controller != null) {
                    controller.setRentalData(fromMIndex, toMIndex, fxPrIndex, rfIndex, afIndex);
                    controller.createDepositDialog();
                    controller.showDepositDialog();
                }
            } else {
                JOptionPane.showMessageDialog(new JFrame(), "Invalid, please check the inputs again!", "WARNING",
                        JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(new JFrame(), "Bạn cần nhập các số nguyên dương", "WARNING",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    public boolean validateRentalDialog(int fromM, int toM, int fxPr, int reF, int af15) {

        // check not negative integer
        if (fromM < 0 || toM < 0 || fxPr < 0 || reF < 0 || af15 < 0) {
            return false;
        }

        // check fromM <= toM
        if (fromM > toM) {
            return false;
        }

        return true;
    }
}
