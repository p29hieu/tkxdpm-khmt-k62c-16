package main;

import bike.BikePageController;
import rentalcomponent.rentalpage.ChangeRentalPriceEvent;
import rentalcomponent.rentalpage.ChangeRentalPriceListener;
import rentalcomponent.rentalpage.ToolBar;
import station.AdminStationPageController;

import javax.swing.*;

public class EBAdminController extends JFrame {

    private AdminStationPageController adminStationPageController;

    public EBAdminController() {
        this.adminStationPageController = new AdminStationPageController();
    }

    public JPanel getBikePage() {
        if (adminStationPageController == null) {
            adminStationPageController = new AdminStationPageController();
        }
        BikePageController bikePageController = new BikePageController(adminStationPageController);
        return bikePageController.getDataPagePane();
    }

    public JPanel getStationPage() {
        if (adminStationPageController == null) {
            adminStationPageController = new AdminStationPageController();
        }
        return adminStationPageController.getDataPagePane();
    }

    /** get rental-deposit admin page */
    public JPanel getRentalPage(JFrame parent) {
        ToolBar toolBar = new ToolBar(parent);

        // set listener for change-rental-price event
        toolBar.setChangeRentalPriceListener(new ChangeRentalPriceListener() {
            @Override
            public void changeEventOccurred(ChangeRentalPriceEvent e) {
                e.getController().showRentalDialog();
            }
        });
        return toolBar;
    }
}
