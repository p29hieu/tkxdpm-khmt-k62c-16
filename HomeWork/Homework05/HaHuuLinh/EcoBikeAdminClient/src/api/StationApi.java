package api;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ecoapp.bean.Bike;
import com.ecoapp.bean.Station;

public class StationApi extends AClient<Station> {
	public static final String PATH = "http://localhost:8080/";
	
	private Client client;
	
	public StationApi() {
		client = ClientBuilder.newClient();
	}
	
	public ArrayList<Station> getStations(Map<String, String> queryParams) {
		WebTarget webTarget = client.target(PATH).path("stations/get/");
		
		
		if (queryParams != null) {
			for (String key : queryParams.keySet()) {
				String value = queryParams.get(key);
				webTarget = webTarget.queryParam(key, value);
			}
		}
		

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		ArrayList<Station> res = response.readEntity(new GenericType<ArrayList<Station>>() {});
		System.out.println(res);
		return res;
	}
	public ArrayList<Station> getAllStations() {
		WebTarget webTarget = client.target(PATH).path("stations");

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		ArrayList<Station> res = response.readEntity(new GenericType<ArrayList<Station>>() {
		});
		System.out.println(res);
		return res;
	}

	public Station create(Station station) {
        WebTarget webTarget = client.target(PATH).path("stations");

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.post(Entity.entity(station, MediaType.APPLICATION_JSON));

        Station res = response.readEntity(Station.class);

        return res;
    }
	
	@Override
	public Station[] getList() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Station getOne(Station t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Station search(Map<String, String> params) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Station update(String id, Station t) {
		// TODO Auto-generated method stub
		return null;
	}
}
