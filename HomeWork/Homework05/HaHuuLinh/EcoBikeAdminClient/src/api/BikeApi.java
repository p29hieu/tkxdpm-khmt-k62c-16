package api;

import com.ecoapp.bean.Bike;
import com.ecoapp.bean.Station;

import javax.ws.rs.client.*;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Map;

public class BikeApi extends AClient<Bike> {
    public static final String PATH = "http://localhost:8080/";

    private Client client;

    public BikeApi() {
        client = ClientBuilder.newClient();
    }

    @Override
    public Bike[] getList() {
        return new Bike[0];
    }
    public Bike create(Bike bike) {
    	WebTarget webTarget = client.target(PATH).path("bikes");

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.post(Entity.entity(bike, MediaType.APPLICATION_JSON));

        Bike res = response.readEntity(Bike.class);
        return res;
    }

    @Override
    public Bike getOne(Bike bike) {
        return null;
    }

    @Override
    public Bike search(Map<String, String> params) {
        return null;
    }

    @Override
    public Bike update(String id, Bike bike) {
        return null;
    }

    public ArrayList<Bike> getAll() {
        WebTarget webTarget = client.target(PATH).path("bikes");

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();

        ArrayList<Bike> res = response.readEntity(new GenericType<ArrayList<Bike>>() {
        });
        System.out.println(res);
        return res;
    }

    public ArrayList<Bike> getBikes(Map<String, String> queryParams) {
        WebTarget webTarget = client.target(PATH).path("bikes");
        if (queryParams != null) {
            for (String key : queryParams.keySet()) {
                String value = queryParams.get(key);
                webTarget = webTarget.queryParam(key, value);
            }
        }
        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();
        System.out.print(response.toString());
        ArrayList<Bike> res = response.readEntity(new GenericType<ArrayList<Bike>>() {
        });
        System.out.println(res.toString());
        return res;
    }

    public Bike updateBook(Bike bike) {
        WebTarget webTarget = client.target(PATH).path("books").path(bike.getId());

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.post(Entity.entity(bike, MediaType.APPLICATION_JSON));

        Bike res = response.readEntity(Bike.class);
        return res;
    }
}
