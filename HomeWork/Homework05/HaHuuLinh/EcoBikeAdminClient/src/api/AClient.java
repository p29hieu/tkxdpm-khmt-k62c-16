package api;


import java.util.ArrayList;
import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


public abstract class AClient<T> {
    public static final String PATH = "http://localhost:8081/";

    private Client client;

    public AClient() {
        client = ClientBuilder.newClient();
    }

    public abstract T[] getList();

    public abstract T getOne(T t);

    public abstract T search(Map<String, String> params);

    public abstract T update(String id, T t);
}
