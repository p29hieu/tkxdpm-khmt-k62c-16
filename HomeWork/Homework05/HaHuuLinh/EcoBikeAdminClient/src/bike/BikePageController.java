package bike;

import abstractdata.ADataListPane;
import abstractdata.ADataPageController;
import abstractdata.ADataSearchPane;
import abstractdata.ADataSinglePane;
import api.BikeApi;
import api.StationApi;

import com.ecoapp.bean.Bike;
import com.ecoapp.bean.Station;
import station.AdminStationPageController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class BikePageController extends ADataPageController<Bike> {
    private EditBikeController editBikeController;
    private final AdminStationPageController adminStationPageController;

    public BikePageController(AdminStationPageController adminStationPageController) {
        this.adminStationPageController = adminStationPageController;
        super.init();
    }

    public void setController(EditBikeController controller) {
        this.editBikeController = controller;
    }

    public ArrayList<Station> getListStation() {
        return null;
    }

    @Override
    public ADataListPane<Bike> createListPane() {
        return new BikeListPane(this, "ADMIN", adminStationPageController);
    }

    public void update(Bike bike) {
        editBikeController.update(bike);
    }

    @Override
    public ADataSearchPane createSearchPane() {
        return new BikeSearchPane();
    }

    @Override
    public List<? extends Bike> search(Map<String, String> searchParams) {
        return new BikeApi().getBikes(searchParams);
    }

    @Override
    public ADataSinglePane<Bike> createSinglePane() {
        return new BikeSinglePane();
    }

}
