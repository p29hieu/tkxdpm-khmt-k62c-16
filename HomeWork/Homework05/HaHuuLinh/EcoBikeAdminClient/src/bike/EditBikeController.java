package bike;

import abstractdata.IDataManageController;
import com.ecoapp.bean.Bike;
import constant.StatusBike;

import javax.swing.*;

public class EditBikeController implements IDataManageController<Bike> {

    private Bike bike;
    private final BikeListPane listPane;

    public EditBikeController(Bike bike, BikeListPane listPane) {
        this.bike = bike;
        this.listPane = listPane;
    }

    @Override
    public Bike create(Bike bike) {
        return null;
    }

    @Override
    public Bike read(Bike bike) {
        return null;

    }

    @Override
    public void delete(Bike bike) {

    }

    @Override
    public boolean update(Bike b) {
        if (b != null) {
            if (this.bike.getId().equals(b.getId())) {
                if (!this.bike.isValidateToUpdate(b)) {
                    JOptionPane.showMessageDialog(new JFrame("Input is invalid"), "Your data is invalid!");
                    return false;
                }
                listPane.updateData(b);
                this.bike = b;
                // call API

                //
            }
            return true;
        }
        return false;
    }

}
