package abstractdata;

import javax.swing.*;

import com.ecoapp.bean.Bike;
import com.ecoapp.bean.Station;

import api.BikeApi;
import api.StationApi;

import java.util.List;
import java.util.Map;

public abstract class ADataPageController<T> {
    private ADataPagePane<T> pagePane;

    private JDialog dialog;
    private ADataListPane<T> listPane;


    public ADataPageController() {

    }
    public void init(){
        ADataSearchPane searchPane = createSearchPane();

        listPane = createListPane();

        if(searchPane!=null) {
            searchPane.setController(new IDataSearchController() {
                @Override
                public void search(Map<String, String> searchParams) {
                    List<? extends T> list = ADataPageController.this.search(searchParams);
                    listPane.updateListData(list);
                }
            });

            searchPane.fireSearchEvent();
        }

        pagePane = new ADataPagePane<T>(searchPane, listPane);
    }

    public JPanel getDataPagePane() {
        return pagePane;
    }

    public ADataListPane<T> getListPane() {
        return listPane;
    }

    public void setListPane(ADataListPane<T> listPane) {
        this.listPane = listPane;
    }

    public abstract ADataSearchPane createSearchPane();

    public abstract List<? extends T> search(Map<String, String> searchParams);

    public abstract ADataSinglePane<T> createSinglePane();

    public abstract ADataListPane<T> createListPane();

    public void showDialog() {
        dialog.setVisible(true);
    }

    public void hideDialog() {
        dialog.setVisible(false);
    }

    public JDialog getDialog() {
        return dialog;
    }

    public void setDialog(JDialog dialog) {
        this.dialog = dialog;
        dialog.setVisible(false);
    }

    public void updateDataListPane(T t){
        listPane.updateData(t);
    }
    public List<Station> getListStation(Map<String, String> searchParams) {
		return new StationApi().getStations(searchParams);
	};
	public List<Station> getAllStation() {
		return new StationApi().getAllStations();
	};

    public Bike create(Bike bike) {
    	int i = listPane.getListDataLength();
    	if(i<10) {
    		bike.setId("bike-00"+i);
    	}else {
    		bike.setId("bike-0"+i);
    	}

    	return new BikeApi().create(bike);
    }
}
