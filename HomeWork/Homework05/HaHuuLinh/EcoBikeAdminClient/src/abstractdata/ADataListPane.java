package abstractdata;

import com.ecoapp.bean.Bike;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SuppressWarnings("serial")
public abstract class ADataListPane<T> extends JScrollPane {
    private LayoutManager layout;
    protected JPanel pane;
    protected JButton button;
    protected List<? extends T> listData;

    public List<? extends T> getListData() {
        return listData;
    }

    public void setListData(List<? extends T> listData) {
        this.listData = listData;
    }

    protected ADataPageController<T> controller;

    public ADataListPane() {
        pane = new JPanel();
        layout = new BoxLayout(pane, BoxLayout.Y_AXIS);
        pane.setLayout(layout);

        this.setViewportView(pane);
        this.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        this.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        this.getVerticalScrollBar().setUnitIncrement(20);
        this.getHorizontalScrollBar().setUnitIncrement(20);
    }


    public abstract void decorateSinglePane(ADataSinglePane<T> singlePane);

    public void updateListData(List<? extends T> list) {
        this.listData = list;
        pane.removeAll();
		
        if (button != null) {
        	pane.add(button);
        }
        for (T t : listData) {
            ADataSinglePane<T> singlePane = controller.createSinglePane();
            decorateSinglePane(singlePane);

            singlePane.updateData(t);
            pane.add(singlePane);
            pane.add(Box.createRigidArea(new Dimension(0, 40)));
        }
        pane.revalidate();
        pane.repaint();
    }

    public void insertData(T t) {
        ADataSinglePane<T> singlePane = controller.createSinglePane();
        decorateSinglePane(singlePane);
        singlePane.updateData(t);
        pane.add(singlePane);
    }

    public void updateData(T t) {
        for (int i = 0; i < listData.size(); i++) {
            T temp = listData.get(i);
            if (temp.equals(t)) {
                ADataSinglePane<T> singlePane = controller.createSinglePane();
                decorateSinglePane(singlePane);
                singlePane.updateData(t);
                pane.remove(i);
                pane.add(singlePane, i);
            }
        }
        pane.revalidate();
        pane.repaint();
    }

    public int getListDataLength() {
		return listData.size();
	}

}
