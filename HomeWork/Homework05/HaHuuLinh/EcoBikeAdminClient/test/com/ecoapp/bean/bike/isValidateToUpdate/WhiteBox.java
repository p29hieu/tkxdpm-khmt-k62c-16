package com.ecoapp.bean.bike.isValidateToUpdate;

import com.ecoapp.bean.Bike;
import constant.StatusBike;
import constant.TypeBike;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class WhiteBox {
    private String currentId;
    private String currentStatus;
    private String id;
    private long cost;
    private String name;
    private double weight;
    private String currentType;
    private String stationId;
    private String stationName;
    private double batteryPercentage;
    private int loadCycles;
    private double estimated;
    private boolean expectedResult;

    public WhiteBox(String currentId, String currentStatus, String id, long cost, String name, double weight, String currentType, String stationId, String stationName, double batteryPercentage, int loadCycles, double estimated, boolean expectedResult) {
        this.currentId = currentId;
        this.currentStatus = currentStatus;
        this.id = id;
        this.cost = cost;
        this.name = name;
        this.weight = weight;
        this.currentType = currentType;
        this.stationId = stationId;
        this.stationName = stationName;
        this.batteryPercentage = batteryPercentage;
        this.loadCycles = loadCycles;
        this.estimated = estimated;
        this.expectedResult = expectedResult;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> primeNumbers() {
        return Arrays.asList(new Object[][]{
                {"bike-001", StatusBike.EMPTY, "", 1000, "bike name", 5, TypeBike.SINGLE_E_BIKE, "station-001", "Station name", 10, 1, 156789, false},
                {"bike-001", StatusBike.HIRING, "bike-01", 1000, "bike name", 5, TypeBike.SINGLE_E_BIKE, "station-001", "Station name", 10, 1, 156789, false},
                {"bike-001", StatusBike.EMPTY, "bike-01", 1000, "bike name", 5, TypeBike.SINGLE_E_BIKE, "station-001", "Station name", 10, 1, 156789, false},
                {"bike-001", StatusBike.EMPTY, "bike-001", -1, "bike name", 5, TypeBike.SINGLE_E_BIKE, "station-001", "Station name", 10, 1, 156789, false},
                {"bike-002", StatusBike.EMPTY, "bike-002", 1000, "", 5, TypeBike.SINGLE_E_BIKE, "station-001", "Station name", 10, 1, 156789, false},
                {"bike-003", StatusBike.EMPTY, "bike-003", 1000, "bike name", -1, TypeBike.SINGLE_E_BIKE, "station-001,", "Station name", 10, 1, 156789, false},
                {"bike-003", StatusBike.EMPTY, "bike-003", 1000, "bike name", 5, TypeBike.SINGLE_BIKE, "", "Station name", 10, 1, 156789, false},
                {"bike-003", StatusBike.EMPTY, "bike-003", 1000, "bike name", 5, TypeBike.SINGLE_BIKE, "station-001,", "", 10, 1, 156789, false},
                {"bike-003", StatusBike.EMPTY, "bike-003", 1000, "bike name", 5, TypeBike.SINGLE_BIKE, "station-001,", "Station name", 10, 1, 156789, true},
                {"bike-003", StatusBike.EMPTY, "bike-003", 1000, "bike name", 5, TypeBike.SINGLE_E_BIKE, "station-001,", "Station name", -1, 1, 156789, false},
                {"bike-003", StatusBike.EMPTY, "bike-003", 1000, "bike name", 5, TypeBike.SINGLE_E_BIKE, "station-001,", "Station name", 10, -1, 156789, false},
                {"bike-003", StatusBike.EMPTY, "bike-003", 1000, "bike name", 5, TypeBike.SINGLE_E_BIKE, "station-001,", "Station name", 10, 1, -1, false},
                {"bike-003", StatusBike.EMPTY, "bike-003", 1000, "bike name", 5, TypeBike.SINGLE_E_BIKE, "", "Station name", 10, 1, 156789, false},
                {"bike-003", StatusBike.EMPTY, "bike-003", 1000, "bike name", 5, TypeBike.SINGLE_E_BIKE, "station-001,", "", 10, 1, 156789, false},
                {"bike-003", StatusBike.EMPTY, "bike-003", 1000, "bike name", 5, TypeBike.SINGLE_E_BIKE, "station-001", "Station name", 10, 1, 156789, true}
        });
    }

    @Test
    public void testIsValidToUpdate() {
        Bike currentBike = new Bike();
        Bike bike = new Bike();
        currentBike.setId(this.currentId);
        currentBike.setStatus(this.currentStatus);
        currentBike.setType(this.currentType);

        bike.setId(this.id);
        bike.setCost(this.cost);
        bike.setName(this.name);
        bike.setWeight(this.weight);
        bike.setStationId(this.stationId);
        bike.setStationName(this.stationName);
        bike.setBatteryPercentage(this.batteryPercentage);
        bike.setEstimated(this.estimated);
        bike.setLoadCycles(this.loadCycles);

        assert currentBike.isValidateToUpdate(bike) == this.expectedResult;
    }
}
