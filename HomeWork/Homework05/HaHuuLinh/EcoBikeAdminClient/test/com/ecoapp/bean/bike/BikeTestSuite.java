package com.ecoapp.bean.bike;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({com.ecoapp.bean.bike.isValidateToUpdate.TestSuite.class})
public class BikeTestSuite {
}
