package editdialog;

import abstractdata.ADataPageController;
import abstractdata.IDataManageController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@SuppressWarnings("serial")
public abstract class ADataEditDialog<T> extends JDialog {
    protected T t;
    protected GridBagLayout layout;
    protected GridBagConstraints c = new GridBagConstraints();
    protected JFrame frame;

    public ADataEditDialog(T t, IDataManageController<T> manageController) {
        super((Frame) null, "Edit", true);

        JFrame frame = new JFrame("Dialog");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.frame = frame;

        this.t = t;

        setContentPane(new JPanel());
        layout = new GridBagLayout();
        getContentPane().setLayout(layout);


        this.buildControls();

        JButton saveButton = new JButton("Save");
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object[] options = {"Yes, please",
                        "No, thanks",
                        "Cancel"};
                int n = JOptionPane.showOptionDialog(frame,
                        "Would you want to change this?",
                        "Verify",
                        JOptionPane.YES_NO_CANCEL_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        null,
                        options,
                        options[2]);
                if (n == JOptionPane.YES_OPTION) {
                    T newT = getNewData();
                    manageController.update(newT);

                    ADataEditDialog.this.dispose();
                } else if (n == JOptionPane.NO_OPTION) {

                    ADataEditDialog.this.dispose();
                } else if (n == JOptionPane.CANCEL_OPTION) {

                } else {

                    ADataEditDialog.this.dispose();
                }
            }
        });


        c.gridx = 1;
        c.gridy = getLastRowIndex();
        getContentPane().add(saveButton, c);


        this.pack();
        this.setResizable(false);
    }

    protected int getLastRowIndex() {
        layout.layoutContainer(getContentPane());
        int[][] dim = layout.getLayoutDimensions();
        int rows = dim[1].length;
        return rows;
    }

    public abstract void buildControls();

    public abstract T getNewData();
}
