package bike;

import abstractdata.ADataListPane;
import abstractdata.ADataPageController;
import abstractdata.ADataSinglePane;
import com.ecoapp.bean.Bike;
import com.ecoapp.bean.Station;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class BikeListPane extends ADataListPane<Bike> {
    private final String role;
    private ArrayList<Station> listStation;

    public BikeListPane(ADataPageController<Bike> controller, String role) {
        this.controller = controller;
        this.role = role;
    }

    @Override
    public void decorateSinglePane(ADataSinglePane<Bike> singlePane) {
        JButton button = new JButton();
        if (this.role.equals("ADMIN")) {
            button.setText("Edit");
            button.addActionListener(showEditDialog(singlePane));
        }
        singlePane.addDataHandlingComponent(button);
    }

    private ActionListener showEditDialog(ADataSinglePane<Bike> singlePane) {
        return e -> {
            if (controller instanceof BikePageController) {
                if (this.listStation == null) {
                    // get list station
                    Station st1 = new Station();
                    listStation = new ArrayList<>();
                    st1.setId("station 1");
                    st1.setName("STATION NAME 1");
                    listStation.add(st1.clone());
                    st1.setId("station 2");
                    st1.setName("STATION NAME 2");
                    listStation.add(st1);
                }
                EditBikeController editBikeController = new EditBikeController(singlePane.getData(), this);
                JDialog editDialog = new EditBikeDialog(
                        singlePane.getData(),
                        editBikeController,
                        listStation
                );
                controller.setDialog(
                        editDialog
                );
                controller.showDialog();
            }
        };
    }
    public void updateData(Bike t) {
        pane.removeAll();
        pane.revalidate();
        pane.repaint();

        for (Bike t1 : listData) {
            ADataSinglePane<Bike> singlePane = controller.createSinglePane();
            decorateSinglePane(singlePane);
            if (t1.equals(t))
                singlePane.updateData(t);
            else singlePane.updateData(t1);
            pane.add(singlePane);
            pane.add(Box.createRigidArea(new Dimension(0, 40)));
        }
    }
}
