package bike;

import abstractdata.IDataManageController;
import com.ecoapp.bean.Bike;

public class EditBikeController implements IDataManageController<Bike> {

    private Bike bike;
    private final BikeListPane listPane;

    public EditBikeController(Bike bike, BikeListPane listPane) {
        this.bike = bike;
        this.listPane = listPane;
    }

    @Override
    public Bike create(Bike bike) {
        return null;
    }

    @Override
    public Bike read(Bike bike) {
        return null;

    }

    @Override
    public void delete(Bike bike) {

    }

    @Override
    public Bike update(Bike bike) {
        if (bike != null) {
            if (this.bike.getId().equals(bike.getId())) {
                listPane.updateData(bike);
                this.bike = bike;
                // call API

                //
            }

            return bike;
        }
        return null;
    }
}
