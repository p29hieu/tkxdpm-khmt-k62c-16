package bike;

import abstractdata.ADataSinglePane;
import com.ecoapp.bean.Bike;

import javax.swing.*;

@SuppressWarnings("serial")
public class BikeSinglePane extends ADataSinglePane<Bike> {
    private JLabel id;
    private JLabel stationId;
    private JLabel stationName;
    private JLabel name;
    private JLabel type; //{SINGLE_BIKE, SINGLE_E_BIKE, DOUBLE_BIKE, DOUBLE_E_BIKE}
    private JLabel weight;
    private JLabel licensePlate;
    private JLabel manufacturingDate;
    private JLabel producer;
    private JLabel cost;
    private JLabel status;  //{EMPTY, PENDING, HIRING}
    private JLabel estimated;
    private JLabel batteryPercentage;
    private JLabel loadCycles;

    public BikeSinglePane() {
        super();
    }

    public BikeSinglePane(Bike bike) {
        this();
        this.t = bike;
        displayData();
    }

    @Override
    public void buildControls() {
        super.buildControls();

        int row = getLastRowIndex();
        c.gridx = 0;
        c.gridy = row;
        stationName = new JLabel();
        add(stationName, c);

        row = getLastRowIndex();
        c.gridx = 0;
        c.gridy = row;
        name = new JLabel();
        add(name, c);

        row = getLastRowIndex();
        c.gridx = 0;
        c.gridy = row;
        type = new JLabel();
        add(type, c);

        row = getLastRowIndex();
        c.gridx = 0;
        c.gridy = row;
        weight = new JLabel();
        add(weight, c);

        row = getLastRowIndex();
        c.gridx = 0;
        c.gridy = row;
        weight = new JLabel();
        add(weight, c);

        row = getLastRowIndex();
        c.gridx = 0;
        c.gridy = row;
        licensePlate = new JLabel();
        add(licensePlate, c);

        row = getLastRowIndex();
        c.gridx = 0;
        c.gridy = row;
        manufacturingDate = new JLabel();
        add(manufacturingDate, c);

        row = getLastRowIndex();
        c.gridx = 0;
        c.gridy = row;
        producer = new JLabel();
        add(producer, c);

        row = getLastRowIndex();
        c.gridx = 0;
        c.gridy = row;
        cost = new JLabel();
        add(cost, c);


        row = getLastRowIndex();
        c.gridx = 0;
        c.gridy = row;
        estimated = new JLabel();
        add(estimated, c);

        row = getLastRowIndex();
        c.gridx = 0;
        c.gridy = row;
        batteryPercentage = new JLabel();
        add(batteryPercentage, c);

        row = getLastRowIndex();
        c.gridx = 0;
        c.gridy = row;
        loadCycles = new JLabel();
        add(loadCycles, c);

        row = getLastRowIndex();
        c.gridx = 0;
        c.gridy = row;
        status = new JLabel();
        add(status, c);
    }


    @Override
    public void displayData() {
        this.stationName.setText("Station: " + t.getStationName());
        this.name.setText("Name: " + t.getName());
        this.type.setText("Type: " + t.getType()); //{SINGLE_BIKE, SINGLE_E_BIKE, DOUBLE_BIKE, DOUBLE_E_BIKE}
        this.weight.setText("Weight: " + t.getWeight());
        this.licensePlate.setText("License Plate: " + t.getLicensePlate());
        this.manufacturingDate.setText("Manufacturing Date: " + t.getManufacturingDate());
        this.producer.setText("Producer: " + t.getProducer());
        this.cost.setText("Cost: " + t.getCost());
        this.status.setText("Status: " + t.getStatus());  //{EMPTY, PENDING, HIRING}
        if (t != null)
            if (t.getType().equals("SINGLE_E_BIKE") || t.getType().equals("DOUBLE_E_BIKE")) {
                this.estimated.setText("producer: " + t.getProducer());
                this.batteryPercentage.setText("batteryPercentage: " + t.getBatteryPercentage());
                this.loadCycles.setText("loadCycles: " + t.getLoadCycles());
            }
    }
}
