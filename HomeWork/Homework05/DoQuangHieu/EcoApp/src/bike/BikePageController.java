package bike;

import abstractdata.ADataListPane;
import abstractdata.ADataPageController;
import abstractdata.ADataSearchPane;
import abstractdata.ADataSinglePane;
import api.BikeApi;
import com.ecoapp.bean.Bike;
import com.ecoapp.bean.Station;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class BikePageController extends ADataPageController<Bike> {
    EditBikeController controller;

    public BikePageController() {
        super();
    }

    public BikePageController(EditBikeController controller) {
        this();
        ADataListPane<Bike> listPane = createListPane();

        setController(controller);
    }

    public void setController(EditBikeController controller) {
        this.controller = controller;
    }

    public ArrayList<Station> getListStation() {
        return null;
    }

    @Override
    public ADataListPane<Bike> createListPane() {
        return new BikeListPane(this, "ADMIN");
    }

    public void update(Bike bike) {
         controller.update(bike);
    }

    @Override
    public ADataSearchPane createSearchPane() {
        return new BikeSearchPane();
    }

    @Override
    public List<? extends Bike> search(Map<String, String> searchParams) {
        return new BikeApi().getBikes(searchParams);
    }

    @Override
    public ADataSinglePane<Bike> createSinglePane() {
        return new BikeSinglePane();
    }

}
