package bike;

import com.ecoapp.bean.Bike;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@SuppressWarnings("serial")
public class BikePane extends JPanel{
	private BikePageController controller;
	private BikeSinglePane singlePane;
	private Bike bike;

	protected GridBagLayout layout;
	protected GridBagConstraints c = new GridBagConstraints();
	
	public BikePane() {
		this.bike = new Bike();
		this.setLayout(new FlowLayout(FlowLayout.RIGHT));
		JPanel panel = singlePane;
//		JButton editButton = new JButton("Edit");
//		this.add(editButton);
//
//		editButton.addActionListener(new ActionListener() {
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				controller.showCartDialog();
//			}
//		});
	}
	
	public void setController(BikePageController controller) {
		this.controller = controller;
	}
	
	public void updateData(Bike bike) {
		this.bike = bike;
	}
}