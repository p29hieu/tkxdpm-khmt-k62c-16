package station;

import com.ecoapp.bean.Station;
import abstractdata.ADataSinglePane;

import javax.swing.*;

@SuppressWarnings("serial")
public class StationSinglePane extends ADataSinglePane<Station> {
    private JLabel name;
    private JLabel address;
    private JLabel numSingleBike;
    private JLabel numSingleEBike;
    private JLabel numDoubleBike;
    private JLabel numEmptyDock;

    public StationSinglePane() {
        super();
    }

    public StationSinglePane(Station station) {
        this();
        this.t = station;
        displayData();
    }

    @Override
    public void buildControls() {
        super.buildControls();

        int row = getLastRowIndex();
        c.gridx = 0;
        c.gridy = row;
        name = new JLabel();
        add(name, c);

        row = getLastRowIndex();
        c.gridx = 0;
        c.gridy = row;
        address = new JLabel();
        add(address, c);

        row = getLastRowIndex();
        c.gridx = 0;
        c.gridy = row;
        numSingleBike = new JLabel();
        add(numSingleBike, c);

        row = getLastRowIndex();
        c.gridx = 0;
        c.gridy = row;
        numSingleEBike = new JLabel();
        add(numSingleEBike, c);

        row = getLastRowIndex();
        c.gridx = 0;
        c.gridy = row;
        numDoubleBike = new JLabel();
        add(numDoubleBike, c);

        row = getLastRowIndex();
        c.gridx = 0;
        c.gridy = row;
        numEmptyDock = new JLabel();
        add(numEmptyDock, c);
    }


    @Override
    public void displayData() {
        this.name.setText("Name: " + t.getName());
        this.address.setText("Address: " + t.getAddress());
        this.numSingleBike.setText("Number of Bikes: " + t.getNumSingleBike()); //{SINGLE_BIKE, SINGLE_E_BIKE, DOUBLE_BIKE, DOUBLE_E_BIKE}
        this.numSingleEBike.setText("Number of EBikes: " + t.getNumSingleEBike());
        this.numDoubleBike.setText("Number of Twin Bikes: " + t.getNumDoubleBike());
        this.numEmptyDock.setText("Number of empty docks: " + t.getNumEmptyDock());
    }
}
