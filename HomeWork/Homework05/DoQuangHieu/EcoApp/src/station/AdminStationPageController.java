package station;

import java.util.List;
import java.util.Map;

import com.ecoapp.bean.Station;

import abstractdata.ADataListPane;
import abstractdata.ADataPageController;
import abstractdata.ADataSearchPane;
import abstractdata.ADataSinglePane;
import abstractdata.IDataSearchController;
import api.StationApi;

public class AdminStationPageController extends ADataPageController<Station> {

	public AdminStationPageController() {
		super();
	}
	
	@Override
	public ADataListPane<Station> createListPane() {
		return new StationListPane(this);
	}
	
	@Override
	public StationSinglePane createSinglePane() {
		return new StationSinglePane();
	}
	
	@Override
	public StationSearchPane createSearchPane() {
		return new StationSearchPane();
	}
	
	@Override
	public List<Station> search(Map<String, String> searchParams) {
		return new StationApi().getStations(searchParams);
	};
}
