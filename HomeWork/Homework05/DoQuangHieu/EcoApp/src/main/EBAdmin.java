package main;

import javax.swing.*;
import java.awt.*;


public class EBAdmin extends JFrame {

    public static final int WINDOW_WIDTH = 800;
    public static final int WINDOW_HEIGHT = 550;

    public EBAdmin(EBAdminController controller) {
        JPanel rootPanel = new JPanel();
        setContentPane(rootPanel);
        BorderLayout layout = new BorderLayout();
        rootPanel.setLayout(layout);

//        rootPanel.add(controller.getBikePage(), BorderLayout.NORTH);

        JTabbedPane tabbedPane = new JTabbedPane();
        rootPanel.add(tabbedPane, BorderLayout.CENTER);

        JPanel bookPage = controller.getBikePage();
        JPanel stationPage = controller.getStationPage();
        if (bookPage != null) tabbedPane.addTab("Bikes", null, bookPage, "Bikes");
        tabbedPane.addTab("Stations", null, stationPage, "Tabs 2");
        tabbedPane.addTab("Tabs 3", null, new JPanel(), "Tabs 3");


        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Eco Bike Systems");
        setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        setVisible(true);
    }

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }

        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new EBAdmin(new EBAdminController());
            }
        });
    }
}
