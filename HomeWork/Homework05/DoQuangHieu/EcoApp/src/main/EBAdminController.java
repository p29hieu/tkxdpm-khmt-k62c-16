package main;

import bike.BikePageController;
import bike.EditBikeController;
import station.AdminStationPageController;

import javax.swing.*;

public class EBAdminController extends JFrame {

    public EBAdminController() {
    }

    public JPanel getBikePage() {
        BikePageController bikePageController= new BikePageController();
        return bikePageController.getDataPagePane();
    }

    public JPanel getStationPage() {
        AdminStationPageController adminStationPageController= new AdminStationPageController();
        return adminStationPageController.getDataPagePane();
    }
//    public JPanel getEditPage() {
//
//    }
}
