package com.ecoapp.bean;

import java.util.Objects;

public class Pair {
    private String key;

    private String value;

    public Pair() {
    }

    public Pair(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public void clone(Pair p) {
        this.key = p.getKey();
        this.value = p.getValue();
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }


    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pair pair = (Pair) o;
        return Objects.equals(key, pair.key);
    }

    public boolean equals(String key) {
        return Objects.equals(key, this.key);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key, value);
    }
}
