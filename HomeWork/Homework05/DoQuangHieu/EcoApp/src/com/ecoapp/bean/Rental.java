package com.ecoapp.bean;

// refund

public class Rental {

    private int fromMinutes = 0;
    private int toMinutes = 30;
    private int fixedPrice = 10000;
    private int refund = 0;
    private int after15 = 3000;

    public Rental() {
    }

    public Rental(int fromM, int toM, int fPrice, int nM, int a15) {
        this.fromMinutes = fromM;
        this.toMinutes = toM;
        this.fixedPrice = fPrice;
        this.refund = nM;
        this.after15 = a15;
    }

    public int getFromMinutes() {
        return fromMinutes;
    }

    public void setFromMinutes(int fromMinutes) {
        this.fromMinutes = fromMinutes;
    }

    public int getToMinutes() {
        return toMinutes;
    }

    public void setToMinutes(int toMinutes) {
        this.toMinutes = toMinutes;
    }

    public int getFixedPrice() {
        return fixedPrice;
    }

    public void setFixedPrice(int fixedPrice) {
        this.fixedPrice = fixedPrice;
    }

    public int getRefund() {
        return refund;
    }

    public void setRefund(int refund) {
        this.refund = refund;
    }

    public int getAfter15() {
        return after15;
    }

    public void setAfter15(int after15) {
        this.after15 = after15;
    }

    // tinh chi phi thue xe tong cong
    public int calRental(int minutes) {
        int tmp, res = 0;
        if (this.fromMinutes <= minutes && minutes <= this.toMinutes) {
            return this.fixedPrice;
        } else if (0 <= minutes && minutes < this.fromMinutes) {
            tmp = (int) Math.ceil((this.fromMinutes - minutes) / 60.0);
            System.out.print("tmp = ");
            System.out.println(tmp);
            res = this.fixedPrice - tmp * this.refund;
        } else {
            tmp = (int) Math.ceil((minutes - this.toMinutes) / 15.0);
            System.out.print("tmp = ");
            System.out.println(tmp);
            res = this.fixedPrice + tmp * this.after15;
        }

        return res;
    }

    @Override
    public String toString() {
        return "fromMinutes: " + this.fromMinutes + ", toMinutes: " +
                this.toMinutes + ", fixedPrice: " + this.fixedPrice + ", refund: " +
                this.refund + ", after15: " + this.after15;
    }
}
