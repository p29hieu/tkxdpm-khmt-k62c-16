package abstractdata;

public interface IDataManageController<T> {
	public T create(T t);
	public T read(T t);
	public void delete(T t);
	public T update(T t);
}
