package abstractdata;

import javax.swing.*;
import java.util.List;
import java.util.Map;

public abstract class ADataPageController<T> {
    private ADataPagePane<T> pagePane;

    private JDialog dialog;
   public ADataListPane<T> listPane;

    public ADataPageController(JDialog dialog) {
        this();
        this.dialog = dialog;
    }

    public ADataPageController() {
        ADataSearchPane searchPane = createSearchPane();

        listPane = createListPane();

       if(searchPane!=null) {
           searchPane.setController(new IDataSearchController() {
               @Override
               public void search(Map<String, String> searchParams) {
                   List<? extends T> list = ADataPageController.this.search(searchParams);
                   listPane.updateData(list);
               }
           });

           searchPane.fireSearchEvent();
       }

        pagePane = new ADataPagePane<T>(searchPane, listPane);
    }

    public JPanel getDataPagePane() {
        return pagePane;
    }


    public abstract ADataSearchPane createSearchPane();

    public abstract List<? extends T> search(Map<String, String> searchParams);


    public abstract ADataSinglePane<T> createSinglePane();

    public abstract ADataListPane<T> createListPane();

    public void showDialog() {
        dialog.setVisible(true);
    }

    public void hideDialog() {
        dialog.setVisible(false);
    }

    public JDialog getDialog() {
        return dialog;
    }

    public ADataPageController<T>  setDialog(JDialog dialog) {
        this.dialog = dialog;
        dialog.setVisible(false);
        return this;
    }

    public void updateDataListPane(T t){
        listPane.updateData(t);
    }
}
