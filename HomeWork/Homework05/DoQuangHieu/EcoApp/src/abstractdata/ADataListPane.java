package abstractdata;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collections;
import java.util.List;

@SuppressWarnings("serial")
public abstract class ADataListPane<T> extends JScrollPane {
    private LayoutManager layout;
    protected JPanel pane;
    protected List<? extends T> listData;

    protected ADataPageController<T> controller;

    public ADataListPane() {
        pane = new JPanel();
        layout = new BoxLayout(pane, BoxLayout.Y_AXIS);
        pane.setLayout(layout);

        this.setViewportView(pane);
        this.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        this.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        this.getVerticalScrollBar().setUnitIncrement(20);
        this.getHorizontalScrollBar().setUnitIncrement(20);
    }


    public abstract void decorateSinglePane(ADataSinglePane<T> singlePane);

    public void updateData(List<? extends T> list) {
        this.listData = list;

        pane.removeAll();
        pane.revalidate();
        pane.repaint();
		
        JButton button = new JButton("Add");
//		singlePane.addDataHandlingComponent(button);
		
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
//				if (controller instanceof UserMediaPageController) {
//					((UserMediaPageController) controller).addToCart(singlePane.getData().getId(), singlePane.getData().getTitle(), singlePane.getData().getCost(), (int)spin.getValue());
//				}
			}
		});
		pane.add(button);
		
        for (T t : listData) {
            ADataSinglePane<T> singlePane = controller.createSinglePane();
            decorateSinglePane(singlePane);

            singlePane.updateData(t);
            pane.add(singlePane);
            pane.add(Box.createRigidArea(new Dimension(0, 40)));
        }
    }

    public void updateData(T t) {
        pane.removeAll();
        pane.revalidate();
        pane.repaint();

        for (T t1 : listData) {
            ADataSinglePane<T> singlePane = controller.createSinglePane();
            decorateSinglePane(singlePane);
            if (t1.equals(t))
                singlePane.updateData(t1);
            else singlePane.updateData(t);
            pane.add(singlePane);
            pane.add(Box.createRigidArea(new Dimension(0, 40)));
        }
    }
}
